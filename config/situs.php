<?php
return [

    'BERANDA_SLIDER_TAG_1' => 'Discover dan enjoy',
    'BERANDA_SLIDER_TITLE_1' => 'Hotel dengan nuansa gunung',
    'BERANDA_SLIDER_LINK_1' => '/tentang-kami',
    'BERANDA_SLIDER_IMAGE_1' => 'img_bg_5.jpg',
    'BERANDA_KATEGORI_MENU_1' => 'dinner',
    'BERANDA_KATEGORI_MENU_2' => 'drink',

    'BERANDA_SLIDER_TAG_2' => 'Kenyamanan',
    'BERANDA_SLIDER_TITLE_2' => 'Kenyamanan yang tak tertandingi',
    'BERANDA_SLIDER_LINK_2' => '/tentang-kami',
    'BERANDA_SLIDER_IMAGE_2' => 'img_bg_1.jpg',

    'BERANDA_SLIDER_TAG_3' => 'Kepuasan selamannya',
    'BERANDA_SLIDER_TITLE_3' => 'Kami mengundang anda',
    'BERANDA_SLIDER_LINK_3' => '/kontak-kami',
    'BERANDA_SLIDER_IMAGE_3' => 'img_bg_3.jpg',

    'BERANDA_SLIDER_TAG_4' => 'promo',
    'BERANDA_SLIDER_TITLE_4' => 'Hotel dengan segudang promo',
    'BERANDA_SLIDER_LINK_4' => '/berita',
    'BERANDA_SLIDER_IMAGE_4' => 'img_bg_4.jpg',

    'BERANDA_FRONT_ICON_1' => 'flaticon-reception',
    'BERANDA_FRONT_TITLE_1' => '24/7 Jam Front desk',
    'BERANDA_FRONT_PARAGRAPH_1' => 'Kami selalu siap melayani anda 24 jam setiap hari demi kenyamanan dan kepuasan anda dalam berlibur dan menginap
        dihotel kami, Petugas kami akan senang membantu anda mendapatkan kamar yang terbaik untuk anda.',

    'BERANDA_FRONT_ICON_2' => 'flaticon-herbs',
    'BERANDA_FRONT_TITLE_2' => 'Pelayanan SPA',
    'BERANDA_FRONT_PARAGRAPH_2' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.
        Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer
        took a galley of type and scrambled it to make a type specimen book.',

    'BERANDA_FRONT_ICON_3' => 'flaticon-car',
    'BERANDA_FRONT_TITLE_3' => 'Layanan Antar jemput airport atau destinasi liburan',
    'BERANDA_FRONT_PARAGRAPH_3' => 'Kami menyediakan transport untuk anda yang ingin berkelana menuju destinasi namun tidak
        mempunyai transportasi, supir kami akan siap mengantar anda menuju tempat yang anda inginkan',

    'BERANDA_FRONT_ICON_4' => 'flaticon-cheers',
    'BERANDA_FRONT_TITLE_4' => 'Restoran yang nyaman',
    'BERANDA_FRONT_PARAGRAPH_4' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.
            Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer
            took a galley of type and scrambled it to make a.',

    'BERANDA_ROOM_TITLE' => 'Kamar & Suite',
    'BERANDA_ROOM_PARAGRAPH' => 'Kami tersanjung dengan kedatangan anda dan memberikan pelayanan yang maksimal dengan
        kamar yang sangat nyaman dan memukau.',

    'BERANDA_DINING_TITLE' => 'Dining & Bar',
    'BERANDA_DINING_PARAGRAPH' => 'Dengan makanan khas eropa kami akan memanjakan lidah anda diwaktu malam hari dan anda
        akan menemukan sesuatu momen yang spesial',
    'BERANDA_DINING_IMAGE_BACKGROUND' => 'cover_img_1.jpg',

    'KAMAR_TAG' => 'Pilihan kamar terbaik kami',
    'KAMAR_TITLE' => 'Kamar & Suite',
    'KAMAR_BACKGROUND' => 'double_bed2.jpg',

    'AMENITIES_TAG' => 'Akomodasi',
    'AMENITIES_TITLE' => 'Amenities',
    'AMENITIES_BACKGROUND' => 'pool2.jpg',

    'AMENITIES_1_TITLE' => 'Layanan Antar Jemput Bandara Sukarno Hatta',
    'AMENITIES_1_HARGA' => 1000000,
    'AMENITIES_1_SATUAN' => 'lot',
    'AMENITIES_1_KETERANGAN' => 'Kami menyediakan layanan antar jemput dari bandara ke hotel kami, anda tinggal memesan lewat email atau telpon serta
        membayar uang untuk kemudian kami proses dan segera kami jemput anda berdasarkan waktu yang telah ditetapkan',
    'AMENITIES_1_GAMBAR' => 'antar_jemput.jpg',

    'AMENITIES_2_TITLE' => 'Ruang konferensi untuk acara meeting dan pertemuan',
    'AMENITIES_2_HARGA' => 2500000,
    'AMENITIES_1_SATUAN' => 'lot',
    'AMENITIES_2_KETERANGAN' => 'Kami menyediakan ruang untuk acara pertemuan, silahturami dan rapat institusi yang nyaman dan sejuk serta tidak bising membuat
        rapat anda dan kolega anda berlangsung baik dan fokus',
    'AMENITIES_2_GAMBAR' => 'ruang_meeting.jpeg',

    'AMENITIES_3_TITLE' => 'Kolam Renang',
    'AMENITIES_3_HARGA' => 0,
    'AMENITIES_3_KETERANGAN' => 'Keinginan anda untuk berolahraga di hotel kami dapat terpenuhi dengan adanya kolam renang yang dapat anda nikmati dalam 24 jam
        selama anda menginap dihotel kami, dapatkan kepuasan dan momen spesial setelah anda menginap dihotel kami',
    'AMENITIES_3_GAMBAR' => 'kolam_renang.jpeg',

    'AMENITIES_4_TITLE' => 'Sunset View',
    'AMENITIES_4_HARGA' => 0,
    'AMENITIES_4_KETERANGAN' => 'Pengalaman menginap dihotel kami dapat melihat sunset view yang indah diwaktu sore hari menjelang malam, menjadikan
        momen yang sangat spesial ketika anda menginap dihotel kami',
    'AMENITIES_4_GAMBAR' => 'sunset_view.jpeg',

    'TENTANG_KAMI_TAG' => 'Informasi',
    'TENTANG_KAMI_TITLE' => 'Tentang Kami',
    'TENTANG_KAMI_CONTENT_TITLE' => 'Selamat datang di hotel kami',
    'TENTANG_KAMI_CONTENT_PARAGRAPH' => 'Hotel Kami terletak di pegunungan cisarua yang asri dan rindang, tempat untuk melepas lelah dan letih
    setelah bekerja di ibu kota yang padat dan jenuh. Alam yang sejuk dan bersih serta kamar yang nyaman memberikan pengalaman liburan kepada anda yang
    lebih memuaskan dan tak pernah terlupakan. Hotel Kami sudah berdiri sejak 1993 dan sekarang sudah berkembang hingga mempunyai luas tanah 4 Hektar',
    'TENTANG_KAMI_TOP_IMAGE' => 'kolam.jpg',
    'TENTANG_KAMI_CONTENT_IMAGE' => 'taman2.jpg',

    'KONTAK_KAMI_TAG' => 'Informasi lebih jauh',
    'KONTAK_KAMI_TITLE' => 'Kontak Kami',
    'KONTAK_KAMI_KONTAK_TITLE' => 'Dapatkan Informasi Lebih Jauh',
    'KONTAK_KAMI_IMAGE_BACKGROUND' => 'taman2.jpg',

    'LOGIN_TAG' => 'Khusus untuk user yang berwenenang',
    'LOGIN_TITLE' => 'Halaman Login',
    'LOGIN_SUB_TITLE' => 'Silahkan Login',

    'DAFTAR_TAG' => 'Pendaftaran user baru',
    'DAFTAR_TITLE' => 'Registrasi User Baru',
    'DAFTAR_SUB_TITLE' => 'Pendaftaran User Baru',
    'DAFTAR_BARU_TOP_IMAGE' => 'registration.jpg',

    'DETAIL_KAMAR_TAG' => 'Melihat Kamar Lebih Jauh',
    'DETAIL_KAMAR_TITLE' => 'Detail Kamar',
    'DETAIL_KAMAR_TOP_IMAGE' => 'detail.jpg'




];
