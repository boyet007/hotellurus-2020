<?php

return [
    'NAMA_HOTEL'        => 'Hotel Lurus',
    'TAG_HOTEL'         => 'Home For Everyone',
    'TELPON_BASIC'      => '02412820',
    'TELPON'            => '0251-8259188',
    'HP'                => '0813 847-304-35',
    'WEBSITE'           => 'www.hotellurus.com',
    'EMAIL'             => 'hotellurus@gmail.com',
    'ALAMAT_JALAN'      => 'Jl. hankam raya no. 22',
    'ALAMAT_KABUPATEN'  => 'Cisarua',
    'ALAMAT_KOTA'       => 'Bogor',
    'ALAMAT_PROVINSI'   => 'Jawa Barat',
    'ALAMAT_KODEPOS'    => '17650',
    'LOGO'              => 'logo2.jpg',
    'DESKRIPSI'         => 'Hotel Lurus merupakan hotel mewah yang berdiri diatas puncak Cisarua dengan pemandangan gunung yang indah
                            dan taman yang asri cocok untuk anda yang ingin melepas penat setelah bekerja di kota yang padat'
];
