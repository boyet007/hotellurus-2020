const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.sass('resources/sass/web_custom.scss', 'public/web/css/custom.css')
   .sass('resources/sass/app.scss', 'public/css')
   .copy('resources/web/',  'public/web')
   .copy('resources/admin/', 'public/admin')
   .copy('resources/images/', 'public/images');
