<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pesan extends Model
{
    protected $table = 'pesan';
    protected $fillable = ['nama_lengkap', 'no_telpon', 'email', 'judul', 'isi_pesan'];
}
