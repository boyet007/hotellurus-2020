<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Karyawan extends Model
{
    protected $table = 'karyawan';
    protected $fillable = ['nama_lengkap', 'posisi', 'alamat', 'kota', 'kode_pos', 
        'no_telpon', 'email', 'jenkel', 'foto', 'keterangan'];
}
