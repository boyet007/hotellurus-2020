<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Kamar extends Model
{
    protected $table = 'kamar';
    protected $fillable = ['kategori_kamar_id', 'nomor_kamar', 'lantai',
        'keterangan', 'status', 'gambar'];
    public $timestamps = false;

    public function kategori_kamar()
    {
        return $this->belongsTo(KategoriKamar::class);
    }

    public function dapatkanKamarKosong($kategoriKamar) {
        $kamarKosong = DB::raw("SELECT * FROM Kamar WHERE STATUS='available' AND kategori_kamar_id = 1");

    }
}
