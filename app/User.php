<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nama_lengkap', 'email', 'password', 'alamat', 'kota', 'no_telpon',
            'kodepos', 'jenkel', 'role', 'verifikasi_token', 'gambar'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'verifikasi_token',
    ];

    protected $casts = [
        'status_verifikasi' => 'boolean',
    ];

    public function setTokenVerifikasi() {
        $token = Str::random(60);
        $this->verifikasi_token = $token;
        $this->save();
    }

    public function verifikasi() {
        if ($this->status_verifikasi) {
            return 'sudah terverifikasi';
        } else {
            return 'belum terverifikasi';
        }
    }

    public function transaksi_kamar() {
        return $this->hasMany(TransaksiKamar::class);
    }

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    // protected $casts = [
    //     'email_verified_at' => 'datetime',
    // ];
}
