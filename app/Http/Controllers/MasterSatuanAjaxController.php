<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Satuan;
use DataTables;
class MasterSatuanAjaxController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.master.satuan.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new Satuan();
        return view('admin.master.satuan.form', compact('model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'kode' => 'required|string|max:3|unique:satuan',
            'nama' => 'required|string|max:50',
        ]);
        $model = Satuan::create($request->all());
        return $model;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = Satuan::findOrFail($id);
        return view('admin.master.satuan.tampil', compact('model'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Satuan::findOrFail($id);
        return view('admin.master.satuan.form', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'kode' => 'required|string|max:3|unique:satuan,id,' . $id,
            'nama' => 'required|string|max:50',
        ]);

        $model = Satuan::findOrFail($id);
        $model->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Satuan::findOrFail($id);
        $model->delete();
    }

    public function dataTable()
    {
        $model = Satuan::query();

        return Datatables::of($model)
        ->addColumn('aksi', function($model) {
                return view('layouts.aksi._aksi-master', [
                    'model' => $model,
                    'url_show' => route('ajax-master-satuan.show', $model->id),
                    'url_edit' => route('ajax-master-satuan.edit', $model->id),
                    'url_destroy' => route('ajax-master-satuan.destroy', $model->id)
                ]);
        })
        ->rawColumns(['aksi'])
        ->make(true);
    }
}
