<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kontak;
use DataTables;

class KontakAjaxController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Data Kontak';
        return view('admin.kontak.index', compact("title"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = Kontak::findorFail($id);
        //jika belum terbaca set jadi terbaca
        if ($model->status_baca === 0) {
            $model->status_baca = 1;
            $model->save();
        }
        return view('admin.kontak.tampil', compact('model'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Kontak::findOrFail($id);
        $model->delete();
    }

    public function dataTable()
    {
        $model = Kontak::orderBy('created_at', 'DESC');

        return DataTables::of($model)
        ->addColumn('aksi', function($model) {
                return view('layouts.aksi._aksi4-master', [
                    'model' => $model,
                    'url_show' => route('ajax-kontak.show', $model->id),
                    'url_destroy' => route('ajax-kontak.destroy', $model->id)
                ]);
        })
        ->rawColumns(['aksi'])
        ->make(true);
    }
}
