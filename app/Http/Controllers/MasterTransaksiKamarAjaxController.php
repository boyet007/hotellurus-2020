<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TransaksiKamar;
use App\DetailTransaksiKamar;
use App\User;
use App\Kamar;
use App\Layanan;
use DataTables;

class MasterTransaksiKamarAjaxController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.transaksi.kamar.index', compact('title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new TransaksiKamar;
        $noTrans = $this->dapatkanNomorBaru();
        $user = ['' => '====== Masukkan Customer Email ====='] + User::pluck('email', 'id')->all();
        $tipeBayar = [
            '' => '====== Masukkan Tipe Bayar =======',
            'tunai' => 'Tunai',
            'debit' => 'Debit',
            'kredit' => 'Kredit',
            'lain-lain' => 'Lain-Lain'
        ];

        $tipeTransaksi = [
            '' => '== Tipe ==',
            'kamar' => 'Kamar',
            'layanan' => 'Layanan',
        ];

        return view('admin.transaksi.kamar.form', compact('model', 'user', 'noTrans', 'tipeBayar', 'tipeTransaksi', 'listKamar'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'notrans' => 'required|unique:transaksi_kamar',
            'tanggal' => 'required',
            'user_id' => 'required',
            'tipe_bayar' => 'required',
            'tabel-detail-transaksi' => 'required|array|min:1',
        ]);

        $transaksi = TransaksiKamar::create($request->all());

        if($request->has('aktif')) {
            $transaksi->aktif = 1;
        } else {
            $transaksi->aktif = 0;
        }

        $transaksi->save();

        $arrTipe = $request->input('detailTipe', []);
        $arrKategori = $request->input('detailKategori', []);
        $arrNama = $request->input('detailNama', []);
        $arrQty = $request->input('detailQty', []);
        $arrHarga = $request->input('detailHarga', []);
        $arrSatuan = $request->input('detailSatuan', []);

        $arrDetailTransaksi = [];

        for($detail = 0; $detail < count($arrNama); $detail++) {
            $detailTransaksi = new DetailTransaksiKamar;
            $detailTransaksi->transaksi_kamar_id = $transaksi->id;
            $detailTransaksi->tipe = $request->detailTipe[$detail];
            $detailTransaksi->kategori = $request->detailKategori[$detail];
            $detailTransaksi->nama = $request->detailNama[$detail];
            $detailTransaksi->harga = $request->detailHarga[$detail];
            $detailTransaksi->qty = $request->detailQty[$detail];
            $detailTransaksi->satuan = $request->detailSatuan[$detail];
            array_push($arrDetailTransaksi, $detailTransaksi);
        }

        $transaksi->detail_transaksi()->saveMany($arrDetailTransaksi);

        //set kamar booking
        $this->setBookingKamar($transaksi);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = TransaksiKamar::with('user', 'detail_transaksi')->find($id);
        return view('admin.transaksi.kamar.tampil', compact('model'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = TransaksiKamar::findOrFail($id);

        $noTrans = $model->notrans;
        $user = ['' => '====== Masukkan Customer Email ====='] + User::pluck('email', 'id')->all();
        $tipeBayar = [
            '' => '====== Masukkan Tipe Bayar =======',
            'tunai' => 'Tunai',
            'debit' => 'Debit',
            'kredit' => 'Kredit',
            'lain-lain' => 'Lain-Lain'
        ];

        $tipeTransaksi = [
            '' => '== Tipe ==',
            'kamar' => 'Kamar',
            'layanan' => 'Layanan',
        ];

        return view('admin.transaksi.kamar.form', compact('model', 'user', 'noTrans', 'tipeBayar', 'tipeTransaksi', 'listKamar'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'notrans' => 'required|unique:transaksi_kamar,id,' . $id,
            'tanggal' => 'required',
            'user_id' => 'required',
            'tipe_bayar' => 'required',
            'tabel-detail-transaksi' => 'required|array|min:1',
        ]);

        $transaksi = TransaksiKamar::findOrFail($id);
        $transaksi->update($request->all());

        //set kamar lama menjadi available jika status masih aktif
        $detailTransaksiLama = json_decode($request->detail_transaksi_lama);
        
        if($request->has('aktif')) {
            $transaksi->aktif = 1;        
            //set status booking kamar lama menjadi available
            foreach($detailTransaksiLama as $detail) { 
                $tipe = $detail->tipe;
                if ($tipe === 'kamar') {
                    $noKamar = $detail->nama;
                    $kamar = Kamar::where('nomor_kamar', '=', $noKamar)->first();
                    $kamar->status = 'available';
                    $kamar->save();          
                }
            }
            
        } else {
            $transaksi->aktif = 0;
        }        

        $transaksi->save();

       

        DetailTransaksiKamar::where('transaksi_kamar_id', '=', $id)->delete();

        $arrTipe = $request->input('detailTipe', []);
        $arrKategori = $request->input('detailKategori', []);
        $arrNama = $request->input('detailNama', []);
        $arrQty = $request->input('detailQty', []);
        $arrHarga = $request->input('detailHarga', []);
        $arrSatuan = $request->input('detailSatuan', []);

        $arrDetailTransaksi = [];

        for($detail = 0; $detail < count($arrNama); $detail++) {
            $detailTransaksi = new DetailTransaksiKamar;
            $detailTransaksi->transaksi_kamar_id = $transaksi->id;
            $detailTransaksi->tipe = $request->detailTipe[$detail];
            $detailTransaksi->kategori = $request->detailKategori[$detail];
            $detailTransaksi->nama = $request->detailNama[$detail];
            $detailTransaksi->harga = $request->detailHarga[$detail];
            $detailTransaksi->qty = $request->detailQty[$detail];
            $detailTransaksi->satuan = $request->detailSatuan[$detail];

            array_push($arrDetailTransaksi, $detailTransaksi);
        }

        $transaksi->detail_transaksi()->saveMany($arrDetailTransaksi);

        //set kamar booking
        $this->setBookingKamar($transaksi);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DetailTransaksiKamar::where('transaksi_kamar_id', $id)->delete();
        TransaksiKamar::findOrFail($id)->delete();
    }

    public function dataTable()
    {
        $model =  TransaksiKamar::with('detail_transaksi', 'user')->orderBy('tanggal', 'DESC')->get();

        return DataTables::of($model)
        ->addColumn('nama_user', function($model) {
            return $model->user->nama_lengkap;
        })
        ->addColumn('nama_admin', function($model) {
            $id = $model->admin_id;
            return User::find($id)->nama_lengkap;
        })
        ->addColumn('total_bayar', function($model) {
            return $model->dapatkanTotalTransaksi();
        })
        ->addColumn('aksi', function($model) {
                return view('layouts.aksi._aksi5-master', [
                    'model' => $model,
                    'url_show' => route('ajax-master-transaksi-kamar.show', $model->id),
                    'url_cetak' => route('ajax-master-transaksi-kamar.cetak.deskjet', $model->id),
                    'url_edit' => route('ajax-master-transaksi-kamar.edit', $model->id),
                    'url_destroy' => route('ajax-master-transaksi-kamar.destroy', $model->id)
                ]);
        })
        ->rawColumns(['nama_user', 'aksi'])
        ->make(true);
    }

    public function dapatkanNomorBaru() {
        $tahunIni = date('Y');

        //dapatkan kode terakhir transaksi ditahun ini
        $jumlahRekord = TransaksiKamar::count();

        //ambil 3 angka terakhir dari notrans : 2020/HL/001
        if ($jumlahRekord > 0) {
            $notrans = TransaksiKamar::orderBy('notrans', 'DESC')->first()->notrans;
            $nomorBaru = intval(substr($notrans, 8, 3)) + 1;
        } else {
            $nomorBaru = 1;
        }
        

        //bikin kode baru
        $formatNomorBaru = str_pad($nomorBaru, 3, '0', STR_PAD_LEFT);
        $kodeBaru = $tahunIni . '/HL/' . $formatNomorBaru;
        return $kodeBaru;
    }

    public function dapatkanDataTipeTransaksi(Request $request) {
        $tipe = $request->tipe;
        $isAktif = $request->isAktif;

        if ($tipe === 'kamar' && $isAktif === "true") {
            $listKamar = Kamar::where('status', '=', 'available')->pluck('nomor_kamar', 'id')->all();
            return $listKamar;
        } elseif ($tipe === 'kamar' && $isAktif === "false") {
            $listKamar = Kamar::pluck('nomor_kamar', 'id')->all();
            return $listKamar;
        } elseif ($tipe === 'layanan') {
            $listLayanan = Layanan::pluck('nama', 'id')->all();
            return $listLayanan;
        }
    }

    public function validasiDetailTransaksi(Request $request) {

        $this->validate($request, [
            'detailTipe' => 'required',
            'detailKategori' => 'required',
            'detailNama' => 'required',
            'detailHarga' => 'required',
            'detailQty' => 'required',
            'detailSatuan' => 'required'
        ]);
    }

    public function dapatkanDataTransaksiUser(Request $request) {
        $id = $request->transId;
        return TransaksiKamar::findOrFail($id)->user;
    }

    public function dapatkanDataTransaksiDetail(Request $request) {
        $id = $request->transId;
        return TransaksiKamar::findOrFail($id)->detail_transaksi->toArray();
    }

    private function setBookingKamar($parTransaksi) {
        $detailTransaksi = $parTransaksi->detail_transaksi;

        for($no = 0; $no < count($detailTransaksi); $no++) { 
            $tipe = $detailTransaksi[$no]->tipe;
            $aktif = $parTransaksi->aktif;

            if ($tipe === 'kamar' && $aktif === 1) {
                $noKamar = $detailTransaksi[$no]->nama;
                $kamar = Kamar::where('nomor_kamar', '=', $noKamar)->first();
                $kamar->status = 'booking';
                $kamar->save(); 
            }
        }
    }

    public function cetakDeskjet($id) {
        $model = TransaksiKamar::find($id);
        return view('admin.transaksi.kamar.cetak', compact('model'));
    }

}
