<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DataTables;
use Illuminate\Support\Facades\Hash;
use Image;

class MasterUserAjaxController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Master User';
        return view('admin.master.user.index', compact('title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new User;
        $kota = ['' => '=== Pilih Kota ===',
        'Jakarta' => 'Jakarta',
        'Bekasi' => 'Bekasi', 'Bogor' => 'Bogor', 'Tanggerang' => 'Tanggerang', 'Depok' => 'Depok',
        'Bandung' => 'Bandung', 'Lain-Lain' => 'Lain-Lain'];

        $jenkel = ['' => '=== Pilih Jenis Kelamin ===', 'L' => 'Laki-Laki', 'P' => 'Perempuan'];
        return view('admin.master.user.form', compact('model', 'kota', 'jenkel'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $model = new User;
        $requestFile = $request->file;

        if($requestFile) {
            cekNamaFile($model, $requestFile);
        }


        $this->validate($request, [
            'nama_lengkap' => 'required|string|max:50',
            'email' => 'required|email|unique:users',
            'password' => 'required|string|between:5,10|confirmed',
            'alamat' => 'required',
            'kota' => 'required',
            'kodepos' => 'required|string|size:5',
            'no_telpon' => 'required|string|between:5,15|unique:users',
            'jenkel' => 'required',
        ]);

        $password = Hash::make($request->password);

        if($requestFile) {
            $namaGambar = $requestFile->getClientOriginalName();
            $lebarKamar = config('setting.GAMBAR_USER_LEBAR');
            $tinggiKamar = config('setting.GAMBAR_USER_TINGGI');

            $gambar = Image::make($request->file('file'))->resize($lebarKamar, $tinggiKamar)
            ->save(public_path('images/persons') . '/' . $namaGambar);

            $request->request->add(['gambar' => $namaGambar]);
        }

        $model = User::create($request->all());
        if($requestFile) $gambar->destroy();
        return $model;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = User::findOrFail($id);
        return view('admin.master.user.tampil', compact('model'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = User::findOrFail($id);
        $kota = ['' => '=== Pilih Kota ===',
        'Jakarta' => 'Jakarta',
        'Bekasi' => 'Bekasi', 'Bogor' => 'Bogor', 'Tanggerang' => 'Tanggerang', 'Depok' => 'Depok',
        'Bandung' => 'Bandung', 'Lain-Lain' => 'Lain-Lain'];

        $jenkel = ['' => '=== Pilih Jenis Kelamin ===', 'L' => 'Laki-Laki', 'P' => 'Perempuan'];
        return view('admin.master.user.form', compact('model', 'kota', 'jenkel'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->file) {
            $model = new User;
            cekNamaFile($model, $request->file);
            $this->validate($request, [
                'nama_lengkap' => 'required|string|max:50',
                'email' => 'required|email|unique:users,id,' . $id,
                'alamat' => 'required',
                'kota' => 'required',
                'kodepos' => 'required|string|size:5',
                'no_telpon' => 'required|string|between:5,15|unique:users,id,' . $id,
                'jenkel' => 'required',
                'file' => 'image|mimes:jpeg,png,jpg',
            ]);

            if($request->nama_file_lama !== 'unknown.jpg' && file_exists(public_path('images/persons/') . $request->nama_file_lama)) {
                unlink(public_path('images/persons/') . $request->nama_file_lama);
            }

            $namaGambar = $request->file->getClientOriginalName();
            $lebarLayanan = config('setting.GAMBAR_USER_LEBAR');
            $tinggiLayanan = config('setting.GAMBAR_USER_TINGGI');
            $gambar = Image::make($request->file('file'))->resize($lebarLayanan, $tinggiLayanan)
                ->save(public_path('images/persons') . '/' . $namaGambar);
            $request->request->add(['gambar' => $namaGambar]);
            $model = User::findOrFail($id);
            $model->update($request->all());
            $gambar->destroy();
        } else {
            $namaGambar = $request->nama_file_lama;
            $this->validate($request, [
                'nama_lengkap' => 'required|string|max:50',
                'email' => 'required|email|unique:users,id,' . $id,
                'alamat' => 'required',
                'kota' => 'required',
                'kodepos' => 'required|string|size:5',
                'no_telpon' => 'required|string|between:5,15|unique:users,id,' . $id,
                'jenkel' => 'required',
            ]);
            $model = User::findOrFail($id);
            $request->request->add(['gambar' => $namaGambar]);
            $model->update($request->all());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = User::findOrFail($id);
        $gambar = $model->gambar;
        $model->delete();

        if($gambar !== 'unknown.jpg' && file_exists(public_path('images/persons/') . $gambar)) {
            unlink(public_path('images/persons/') . $gambar);
        }
    }

    public function dataTable()
    {
        $model = User::get();

        return DataTables::of($model)
        ->addColumn('aksi', function($model) {
                return view('layouts.aksi._aksi2-master', [
                    'model' => $model,
                    'url_show' => route('ajax-master-user.show', $model->id),
                    'url_edit' => route('ajax-master-user.edit', $model->id),
                    'url_destroy' => route('ajax-master-user.destroy', $model->id)
                ]);
        })
        ->rawColumns(['aksi'])
        ->make(true);
    }

    public function dapatkanDataUser(Request $request) {
        $id = $request->userId;
        $user = User::findOrFail($id);
        return $user;
    }

    public function dapatkanNamaAdmin(Request $request) {
        $id = $request->id;
        $user = User::findOrFail($id);
        return $user->nama_lengkap;
    }
}
