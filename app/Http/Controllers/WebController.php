<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\User;
use App\Slider;
use App\KategoriKamar;
use App\Kamar;
use App\KategoriLayanan;
use App\Kontak;
use Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class WebController extends Controller
{
    use AuthenticatesUsers;

    public function beranda() {
        $listSlider = Slider::all();
        $KategoriLayanan = KategoriLayanan::all();
        $kategoriKamar = KategoriKamar::all();
        $menu1 = KategoriLayanan::where('nama', (config('situs.BERANDA_KATEGORI_MENU_1')))->first();
        $menu2 = KategoriLayanan::where('nama', (config('situs.BERANDA_KATEGORI_MENU_2')))->first();

        return view('beranda', compact('listSlider', 'kategoriKamar', 'menu1', 'menu2'));
    }
    public function kamar($slug) {
        $kamar = null;
        if ($slug && KategoriKamar::where('slug', $slug)->count() > 0) {
            $kategoriKamar = KategoriKamar::where('slug', $slug)->first();
            $kamar = $kategoriKamar->kamar;
        } else {
            $kamar = Kamar::all();
        }
        return view('web/kamar', compact('kamar'));
    }
    public function amenities() {
        return view('web/amenities');
    }
    public function tentangKami() {
        return view('web/tentang-kami');
    }
    public function berita() {
        return view('web/berita');
    }
    public function kontakKami() {
        return view('web/kontak-kami');
    }

    public function postKontak(Request $request) {
        $kontak = new Kontak;

        $this->validate($request, [
            'nama_lengkap' => 'required|string|max:50',
            'no_telpon' => 'required|numeric|digits_between:5,15',
            'email' => 'required|email',
            'subyek' => 'required|max:150',
            'pesan' => 'required'
        ]);

        $kontak = Kontak::create($request->all());
        return redirect()->back()->with('sukses', 'Pesan anda telah terkirim, silahkan menunggu respon dari kami. Terima Kasih');
    }

    public function login() {
        if(Auth::user()) return redirect('/');
        return view('web/login');
    }

    public function postLogin(Request $request) {
        $this->validate($request, [
            'email' => 'required|string|email:rfc,dns|max:50',
            'password' => 'required|string',
        ]);

        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            return redirect()->intended('/');
        }

        return redirect()->back()->withInput($request->except('password'))
            ->with('error', 'Email atau Password anda salah');
    }

    public function daftar() {
        if(Auth::user()) return redirect('/');
        $kota = ['' => '=== Pilih Kota ===',
            'Jakarta' => 'Jakarta',
            'Bekasi' => 'Bekasi', 'Bogor' => 'Bogor', 'Tanggerang' => 'Tanggerang', 'Depok' => 'Depok', 'Bandung' => 'Bandung', 'Lain-Lain' => 'Lain-Lain'];
        $jenkel = ['' => '=== Pilih Jenis Kelamin ===', 'L' => 'Laki-Laki', 'P' => 'Perempuan'];
        return view('web/daftar-baru', compact('kota', 'jenkel'));
    }

    public function postDaftar(Request $request) {
        $this->validate($request, [
            'nama_lengkap' => 'required|string|max:50',
            'email' => 'required|string|email:rfc,dns|max:50|unique:users',
            'password' => 'required|string|between:5,10|confirmed',
            'alamat' => 'required',
            'kota' => 'required',
            'kodepos' => 'required|string|size:5',
            'no_telpon' => 'required|string|between:5,15|unique:users',
            'jenkel' => 'required',
        ]);

        $password = Hash::make($request->password);
        $request->merge(['password' => $password]);

        $request->request->add(['verifikasi_token' => Str::random(64)]);
        $model = User::create($request->all());

        Auth::login($model);

        session()->flash('sukses', 'Selamat akun anda telah berhasil didaftarkan');
        return redirect()->route('beranda');
    }

    public function logout() {
        Auth::logout();
        return redirect()->route('custom.login');
    }

    public function detailKamar($id) {
        $kamar = Kamar::with('kategori_kamar')->findOrFail($id);
        $kategoriKamarId = $kamar->kategori_kamar_id;
        $listKeterangan = KategoriKamar::find($kategoriKamarId)->listKamarKeterangan;
        return view('web/detail-kamar', compact('kamar', 'listKeterangan'));
    }
}
