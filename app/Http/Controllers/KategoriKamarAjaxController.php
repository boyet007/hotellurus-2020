<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\KategoriKamar;
use App\KamarKeterangan;
use App\Satuan;
use DataTables;
use Illuminate\Support\Facades\View;

class KategoriKamarAjaxController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   $title = 'Kategori Kamar';
        return view ('admin.kategori.kategori-kamar.index', compact('title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new KategoriKamar;
        return view('admin.kategori.kategori-kamar.form', compact('model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->merge(['harga_permalam' => $request->hidden_harga_permalam]);

        $model = new KategoriKamar();
        $this->validate($request, [
            'nama' => 'required|string|max:50|unique:kategori_kamar',
            'harga_permalam' => 'required|min:0|numeric',
            'jumlah_dewasa' => 'required|min:0|numeric',
            'jumlah_anak' => 'required|min:0|numeric',
            'slug' => 'required|max:50|unique:kategori_kamar',
            'jumlah_bed' => 'required|min:0|numeric',
            'tabelListKeterangan' => 'required|array|min:1',
        ]);

        $listKeterangan = $request->input('tabelListKeterangan', []);

        $model = KategoriKamar::create($request->all());

        $arrayKeterangan = [];

        foreach($listKeterangan as $ket) {
            $arrayKeterangan[] = new KamarKeterangan([
                'kategori_kamar_id' => $model->id,
                'list_keterangan' => $ket
            ]);
        }

        $model->listKamarKeterangan()->saveMany($arrayKeterangan);
        return $model;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = KategoriKamar::findOrFail($id);
        return view('admin.kategori.kategori-kamar.tampil', compact('model'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = KategoriKamar::findOrFail($id);
        return view('admin.kategori.kategori-kamar.form', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->merge(['harga_permalam' => $request->hidden_harga_permalam]);

        $model = new KategoriKamar();
        $this->validate($request, [
            'nama' => 'required|string|max:50|unique:kategori_kamar,id,' . $id,
            'harga_permalam' => 'required|min:0|numeric',
            'jumlah_dewasa' => 'required|min:0|numeric',
            'jumlah_anak' => 'required|min:0|numeric',
            'jumlah_bed' => 'required|min:0|numeric',
            'slug' => 'required|max:50|unique:kategori_kamar,id,' . $id,
            'tabelListKeterangan' => 'required|array|min:1',
        ]);

        $listKeterangan = $request->input('tabelListKeterangan', []);
        $model = KategoriKamar::findOrFail($id);
        $model->update($request->all());

        $model->listKamarKeterangan()->where('kategori_kamar_id', '=', $model->id)->delete();

        $arrayKeterangan = [];

        foreach($listKeterangan as $ket) {
            $arrayKeterangan[] = new KamarKeterangan([
                'kategori_kamar_id' => $model->id,
                'list_keterangan' => $ket
            ]);
        }

        $model->listKamarKeterangan()->saveMany($arrayKeterangan);

        return $model;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = KategoriKamar::findOrFail($id);
        $tempModel = $model;
        $model->delete();
        $tempModel->listKamarKeterangan()->where('kategori_kamar_id', '=', $model->id)->delete();
    }

    public function dataTable()
    {
        $model = KategoriKamar::query();
        $satuan = Satuan::find(9)->nama;

        return DataTables::of($model)
        ->addColumn('aksi', function($model) {
                return view('layouts.aksi._aksi3-master', [
                    'model' => $model,
                    'url_show' => route('ajax-kategori-kamar.show', $model->id),
                    'url_edit' => route('ajax-kategori-kamar.edit', $model->id),
                    'url_destroy' => route('ajax-kategori-kamar.destroy', $model->id)
                ]);
        })
        ->addColumn('satuan', function() { return "mlm"; })
        ->rawColumns(['aksi', 'satuan'])
        ->make(true);
    }

    public function dapatkanInfoKategoriKamar(Request $request) {
        $id = $request->kategoriId;
        $kategoriKamar = KategoriKamar::find($id);
        return $kategoriKamar;
    }

    public function cariDetailKamar(Request $request) {
        $kategoriId = $request->kategoriId;
        $detailKamar = KategoriKamar::findOrFail($kategoriId)->listKamarKeterangan->toArray();
        return $detailKamar;
    }
}
