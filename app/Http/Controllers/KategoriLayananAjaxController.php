<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\KategoriLayanan;
use DataTables;

class KategoriLayananAjaxController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Kategori Layanan';
        return view ('admin.kategori.kategori-layanan.index', compact('title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new KategoriLayanan;
        return view('admin.kategori.kategori-layanan.form', compact('model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $model = new KategoriLayanan();
        $this->validate($request, [
            'nama' => 'required|string|max:50|unique:kategori-layanan'
        ]);

        $model = KategoriLayanan::create($request->all());
        return $model;    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = KategoriLayanan::findOrFail($id);
        return view('admin.kategori.kategori-layanan.tampil', compact('model'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = KategoriLayanan::findOrFail($id);
        return view('admin.kategori.kategori-layanan.form', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = new KategoriLayanan();
        $this->validate($request, [
            'nama' => 'required|string|max:50|unique:kategori-layanan,id,' . $id,
        ]);

        $model = KategoriLayanan::findOrFail($id);
        $model->update($request->all());
        return $model;  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = KategoriLayanan::findOrFail($id);
        $model->delete();
    }

    public function dataTable()
    {
        $model = KategoriLayanan::query();
        return DataTables::of($model)
        ->addColumn('aksi', function($model) {
                return view('layouts.aksi._aksi-master', [
                    'model' => $model,
                    'url_show' => route('ajax-kategori-layanan.show', $model->id),
                    'url_edit' => route('ajax-kategori-layanan.edit', $model->id),
                    'url_destroy' => route('ajax-kategori-layanan.destroy', $model->id)
                ]);
        })
        ->rawColumns(['aksi'])
        ->make(true);
    }
}
