<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kontak;
use App\Kamar;
use App\TransaksiKamar;
use stdClass;

class AdminAjaxController extends Controller
{
    public function tampilDashboard() {
        $title = 'Dashboard';
        $jumlahEmailBelumTerbaca = Kontak::where('status_baca', '=', 0)->count();
        $jumlahKamarKosong = Kamar::where('status', '=', 'available')->count();
        $jumlahKamarTerisi = Kamar::where('status', '=', 'booking')->count();

        $tahunIni = date('Y');
        $transaksiTahunIni = TransaksiKamar::whereYear('tanggal', $tahunIni)->get();
        $totalPendapatanTahunIni = 0;

        $listKamarKosong = Kamar::where('status', '=', 'available')->get();
        foreach($transaksiTahunIni as $trans) {
            $totalPerTransaksi = $trans->dapatkanTotalTransaksi();
            $totalPendapatanTahunIni += $totalPerTransaksi;
        }

        $transaksiKamarAktif = TransaksiKamar::with('user', 'detail_transaksi',  'detail_transaksi')->where('aktif', '=', 1)->get();

        //cari list user aktif
        $listUserAktif = [];
        $nomor = 1;
        foreach($transaksiKamarAktif as $transaksi) {
            foreach($transaksi->detail_transaksi as $detail) {
                $tipe = $detail->tipe;
                if ($tipe === 'kamar') {
                    $userAktif = new UserAktif();
                    $userAktif->nomor = $nomor;
                    $userAktif->namaLengkap = $transaksi->user->nama_lengkap;
                    $userAktif->telpon = $transaksi->user->no_telpon;
                    $userAktif->noKamar = $detail->nama;
                    $userAktif->lamaMenginap = $detail->qty;
                    $userAktif->satuan = $detail->satuan;
                    array_push($listUserAktif, $userAktif);
                }
            }
        }

        return view('admin.dashboard', compact('title', 'jumlahEmailBelumTerbaca', 'jumlahKamarKosong',
            'jumlahKamarTerisi', 'totalPendapatanTahunIni', 'listKamarKosong', 'listUserAktif'));
    }
}

class UserAktif {
    public $nomor;
    public $namaLengkap;
    public $telpon;
    public $noKamar;
    public $lamaMenginap;
    public $satuan;
}
