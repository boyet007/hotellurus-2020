<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Slider;
use Image;
use DataTables;

class MasterSliderAjaxController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Master Slider';
        return view ('admin.master.slider.index', compact('title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new Slider();
        return view('admin.master.slider.form', compact('model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $model = new Slider;
        $requestFile = $request->file;

        if($requestFile) {
            cekNamaFile($model, $requestFile);
        }

        $this->validate($request, [
            'tag' => 'required|string|max:50',
            'judul' => 'required|string|max:50',
            'file' => 'image|mimes:jpeg,png,jpg',
            'link' => 'required|string|max:50',
        ]);

        if($requestFile) {
            $namaGambar = $requestFile->getClientOriginalName();
            $lebarSlider = config('setting.GAMBAR_SLIDE_LEBAR');
            $tinggiSlider = config('setting.GAMBAR_SLIDE_TINGGI');

            $gambar = Image::make($request->file('file'))->resize($lebarSLider, $tinggiSlider)
            ->save(public_path('images/sliders') . '/' . $namaGambar);

            $request->request->add(['gambar' => $namaGambar]);
        }

        $model = Slider::create($request->all());       
        if($requestFile) $gambar->destroy();
        return $model;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = Slider::findOrFail($id);
        return view('admin.master.slider.tampil', compact('model'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Slider::findOrFail($id);
        return view('admin.master.slider.form', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->file) {
            $model = new Slider;
            cekNamaFile($model, $request->file);
            $this->validate($request, [
                'tag' => 'required|string|max:50',
                'judul' => 'required|string|max:50',
                'file' => 'image|mimes:jpeg,png,jpg',
                'link' => 'required|string|max:50',
            ]);

            if($request->nama_file_lama !== 'unknown.jpg' && file_exists(public_path('images/sliders/') . $request->nama_file_lama)) {
                unlink(public_path('images/sliders/') . $request->nama_file_lama);
            }

            $namaGambar = $request->file->getClientOriginalName();
            $lebarSLider = config('setting.GAMBAR_SLIDE_LEBAR');
            $tinggiSlider = config('setting.GAMBAR_SLIDE_TINGGI');
            $gambar = Image::make($request->file('file'))->resize($lebarSLider, $tinggiSlider)
                ->save(public_path('images/sliders') . '/' . $namaGambar);
            $request->request->add(['gambar' => $namaGambar]);
            $model = Slider::findOrFail($id);
            $model->update($request->all());
            $gambar->destroy();
        } else {
            $namaGambar = $request->nama_file_lama;
            $this->validate($request, [
                'tag' => 'required|string|max:50',
                'judul' => 'required|string|max:50',
                'link' => 'required|string|max:50',
            ]);
            $model = Slider::findOrFail($id);
            $request->request->add(['gambar' => $namaGambar]);
            $model->update($request->all());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Slider::findOrFail($id);
        $gambar = $model->gambar;
        $model->delete();

        if($gambar !== 'unknown.jpg' && file_exists(public_path('images/sliders/') . $gambar)) {
            unlink(public_path('images/sliders/') . $gambar);
        }
    }

    public function dataTable()
    {
        $model = Slider::query();
        
        return DataTables::of($model)
        ->addColumn('aksi', function($model) {
                return view('layouts.aksi._aksi3-master', [
                    'model' => $model,
                    'url_show' => route('ajax-master-slider.show', $model->id),
                    'url_edit' => route('ajax-master-slider.edit', $model->id),
                    'url_destroy' => route('ajax-master-slider.destroy', $model->id)
                ]);
        })
        
        ->rawColumns(['aksi', 'satuan'])
        ->make(true);
    }

    public function dapatkanDataSlider(Request $request) {
        $id = $request->sliderId;
        $slider = Slider::findOrFail($id);
        return $slider;
    }
}
