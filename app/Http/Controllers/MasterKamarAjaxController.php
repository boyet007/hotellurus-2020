<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kamar;
use App\KategoriKamar;
use DataTables;
use Illuminate\Support\Facades\View;
use Image;
use Javascript;

class MasterKamarAjaxController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Master Kamar';
        return view ('admin.master.kamar.index', compact('title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new Kamar;
        $listKategoriKamar = ['' => '=== Pilih Kategori Kamar ==='] + KategoriKamar::pluck('nama', 'id')->all();
        $listStatus = ['' => '=== Pilih Status Kamar ===', 'kosong' => 'kosong', 'terisi' => 'terisi', 'kotor' => 'kotor', 'tidak-dijual' => 'tidak-dijual'];
        return view('admin.master.kamar.form', compact('model', 'listKategoriKamar', 'listStatus'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $model = new Kamar;
        cekNamaFile($model, $request->file);

        $this->validate($request, [
            'kategori_kamar_id' => 'required',
            'nomor_kamar' => 'required|min:0|numeric|unique:kamar',
            'lantai' => 'required|min:0|numeric',
            'status' => 'required',
            'file' => 'required|image|mimes:jpeg,png,jpg|max:2048',
        ]);

        $namaGambar = $request->file->getClientOriginalName();
        $lebarKamar = config('setting.GAMBAR_KAMAR_LEBAR');
        $tinggiKamar = config('setting.GAMBAR_KAMAR_TINGGI');

        $gambar = Image::make($request->file('file'))->resize($lebarKamar, $tinggiKamar)
            ->save(public_path('images/kamar') . '/' . $namaGambar);

        $request->request->add(['gambar' => $namaGambar]);

        $model = Kamar::create($request->all());
        $gambar->destroy();

        return $model;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = Kamar::with('kategori_kamar')->find($id);
        return view('admin.master.kamar.tampil', compact('model'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Kamar::findOrFail($id);
<<<<<<< HEAD
        $listKategoriKamar = ['' => '=== Pilih Kategori Kamar ==='] + KategoriKamar::pluck('nama', 'id')->all();
        $listStatus = ['' => '=== Pilih Status Kamar ===', 'kosong' => 'kosong', 'terisi' => 'terisi', 'kotor' => 'kotor', 'tidak-dijual' => 'tidak-dijual'];

        return view('admin.master.kamar.form', compact(['model', 'listKategoriKamar', 'listStatus']));
=======
        $test = 'Asik besok ngewek ama lingling';
        $listKategoriKamar = ['' => '=== Pilih Kategori Kamar ==='] + KategoriKamar::pluck('nama', 'id')->all();
        $listStatus = ['' => '=== Pilih Status Kamar ===', 'kosong' => 'kosong', 'terisi' => 'terisi', 'kotor' => 'kotor', 'tidak-dijual' => 'tidak-dijual'];

        return view('admin.master.kamar.form', compact(['model', 'listKategoriKamar', 'listStatus', 'test']));
 
>>>>>>> master
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->file) {
            $model = new Kamar;
            cekNamaFile($model, $request->file);
            $this->validate($request, [
                'kategori_kamar_id' => 'required',
                'nomor_kamar' => 'required|min:0|numeric|unique:kamar,id,' . $id,
                'lantai' => 'required|min:0|numeric',
                'status' => 'required',
                'file' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            ]);

            if($request->nama_file_lama !== 'room-unknown.jpg' && file_exists(public_path('images/kamar/') . $request->nama_file_lama)) {
                unlink(public_path('images/kamar/') . $request->nama_file_lama);
            }

            $namaGambar = $request->file->getClientOriginalName();
            $lebarKamar = config('setting.GAMBAR_KAMAR_LEBAR');
            $tinggiKamar = config('setting.GAMBAR_KAMAR_TINGGI');
            $gambar = Image::make($request->file('file'))->resize($lebarKamar, $tinggiKamar)
                ->save(public_path('images/kamar') . '/' . $namaGambar);
            $request->request->add(['gambar' => $namaGambar]);
            $model = Kamar::findOrFail($id);
            $model->update($request->all());
            $gambar->destroy();
        } else {
            $namaGambar = $request->nama_file_lama;
            $this->validate($request, [
                'kategori_kamar_id' => 'required',
                'nomor_kamar' => 'required|min:0|numeric|unique:kamar,id,' . $id,
                'lantai' => 'required|min:0|numeric',
                'status' => 'required',
            ]);
            $model = Kamar::findOrFail($id);
            $request->request->add(['gambar' => $namaGambar]);
            $model->update($request->all());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Kamar::findOrFail($id);
        $gambar = $model->gambar;
        $model->delete();

        if($gambar !== 'room-unknown.jpg' && file_exists(public_path('images/kamar/') . $gambar)) {
            unlink(public_path('images/kamar/') . $gambar);
        }
    }

    public function dataTable()
    {
        $model = Kamar::with('kategori_kamar')->orderBy('nomor_kamar', 'ASC')->select('kamar.*');

        return DataTables::of($model)
        ->addColumn('kategori_kamar', function($model) {
        return $model->kategori_kamar->nama;
        })
        ->addColumn('harga', function($model) {
            return $model->kategori_kamar->harga_permalam;
        })
        ->addColumn('aksi', function($model) {
<<<<<<< HEAD
                return view('layouts.aksi._aksi2-master', [
=======
                return view('layouts.aksi._aksi-master-2', [
>>>>>>> master
                    'model' => $model,
                    'url_show' => route('ajax-master-kamar.show', $model->id),
                    'url_edit' => route('ajax-master-kamar.edit', $model->id),
                    'url_destroy' => route('ajax-master-kamar.destroy', $model->id)
                ]);
        })
        ->rawColumns(['kategori_kamar', 'harga', 'aksi'])
        ->make(true);
    }

<<<<<<< HEAD
    public function dapatkanDataKamar(Request $request) {
        $id = $request->kamarId;
        $kamar = Kamar::with('kategori_kamar', 'kategori_kamar.satuan')->find($id);

        return $kamar;
=======
    public function testingLaravelJavascript() {
        Javascript::put([
            'testVariable' => 'test test test'
        ]);
        return view('test-javascript');
>>>>>>> master
    }
}
