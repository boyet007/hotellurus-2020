<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Layanan;
use App\KategoriLayanan;
use App\Satuan;
use DataTables;
use Image;
use Illuminate\Support\Facades\View;


class MasterLayananAjaxController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Master Layanan';
        return view ('admin.master.layanan.index', compact('title'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new Layanan;
        $listKategoriLayanan = ['' => '=== Pilih Kategori ==='] + KategoriLayanan::pluck('nama', 'id')->all();
        $listSatuan = ['' => '=== Pilih Satuan ==='] + Satuan::pluck('nama', 'id')->all();
        return view('admin.master.layanan.form', compact('model', 'listKategoriLayanan', 'listSatuan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $model = new Layanan;
        $requestFile = $request->file;

        if($requestFile) {
            cekNamaFile($model, $requestFile);
        }


        $request->merge(['harga' => $request->hidden_harga]);

        $this->validate($request, [
            'kategori_layanan_id' => 'required',
            'kode' => 'required|string|max:3|unique:layanan',
            'nama' => 'required|string|max:50',
            'harga' => 'required|min:0|numeric',
            'satuan_id' => 'required',
            'file' => 'image|mimes:jpeg,png,jpg',
        ]);

        if($requestFile) {
            $namaGambar = $requestFile->getClientOriginalName();
            $lebarKamar = config('setting.GAMBAR_LAYANAN_LEBAR');
            $tinggiKamar = config('setting.GAMBAR_LAYANAN_TINGGI');

            $gambar = Image::make($request->file('file'))->resize($lebarKamar, $tinggiKamar)
            ->save(public_path('images/layanan') . '/' . $namaGambar);

            $request->request->add(['gambar' => $namaGambar]);
        }

        $model = Layanan::create($request->all());
        if($requestFile) $gambar->destroy();
        return $model;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = Layanan::with('kategori_layanan', 'satuan')->find($id);
        return view('admin.master.layanan.tampil', compact('model'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Layanan::findOrFail($id);
        $listKategoriLayanan = ['' => '=== Pilih Kategori ==='] + KategoriLayanan::pluck('nama', 'id')->all();
        $listSatuan = ['' => '=== Pilih Satuan ==='] + Satuan::pluck('nama', 'id')->all();
        return view('admin.master.layanan.form', compact(['model', 'listKategoriLayanan', 'listSatuan']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->merge(['harga' => $request->hidden_harga]);
        if($request->file) {
            $model = new Layanan;
            cekNamaFile($model, $request->file);
            $this->validate($request, [
                'kategori_layanan_id' => 'required',
                'kode' => 'required|string|max:3|unique:layanan,id,' . $id,
                'nama' => 'required|string|max:50',
                'harga' => 'required|min:0|numeric',
                'satuan_id' => 'required',
                'file' => 'image|mimes:jpeg,png,jpg',
            ]);

            if($request->nama_file_lama !== 'unknown.jpg' && file_exists(public_path('images/layanan/') . $request->nama_file_lama)) {
                unlink(public_path('images/layanan/') . $request->nama_file_lama);
            }

            $namaGambar = $request->file->getClientOriginalName();
            $lebarLayanan = config('setting.GAMBAR_LAYANAN_LEBAR');
            $tinggiLayanan = config('setting.GAMBAR_LAYANAN_TINGGI');
            $gambar = Image::make($request->file('file'))->resize($lebarLayanan, $tinggiLayanan)
                ->save(public_path('images/layanan') . '/' . $namaGambar);
            $request->request->add(['gambar' => $namaGambar]);
            $model = Layanan::findOrFail($id);
            $model->update($request->all());
            $gambar->destroy();
        } else {
            $namaGambar = $request->nama_file_lama;
            $this->validate($request, [
                'kategori_layanan_id' => 'required',
                'nama' => 'required|string|max:50|unique:layanan,id,' . $id,
                'harga' => 'required|min:0|numeric',
                'satuan_id' => 'required',
            ]);
            $model = Layanan::findOrFail($id);
            $request->request->add(['gambar' => $namaGambar]);
            $model->update($request->all());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Layanan::findOrFail($id);
        $gambar = $model->gambar;
        $model->delete();

        if($gambar !== 'unknown.jpg' && file_exists(public_path('images/layanan/') . $gambar)) {
            unlink(public_path('images/layanan/') . $gambar);
        }
    }

    public function dataTable()
    {
        $model = Layanan::with('kategori_layanan', 'satuan')->select('layanan.*');

        return DataTables::of($model)
        ->addColumn('kategori_layanan', function($model) {
        return $model->kategori_layanan->nama;
        })
        ->addColumn('satuan', function($model) {
            return $model->satuan->nama;
        })
        ->addColumn('aksi', function($model) {
                return view('layouts.aksi._aksi2-master', [
                    'model' => $model,
                    'url_show' => route('ajax-master-layanan.show', $model->id),
                    'url_edit' => route('ajax-master-layanan.edit', $model->id),
                    'url_destroy' => route('ajax-master-layanan.destroy', $model->id)
                ]);
        })
        ->rawColumns(['kategori_layanan', 'satuan', 'aksi'])
        ->make(true);
    }

    public function dapatkanDataLayanan(Request $request) {
        $id = $request->layananId;
        $layanan = Layanan::with('kategori_layanan', 'satuan')->findOrFail($id);
        return $layanan;
    }
}
