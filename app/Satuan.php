<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Satuan extends Model
{
    protected $table = 'satuan';
    protected $fillable = ['kode', 'nama'];
    public $timestamps = false;

    public function kamar()
    {
        return $this->hasMany(Kamar::class);
    }

    public function layanan() {
        return $this->hasMany(Layanan::class);
    }
}
