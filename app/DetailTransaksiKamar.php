<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailTransaksiKamar extends Model
{
    protected $table = 'detail_transaksi_kamar';
    protected $fillable = ['tipe', 'kategori', 'nama', 'harga', 'qty', 'satuan'];

    public function transaksi_kamar() {
        return $this->belongsTo(TransaksiKamar::class);
    }
}
