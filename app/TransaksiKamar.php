<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransaksiKamar extends Model
{
    protected $table = 'transaksi_kamar';
    protected $fillable = ['notrans', 'tanggal', 'user_id', 'keterangan', 'tipe_bayar', 'admin_id'];

    public function detail_transaksi() {
        return $this->hasMany(DetailTransaksiKamar::class);
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function dapatkanTotalTransaksi() {
        $id = $this->id;
        $detailTransaksi = $this->detail_transaksi;
        $total = 0;

        foreach ($detailTransaksi as $detail) {
            $harga = $detail->harga;
            $qty = $detail->qty;

            $total += $harga * $qty;
        }

        return $total;
    }

    public function dapatkanNamaAdmin() {
        $adminId = $this->admin_id;
        return User::where('id', $adminId)->first()->nama_lengkap;
    }
}
