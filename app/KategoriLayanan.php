<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KategoriLayanan extends Model
{
    protected $table = 'kategori-layanan';
    protected $fillable = ['nama', 'keterangan'];
    public $timestamps = false;

    public function layanan() 
    {
        return $this->hasMany(Layanan::class);
    }
}
