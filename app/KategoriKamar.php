<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KategoriKamar extends Model
{
    protected $table = 'kategori_kamar';
    protected $fillable = ['nama', 'harga_permalam', 'jumlah_dewasa', 'jumlah_anak', 'jumlah_bed', 'slug', 'keterangan'];
    public $timestamps = false;

    public function kamar()
    {
        return $this->hasMany(Kamar::class);
    }

    public function satuan() {
        return $this->belongsTo(Satuan::class);
    }

    public function listKamarKeterangan() {
        return $this->hasMany(KamarKeterangan::class);
    }
}
