<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;
use App\KategoriKamar;
use App\Kontak;
use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        Blade::directive('formatUang', function ($money) {
            return "<?php echo number_format($money, 2); ?>";
        });

        View::composer(['layouts.web'], function($view) {
            $kategoriKamar = KategoriKamar::all();
            $view->with('kategoriKamar', $kategoriKamar);
        });

        View::composer(['layouts.admin'], function($view, $BELUM_TERBACA = 0) {
            $jumlahKontakBelumTerbaca = Kontak::where('status_baca', '=', $BELUM_TERBACA)->count();
            $view->with('jumlahKontakBelumTerbaca', $jumlahKontakBelumTerbaca);
        });
    }
}
