<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Layanan extends Model
{
    protected $table = 'layanan';
    protected $fillable = ['kategori_layanan_id', 'kode', 'nama', 'harga', 'satuan_id', 'gambar', 'keterangan'];
    public $timestamps = false;

    public function kategori_layanan()
    {
        return $this->belongsTo(KategoriLayanan::class);
    }

    public function satuan() {
        return $this->belongsTo(Satuan::class);
    }
}
