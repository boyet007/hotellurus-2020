<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KamarKeterangan extends Model
{
    protected $table = 'kamar_keterangan';
    protected $fillable = ['kategori_kamar_id', 'list_keterangan'];

public function kategoriKamar() {
        return $this->belongsTo(KategoriKamar::class);
    }
}
