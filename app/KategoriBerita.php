<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KategoriBerita extends Model
{
    protected $table = 'kategori_berita';
    protected $fillable = ['nama'];
    public $timestamps = false;

    public function berita() 
    {
        return $this->hasMany(Berita::class);
    }
}
