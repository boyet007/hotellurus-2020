<?php
    function formatTanggal($tanggal) {
        return date("d/m/Y", strtotime($tanggal));
    }

    function formatTanggalStrip($tanggal) {
        return date("d-m-Y", strtotime($tanggal));
    }
?>