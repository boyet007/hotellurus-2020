<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(SliderSeeder::class);
        $this->call(SatuanSeeder::class);
        $this->call(KategoriKamarSeeder::class);
        $this->call(KategoriLayananSeeder::class);
        $this->call(KamarSeeder::class);
        $this->call(LayananSeeder::class);
        $this->call(KontakSeeder::class);
        $this->call(TransaksiKamarSeeder::class);
    }
}
