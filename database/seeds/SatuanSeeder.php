<?php

use Illuminate\Database\Seeder;
use App\Satuan;

class SatuanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Satuan::truncate();

        $satuan = new Satuan;
        $satuan->kode             = 'Kg';
        $satuan->nama             = 'Kilogram';
        $satuan->save();

        $satuan = new Satuan;
        $satuan->kode             = 'G';
        $satuan->nama             = 'Gram';
        $satuan->save();

        $satuan = new Satuan;
        $satuan->kode             = 'Pcs';
        $satuan->nama             = 'Piesces';
        $satuan->save();

        $satuan = new Satuan;
        $satuan->kode             = 'Btg';
        $satuan->nama             = 'Batang';
        $satuan->save();

        $satuan = new Satuan;
        $satuan->kode             = 'Ktk';
        $satuan->nama             = 'Kotak';
        $satuan->save();

        $satuan = new Satuan;
        $satuan->kode             = 'Prs';
        $satuan->nama             = 'Porsi';
        $satuan->save();

        $satuan = new Satuan;
        $satuan->kode             = 'Cup';
        $satuan->nama             = 'Cup';
        $satuan->save();

        $satuan = new Satuan;
        $satuan->kode             = 'Pkt';
        $satuan->nama             = 'Paket';
        $satuan->save();

        $satuan = new Satuan;
        $satuan->kode             = 'Mlm';
        $satuan->nama             = 'Malam';
        $satuan->save();

    }
}
