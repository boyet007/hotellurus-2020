<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();

        $user = new User;
        $user->nama_lengkap     = 'Philip Loa';
        $user->email            = 'boyet007@gmail.com';
        $user->password         = bcrypt('Wynne321');
        $user->alamat           = 'Jl kresna no. 188 Pondok Gede';
        $user->kota             = 'Bekasi';
        $user->kodepos          = '13910';
        $user->no_telpon        = '8471337';
        $user->jenkel           = 'L';
        $user->role             = 'admin';
        $user->gambar           = 'philip.jpg';
        $user->save();

        $user = new User;
        $user->nama_lengkap     = 'Wynne Zong';
        $user->email            = 'wynne_side@gmail.com';
        $user->password         = bcrypt('Wynne321');
        $user->alamat           = 'Jl Keadilan no. 5';
        $user->kota             = 'Jakarta';
        $user->kodepos          = '16910';
        $user->no_telpon        = '8471338';
        $user->gambar           = 'wynne.jpg';
        $user->jenkel           = 'P';
        $user->save();
    }
}
