<?php

use Illuminate\Database\Seeder;
use App\Kamar;

class KamarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Kamar::truncate();

        $kamar = new Kamar;
        $kamar->kategori_kamar_id  = 1;
        $kamar->nomor_kamar = '201';
        $kamar->lantai = 2;
        $kamar->gambar = 'double_room_1.jpg';
        $kamar->status = 'booking';
        $kamar->save();

        $kamar = new Kamar;
        $kamar->kategori_kamar_id  = 1;
        $kamar->nomor_kamar = '202';
        $kamar->lantai = 2;
        $kamar->status = 'booking';
        $kamar->gambar = 'double_room_2.jpg';
        $kamar->save();

        $kamar = new Kamar;
        $kamar->kategori_kamar_id  = 1;
        $kamar->nomor_kamar = '203';
        $kamar->lantai = 2;
        $kamar->gambar = 'double_room_1.jpg';
        $kamar->save();

        $kamar = new Kamar;
        $kamar->kategori_kamar_id  = 2;
        $kamar->nomor_kamar = '204';
        $kamar->lantai = 2;
        $kamar->gambar = 'standar_twin_room.jpg';
        $kamar->save();

        $kamar = new Kamar;
        $kamar->kategori_kamar_id  = 2;
        $kamar->nomor_kamar = '205';
        $kamar->lantai = 2;
        $kamar->gambar = 'standar_twin_room2.jpg';
        $kamar->save();

        $kamar = new Kamar;
        $kamar->kategori_kamar_id  = 3;
        $kamar->nomor_kamar = '301';
        $kamar->lantai = 3;
        $kamar->gambar = 'family_room.jpg';
        $kamar->save();

        $kamar = new Kamar;
        $kamar->kategori_kamar_id  = 3;
        $kamar->nomor_kamar = '302';
        $kamar->lantai = 3;
        $kamar->gambar = 'family_room2.jpg';
        $kamar->save();

        $kamar = new Kamar;
        $kamar->kategori_kamar_id  = 4;
        $kamar->nomor_kamar = '303';
        $kamar->lantai = 3;
        $kamar->gambar = 'vip.jpg';
        $kamar->save();

        $kamar = new Kamar;
        $kamar->kategori_kamar_id  = 5;
        $kamar->nomor_kamar = '303';
        $kamar->lantai = 3;
        $kamar->gambar = 'villa_inti.jpg';
        $kamar->save();
    }
}
