<?php

use Illuminate\Database\Seeder;
use App\TransaksiKamar;
use App\DetailTransaksiKamar;

class TransaksiKamarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TransaksiKamar::truncate();
        DetailTransaksiKamar::truncate();

        $transaksi = new TransaksiKamar;
        $transaksi->notrans = '2020/HL/001';
        $transaksi->tanggal = date('2020/07/25');
        $transaksi->user_id = 2;
        $transaksi->keterangan = 'Tidak ada';
        $transaksi->tipe_bayar = 'tunai';
        $transaksi->admin_id = 1;
        $transaksi->save();
        $detail = array();
        $detailTransaksiKamar = new DetailTransaksiKamar;
        $detailTransaksiKamar->transaksi_kamar_id = $transaksi->id;
        $detailTransaksiKamar->tipe = 'kamar';
        $detailTransaksiKamar->kategori = 'Standar Double Room';
        $detailTransaksiKamar->nama = '201';
        $detailTransaksiKamar->harga = 400000;
        $detailTransaksiKamar->qty = 1;
        $detailTransaksiKamar->satuan = 'mlm';
        array_push($detail, $detailTransaksiKamar);
        $detailTransaksiKamar2 = new DetailTransaksiKamar;
        $detailTransaksiKamar2->transaksi_kamar_id = $transaksi->id;
        $detailTransaksiKamar2->tipe = 'layanan';
        $detailTransaksiKamar2->kategori = 'Standar Double Room';
        $detailTransaksiKamar2->nama = 'Nasi Goreng Spesial';
        $detailTransaksiKamar2->harga = 35000;
        $detailTransaksiKamar2->qty = 2;
        $detailTransaksiKamar2->satuan = 'mlm';
        array_push($detail, $detailTransaksiKamar2);
        $transaksi->detail_transaksi()->saveMany($detail);

        $transaksi = new TransaksiKamar;
        $transaksi->notrans = '2020/HL/002';
        $transaksi->tanggal = date('2020/07/25');
        $transaksi->user_id = 2;
        $transaksi->keterangan = 'Tidak ada';
        $transaksi->tipe_bayar = 'tunai';
        $transaksi->admin_id = 1;
        $transaksi->save();
        $detail = array();
        $detailTransaksiKamar = new DetailTransaksiKamar;
        $detailTransaksiKamar->transaksi_kamar_id = $transaksi->id;
        $detailTransaksiKamar->tipe = 'kamar';
        $detailTransaksiKamar->kategori = 'Standar Double Room';
        $detailTransaksiKamar->nama = '202';
        $detailTransaksiKamar->harga = 400000;
        $detailTransaksiKamar->qty = 1;
        $detailTransaksiKamar->satuan = 'mlm';
        array_push($detail, $detailTransaksiKamar);
        $transaksi->detail_transaksi()->saveMany($detail);
    }
}
