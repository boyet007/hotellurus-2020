<?php

use Illuminate\Database\Seeder;
use App\Slider;

class SliderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Slider::truncate();

        $slider = new Slider;
        $slider->tag             = 'Complete Facilities You Want';
        $slider->judul           = 'Complete Facilities';
        $slider->gambar          = 'kolam_renang.jpg';
        $slider->link            = 'galeri';
        $slider->save();

        $slider = new Slider;
        $slider->tag             = 'View Fresh Mountain From Room';
        $slider->judul           = 'Mountain View Room';
        $slider->gambar          = 'mountain_room.jpg';
        $slider->link            = 'galeri';
        $slider->save();

        $slider = new Slider;
        $slider->tag             = 'Dinner in the night with Exclusive Buffet';
        $slider->judul           = 'Dinner in the moon';
        $slider->gambar          = 'restoran.jpg';
        $slider->link            = 'amenities';
        $slider->save();

        $slider = new Slider;
        $slider->tag             = 'Luxury Bed with Love Theme';
        $slider->judul           = 'Luxury Loves Bed';
        $slider->gambar          = 'double_bed.jpg';
        $slider->link            = 'kamar';
        $slider->save();
    }
}
