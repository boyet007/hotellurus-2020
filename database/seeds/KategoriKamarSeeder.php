<?php

use Illuminate\Database\Seeder;
use App\KategoriKamar;
use App\KamarKeterangan;


class KategoriKamarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        KategoriKamar::truncate();
        KamarKeterangan::truncate();

        $kategoriKamar = new KategoriKamar;
        $kategoriKamar->nama               = 'Standar Double Room';
        $kategoriKamar->harga_permalam     =  400000;
        $kategoriKamar->jumlah_bed         = 1;
        $kategoriKamar->slug               = 'standar_double_room';
        $kategoriKamar->save();

        $listKeterangan = [];
        $listKeterangan[] = new KamarKeterangan([
            'kategori_kamar_id' => $kategoriKamar->id,
            'list_keterangan' => 'Tempat tidur double'
        ]);
        $listKeterangan[] = new KamarKeterangan([
            'kategori_kamar_id' => $kategoriKamar->id,
            'list_keterangan' => 'Pemandangan Gunung'
        ]);
        $listKeterangan[] = new KamarKeterangan([
            'kategori_kamar_id' => $kategoriKamar->id,
            'list_keterangan' => 'Breakfast setiap pagi'
        ]);
        $kategoriKamar->listKamarKeterangan()->saveMany($listKeterangan);

        $kategoriKamar = new KategoriKamar;
        $kategoriKamar->nama               = 'Standar Twin Room';
        $kategoriKamar->harga_permalam     =  400000;
        $kategoriKamar->jumlah_bed         = 2;
        $kategoriKamar->slug               = 'standar_twin_room';
        $kategoriKamar->save();
        $listKeterangan = [];
        $listKeterangan[] = new KamarKeterangan([
            'kategori_kamar_id' => $kategoriKamar->id,
            'list_keterangan' => 'Tempat tidur single bed'
        ]);
        $listKeterangan[] = new KamarKeterangan([
            'kategori_kamar_id' => $kategoriKamar->id,
            'list_keterangan' => 'Pemandangan Gunung'
        ]);
        $listKeterangan[] = new KamarKeterangan([
            'kategori_kamar_id' => $kategoriKamar->id,
            'list_keterangan' => 'Breakfast setiap pagi'
        ]);
        $kategoriKamar->listKamarKeterangan()->saveMany($listKeterangan);

        $kategoriKamar = new KategoriKamar;
        $kategoriKamar->nama               = 'Family Room';
        $kategoriKamar->harga_permalam     =  700000;
        $kategoriKamar->jumlah_dewasa      = 3;
        $kategoriKamar->jumlah_anak        = 2;
        $kategoriKamar->jumlah_bed         = 2;
        $kategoriKamar->slug               = 'family_room';
        $kategoriKamar->save();
        $listKeterangan = [];
        $listKeterangan[] = new KamarKeterangan([
            'kategori_kamar_id' => $kategoriKamar->id,
            'list_keterangan' => 'Satu Double bed dan satu single bed'
        ]);
        $listKeterangan[] = new KamarKeterangan([
            'kategori_kamar_id' => $kategoriKamar->id,
            'list_keterangan' => 'Pemandangan gunung atau taman'
        ]);
        $listKeterangan[] = new KamarKeterangan([
            'kategori_kamar_id' => $kategoriKamar->id,
            'list_keterangan' => 'Breakfast setiap pagi'
        ]);
        $kategoriKamar->listKamarKeterangan()->saveMany($listKeterangan);

        $kategoriKamar = new KategoriKamar;
        $kategoriKamar->nama               = 'VIP';
        $kategoriKamar->harga_permalam     =  900000;
        $kategoriKamar->jumlah_bed         = 1;
        $kategoriKamar->slug               = 'vip';
        $kategoriKamar->save();
        $listKeterangan = [];
        $listKeterangan[] = new KamarKeterangan([
            'kategori_kamar_id' => $kategoriKamar->id,
            'list_keterangan' => 'Tempat tidur double'
        ]);
        $listKeterangan[] = new KamarKeterangan([
            'kategori_kamar_id' => $kategoriKamar->id,
            'list_keterangan' => 'Air Conditioner'
        ]);
        $listKeterangan[] = new KamarKeterangan([
            'kategori_kamar_id' => $kategoriKamar->id,
            'list_keterangan' => 'Breakfast setiap pagi'
        ]);
        $kategoriKamar->listKamarKeterangan()->saveMany($listKeterangan);

        $kategoriKamar = new KategoriKamar;
        $kategoriKamar->nama               = 'Vila Inti';
        $kategoriKamar->harga_permalam     =  4000000;
        $kategoriKamar->jumlah_dewasa      = 17;
        $kategoriKamar->jumlah_anak        = 7;
        $kategoriKamar->jumlah_bed         = 10;
        $kategoriKamar->slug               = 'villa_inti';
        $kategoriKamar->keterangan         = 'sedang diservis';
        $kategoriKamar->save();
        $listKeterangan = [];
        $listKeterangan[] = new KamarKeterangan([
            'kategori_kamar_id' => $kategoriKamar->id,
            'list_keterangan' => 'Cocok untuk orang banyak'
        ]);
        $listKeterangan[] = new KamarKeterangan([
            'kategori_kamar_id' => $kategoriKamar->id,
            'list_keterangan' => 'Pemandangan Gunung'
        ]);
        $listKeterangan[] = new KamarKeterangan([
            'kategori_kamar_id' => $kategoriKamar->id,
            'list_keterangan' => 'Dapur tersendiri'
        ]);
        $kategoriKamar->listKamarKeterangan()->saveMany($listKeterangan);
    }
}
