<?php

use Illuminate\Database\Seeder;
use App\Layanan;

class LayananSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Layanan::truncate();

        $layanan = new Layanan();
        $layanan->kategori_layanan_id = 1;
        $layanan->kode = 'NGS';
        $layanan->nama = 'Nasi Goreng Spesial';
        $layanan->harga = 25000;
        $layanan->satuan_id = 6;
        $layanan->gambar = 'nasi_goreng_spesial.jpg';
        $layanan->save();

        $layanan = new Layanan();
        $layanan->kategori_layanan_id = 4;
        $layanan->kode = 'ETM';
        $layanan->nama = 'Es Teh Manis';
        $layanan->harga = 5000;
        $layanan->satuan_id = 7;
        $layanan->gambar = 'es_teh_manis.jpg';
        $layanan->save();

        $layanan = new Layanan();
        $layanan->kategori_layanan_id = 2;
        $layanan->kode = 'SSA';
        $layanan->nama = 'Steak Sapi Arabian';
        $layanan->harga = 25000;
        $layanan->satuan_id = 6;
        $layanan->gambar = 'steak_sapi.jpg';
        $layanan->save();

        $layanan = new Layanan();
        $layanan->kategori_layanan_id = 2;
        $layanan->kode = 'SKK';
        $layanan->nama = 'Seafood Kepiting Kuah';
        $layanan->harga = 225000;
        $layanan->satuan_id = 6;
        $layanan->save();

        $layanan = new Layanan();
        $layanan->kategori_layanan_id = 6;
        $layanan->kode = 'JSH';
        $layanan->nama = 'Jasa Antar Kebandara Sukarno Hatta';
        $layanan->harga = 765000;
        $layanan->satuan_id = 8;
        $layanan->save();

        $layanan = new Layanan();
        $layanan->kategori_layanan_id = 1;
        $layanan->kode = 'RBA';
        $layanan->nama = 'Room Boy Addon';
        $layanan->harga = 50000;
        $layanan->satuan_id = 8;
        $layanan->save();

        $layanan = new Layanan();
        $layanan->kategori_layanan_id = 4;
        $layanan->kode = 'BIB';
        $layanan->nama = 'Bir Bintang';
        $layanan->harga = 25000;
        $layanan->satuan_id = 7;
        $layanan->gambar = 'bir_bintang.jpg';
        $layanan->save();

        $layanan = new Layanan();
        $layanan->kategori_layanan_id = 4;
        $layanan->kode = 'AQB';
        $layanan->nama = 'Aqua Botol';
        $layanan->harga = 10000;
        $layanan->satuan_id = 7;
        $layanan->save();
    }
}
