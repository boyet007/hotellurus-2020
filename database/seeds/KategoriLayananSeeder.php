<?php

use Illuminate\Database\Seeder;
use App\KategoriLayanan;

class KategoriLayananSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        KategoriLayanan::truncate();

        $kategori_layanan = new KategoriLayanan;
        $kategori_layanan->nama               = 'BreakFast';
        $kategori_layanan->save();

        $kategori_layanan = new KategoriLayanan;
        $kategori_layanan->nama               = 'Dinner';
        $kategori_layanan->save();

        $kategori_layanan = new KategoriLayanan;
        $kategori_layanan->nama               = 'Breverage';
        $kategori_layanan->save();

        $kategori_layanan = new KategoriLayanan;
        $kategori_layanan->nama               = 'Drink';
        $kategori_layanan->save();

        $kategori_layanan = new KategoriLayanan;
        $kategori_layanan->nama               = 'Meal';
        $kategori_layanan->save();

        $kategori_layanan = new KategoriLayanan;
        $kategori_layanan->nama               = 'Service Addon';
        $kategori_layanan->save();
    }
}
