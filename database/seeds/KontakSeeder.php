<?php

use Illuminate\Database\Seeder;
use App\Kontak;

class KontakSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Kontak::truncate();

        $kontak = new Kontak;
        $kontak->nama_lengkap = 'Adri Baharudin';
        $kontak->no_telpon = '829 1921';
        $kontak->email = 'adri@gmail.com';
        $kontak->subyek = 'Tanya harga kamar';
        $kontak->pesan = 'Tolong dikirim harga kamar untuk nanti malam';
        $kontak->save();

        $kontak = new Kontak;
        $kontak->nama_lengkap = 'Jefri Woworuntu';
        $kontak->no_telpon = '829 1922';
        $kontak->email = 'jefri@gmail.com';
        $kontak->subyek = 'Restoran ada ga ya';
        $kontak->pesan = 'Mas disana ada restoran ga ya??';
        $kontak->save();

        $kontak = new Kontak;
        $kontak->nama_lengkap = 'Muhammad Aman';
        $kontak->no_telpon = '829 1923';
        $kontak->email = 'amman@gmail.com';
        $kontak->subyek = 'Tanya harga kamar';
        $kontak->pesan = 'Untuk nanti malam ada kamar kosong ga ya';
        $kontak->save();

        $kontak = new Kontak;
        $kontak->nama_lengkap = 'Wenny';
        $kontak->no_telpon = '829 1924';
        $kontak->email = 'wenny@gmail.com';
        $kontak->subyek = 'Booking kamar ntar malam';
        $kontak->pesan = 'Mas, kalo ada yang kosong buat nanti malam tolong dikabari, makasi';
        $kontak->save();


        $kontak = new Kontak;
        $kontak->nama_lengkap = 'Saputra';
        $kontak->no_telpon = '829 1925';
        $kontak->email = 'saputra@gmail.com';
        $kontak->subyek = 'Kebersihan';
        $kontak->pesan = 'Mas tadi malam ada semut banyak dikamar kami, tolong lain kali dipilih kamar yang ga  banyak semut';
        $kontak->save();

    }
}
