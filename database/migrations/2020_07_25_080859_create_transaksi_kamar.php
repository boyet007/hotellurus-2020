<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransaksiKamar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksi_kamar', function (Blueprint $table) {
            $table->id();
            $table->string('notrans');
            $table->date('tanggal');
            $table->integer('user_id');
            $table->text('keterangan')->nullable();
            $table->string('tipe_bayar');
            $table->boolean('aktif')->default(1);
            $table->integer('admin_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksi_kamar');
    }
}
