<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKategoriKamarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kategori_kamar', function (Blueprint $table) {
            $table->id();
            $table->string('nama');
            $table->integer('harga_permalam');
            $table->integer('satuan_id')->default(9); //9 = permalam ditable satuan
            $table->smallInteger('jumlah_dewasa')->default(2);
            $table->smallInteger('jumlah_anak')->default(1);
            $table->smallInteger('jumlah_bed');
            $table->string('slug');
            $table->text('keterangan')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kategori_kamar');
    }
}
