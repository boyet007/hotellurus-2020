<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailTransaksiKamar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_transaksi_kamar', function (Blueprint $table) {
            $table->id();
            $table->integer('transaksi_kamar_id');
            $table->string('tipe');
            $table->string('kategori');
            $table->string('nama');
            $table->double('harga');
            $table->integer('qty');
            $table->string('satuan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_transaksi_kamar');
    }
}
