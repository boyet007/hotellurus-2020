Hotel Lurus
=========================

table booking :
- nomor_booking
- user_id
- tanggal_booking
- diskon
- keterangan

table detail_booking
- tipe
- nama
- kategori
- harga
- qty
- satuan
- keterangan

table transaksi_kamar
- tanggal
- user_id
- keterangan
- tipe_bayar (cash / debit / kartu kredit / lain-lain)
- admin_id

table detail_transaksi_kamar
- tipe_transaksi (kamar / layanan)
- harga_satuan
- jumlah
- keterangan



pixel images :
background image slider : 1920 x 900
galeri proyek : 500 x 700
admin photo avatar : 60 x 60
kamar photo : 800 x 507
layanan photo : 400 x 400
sliders photo : 1300 x 692


referensi :
https://duniamu38.blogspot.com/2018/11/aplikasi-reservasi-hotel-berbasis-web.html

crud :
https://www.itsolutionstuff.com/post/laravel-6-ajax-crud-tutorialexample.html



icon list
==========
https://mdbootstrap.com/docs/jquery/content/icons-list/


frontend template
===================

bootstrap template
===================
https://startbootstrap.com/previews/sb-admin-2/

icon list fa 
=============
https://mdbootstrap.com/docs/jquery/content/icons-list/

xdebug
=======
https://www.codewall.co.uk/debug-php-in-vscode-with-xdebug/
https://stackoverflow.com/questions/51685952/debugging-laravel-application-on-vscode --> setting json

xdebug launch json
=====================
{
    // Use IntelliSense to learn about possible attributes.
    // Hover to view descriptions of existing attributes.
    // For more information, visit: https://go.microsoft.com/fwlink/?linkid=830387
    "configurations": [
        {
            "name": "Launch Program",
            "type": "php",
            "request": "launch",
            "cwd": "${workspaceRoot}",
            "port" : 9000
        },
        {
            "name": "Launch localhost",
            "request": "launch",
            "url": "http://localhost/public",
            "webRoot": "${workspaceRoot}"
        }
    ]
}

xdebug file xampp
==================
[XDebug]
xdebug.remote_enable=1
xdebug.remote_host=127.0.0.1
xdebug.remote_port=9000
xdebug.remote_autostart=1
xdebug.remote_handler = dbgp
xdebug.remote_mode = req

faker list :
$factory->define(App\User::class, function (Faker $faker) {
    static $password;
    return [
        'kdusr' => 'usr' . str_pad($faker->unique()->numberBetween(1, 90), 2, '0', STR_PAD_LEFT),
        'kdkat' => KategoriUser::all()->random()->kdkat,
        'username' => $faker->username,
        'password' => $password ? : $password = app('hash')->make('secret'),
        'nama' => $faker->name,
        'email' => $faker->email,
        'alamat' => $faker->address,
        'kdkot' => Kota::all()->random()->kdkot,
        'kdpos' => Address::postcode(),
        'telp' => $faker->phoneNumber,
        'token' => str_random(10),
        'foto' => 'unknown.jpg',
        'kdbank' => Bank::all()->random()->kdbank,
        'no_rek' => $faker->numerify('###.####.####.###'),
        'nama_akun_bank' => $faker->name
    ];
});


Faker Factory
=====================

create di artiasn : php artisan make:factory StockFactory

$faker = Faker\Factory::create();
manggilnya lewat tinker : 
factory(App\User::class, 500)->create();


example jquery laravel pos sale 
=====================================
https://quickadminpanel.com/blog/master-detail-form-in-laravel-jquery-create-order-with-products/

access laravel server from other pc 
=========================================
sudo php artisan serve --host 192.168.1.101 --port 80


show images at domp pdf path
============================================
<img src="{{ public_path('admin/img/logo_baru.jpg') }}"  alt="Logo" class="logo"/>

margin invoice matrix
===========================================
margin: none


engineer template
=======================================
about : 1000 x 1280 
slide : 1900 x 1267
galeri : 500 x 700
tentang : 400 x 500
logo : 132 x 68

sisipkan gambar dari file config
===================================
<style="background-image: url('web/images/{{ config('situs.BERANDA_CAROUSEL_3_GAMBAR') }}')
<img src="{{ asset('web/images/mesin/' . config('situs.BERANDA_MESIN_1_GAMBAR')) }}" alt="Image" class="img-fluid">

format tanggal
=================
moment(data).format('DD-MM-YYYY'); -> javascript;

{ data: 'tanggal', name: 'tanggal', 
    render:function(data) {
         return moment(data).format('DD-MM-YYYY');
    }
},

kalo dari controller php :
$model_tanggal = $model->tanggal;
$tanggal = date('d-m-Y', strtotime($model_tanggal));

untuk mengambil tahun dari tahun ini dicontroller : $tahun = date('Y');

format uang index blade dari datatable:
=========================================
{ data: 'harga', render: $.fn.dataTable.render.number('.', ',', 2, 'Rp ')},

kalo javascript : 
formatRupiah('5000', 'Rp. ')


Eager Loading
================
https://stackoverflow.com/questions/20036269/query-relationship-eloquent



file .env
==========
APP_NAME=Laravel
APP_ENV=local
APP_KEY=
APP_DEBUG=true
APP_URL=http://localhost

LOG_CHANNEL=stack

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=laravel
DB_USERNAME=root
DB_PASSWORD=

BROADCAST_DRIVER=log
CACHE_DRIVER=file
QUEUE_CONNECTION=sync
SESSION_DRIVER=file
SESSION_LIFETIME=120

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379

MAIL_MAILER=smtp
MAIL_HOST=smtp.mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null
MAIL_FROM_ADDRESS=null
MAIL_FROM_NAME="${APP_NAME}"

AWS_ACCESS_KEY_ID=
AWS_SECRET_ACCESS_KEY=
AWS_DEFAULT_REGION=us-east-1
AWS_BUCKET=

PUSHER_APP_ID=
PUSHER_APP_KEY=
PUSHER_APP_SECRET=
PUSHER_APP_CLUSTER=mt1

MIX_PUSHER_APP_KEY="${PUSHER_APP_KEY}"
MIX_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"

vscode collapse code
=====================
Use Ctrl + K + 0 to fold all and Ctrl + K + J to unfold all. Right-click anywhere in document and select "format document" option. Then hover next to number lines and you will see the (-) sign for collapsing method.


Hotel Lurus
===============
background img : 1300 x 731
room img : 800 x 507
cover makanan : 1300 x 1300
menu mkaanan : 400 x 400
blog : 800 x 533
person : 724 x 704
armenities : 800 x 600
