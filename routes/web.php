<?php

use Illuminate\Support\Facades\Route;

Route::get('/', 'WebController@beranda')->name('beranda');
Route::get('/kamar/{slug}', 'WebController@kamar')->name('kamar');
Route::get('/detail-kamar/{id}', 'WebController@detailKamar')->name('detail.kamar');
Route::get('/tentang-kami', 'WebController@tentangKami')->name('tentang.kami');
Route::get('/amenities', 'WebController@amenities')->name('amenities');
Route::get('/berita', 'WebController@berita')->name('berita');
Route::get('/kontak-kami', 'WebController@kontakKami')->name('kontak.kami');
Route::post('/kontak-kami', 'WebController@postKontak')->name('post.kontak.kami');
Route::get('/login', 'WebController@login')->name('custom.login');
Route::get('/daftar-baru', 'WebController@daftar')->name('daftar.baru');
Route::post('/daftar-baru', 'WebController@postDaftar')->name('post.daftar.baru');
Route::post('/login', 'WebController@postLogin')->name('post.login');


Route::group(['middleware' => ['auth', 'checkRole:admin'], 'prefix' => 'admin'], function() {
    Route::get('dashboard', 'AdminAjaxController@tampilDashboard')->name('dashboard');

    Route::group(['prefix' => 'kategori'], function() {
        Route::resource('ajax-kategori-kamar','KategoriKamarAjaxController');
        Route::resource('ajax-kategori-layanan','KategoriLayananAjaxController');
    });

    Route::group(['prefix' => 'master'], function() {
        Route::resource('ajax-master-satuan','MasterSatuanAjaxController');
        Route::resource('ajax-master-kamar','MasterKamarAjaxController');
        Route::resource('ajax-master-layanan','MasterLayananAjaxController');
        Route::resource('ajax-master-user','MasterUserAjaxController');
        Route::resource('ajax-master-slider', 'MasterSliderAjaxController');
    });

    Route::group(['prefix' => 'transaksi'], function() {
        Route::resource('ajax-master-transaksi-kamar', 'MasterTransaksiKamarAjaxController');
    });

    Route::group(['prefix' => 'kontak'], function() {
        Route::resource('ajax-kontak', 'KontakAjaxController');
    });

    //table
    Route::get('/ajax/tabel-ajax-kategori-kamar', 'KategoriKamarAjaxController@dataTable')->name('table.ajax.kategori.kamar');
    Route::get('/ajax/tabel-ajax-kategori-layanan', 'KategoriLayananAjaxController@dataTable')->name('table.ajax.kategori.layanan');
    Route::get('/ajax/tabel-ajax-satuan', 'MasterSatuanAjaxController@dataTable')->name('table.ajax.satuan');
    Route::get('/ajax/tabel-ajax-kamar', 'MasterKamarAjaxController@dataTable')->name('table.ajax.master.kamar');
    Route::get('/ajax/tabel-ajax-layanan', 'MasterLayananAjaxController@dataTable')->name('table.ajax.master.layanan');
    Route::get('/ajax/tabel-ajax-slider', 'MasterSliderAjaxController@dataTable')->name('table.ajax.master.slider');
    Route::get('/ajax/tabel-ajax.user', 'MasterUserAjaxController@dataTable')->name('table.ajax.master.user');
    Route::get('/ajax/tabel-ajax-kontak', 'KontakAjaxController@dataTable')->name('table.ajax.kontak');
    Route::get('/ajax/tabel-ajax-transaksi-kamar', 'MasterTransaksiKamarAjaxController@dataTable')->name('table.ajax.transaksi.kamar');

    //lain-lain
    Route::post('/dapatkan-info-kategori-kamar', 'KategoriKamarAjaxController@dapatkanInfoKategoriKamar');
    Route::post('/dapatkan-data-kamar', 'MasterKamarAjaxController@dapatkanDataKamar');
    Route::post('/dapatkan-data-layanan', 'MasterLayananAjaxController@dapatkanDataLayanan');
    Route::post('/dapatkan-data-user', 'MasterUserAjaxController@dapatkanDataUser');
    Route::post('/dapatkan-data-slider', 'MasterSliderAjaxController@dapatkanDataSlider');
    Route::post('/cari-detail-kamar', 'KategoriKamarAjaxController@cariDetailKamar');
    Route::post('/dapatkan-data-tipe-transaksi', 'MasterTransaksiKamarAjaxController@dapatkanDataTipeTransaksi');
    Route::post('/validasi-detail-transaksi', 'MasterTransaksiKamarAjaxController@validasiDetailTransaksi');
    Route::post('/dapatkan-data-transaksi-user', 'MasterTransaksiKamarAjaxController@dapatkanDataTransaksiUser');
    Route::post('/dapatkan-data-transaksi-detail', 'MasterTransaksiKamarAjaxController@dapatkanDataTransaksiDetail');

    //print
    Route::get('/cetak-transaksi-kamar/{id}/cetak-deskjet', 'MasterTransaksiKamarAjaxController@cetakDeskjet')->name('ajax-master-transaksi-kamar.cetak.deskjet');
    Route::get('/testing-laravel-javascript', 'MasterKamarAjaxController@testingLaravelJavascript');
});



Route::group(['middleware' => ['auth']], function() {
    Route::get('/logout', 'WebController@logout')->name('logout');
});

Route::get('/test', function () {
    $x = 1;
    $y = 2;
    $z = 3;
    return 'test';
});

Route::get('tanggal', function() {
    $date = date("D M d, Y G:i");
    return formatTanggal($date);
});
