<table class="table table-borderless tabel-master-show">
    <tbody>
        <tr>
            <td>Nama Lengkap</td>
            <td>:</td>
            <td>{{ $model->nama_lengkap }}</td>
        </tr>
        <tr>
            <td>No Telpon</td>
            <td>:</td>
            <td>{{ $model->no_telpon }}</td>
        </tr>
        <tr>
            <td>Email</td>
            <td>:</td>
            <td>{{ $model->email }}</td>
        </tr>
        <tr>
            <td>Subyek</td>
            <td>:</td>
            <td>{{ $model->subyek }}</td>
        </tr>
        <tr>
            <td>Pesan</td>
            <td>:</td>
            <td>{{ $model->pesan }}</td>
        </tr>
    </tbody>
</table>
