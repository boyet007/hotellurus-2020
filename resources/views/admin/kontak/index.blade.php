@extends('layouts.admin')
@section('title', 'Data Kategori Kamar')

@section('content')
<div class="row">
    <div class="col-md-10">
        <div class="card border-danger">
            <div class="card-header">
                <h3 class="card-title">{{ $title }}</h3>
                <div class="card-tools">
                    <a class="btn btn-sm btn-success btn-tampil-modal" data-toggle="modal" tipe="tambah"
                        judul="Tambah Kategori Kamar Baru"
                    href="{{ route('ajax-kategori-kamar.create') }}">
                        <i class="fa fa-plus"></i></a>
                </div>
            </div>
            <div class="card-body">
                <table id="dataTable" class="table table-sm table-hover tabel-kategori-kamar">
                    <thead>
                        <tr>
                            <th width="30">Nama</th>
                            <th>Email</th>
                            <th>No Telpon</th>
                            <th>Subyek</th>
                            <th>Status</th>
                            <th>Waktu</th>
                            <th width="50">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function() {
            $('#dataTable').DataTable({
            autoWidth: false,
            serverSide: true,
            processing: true,
            responsive: true,
            ajax: "{{ route('table.ajax.kontak') }}",
            columns: [
                { data: 'nama_lengkap', name: 'nama_lengkap' },
                { data: 'email', nama: 'email' },
                { data: 'no_telpon', nama: 'no_telpon' },
                { data: 'subyek', nama: 'subyek' },
                { data: 'status_baca', nama: 'status_baca', render:function(data, tipe, row) {
                    if (data === 0) { return '<b>belum terbaca</b>'; } else { return 'sudah terbaca'; }
                }},
                { data: 'created_at', nama: 'created_at', render:function(data) {
                    debugger;
                    return moment(data).format('LLL');
                }},
                { data: 'aksi', name: 'aksi' }
            ],
            language: {
                lengthMenu: 'Tampil _MENU_ rekord per halaman',
                info: 'Tampil hal _PAGE_ dari _PAGES_',
                processing: 'Sedang memproses...',
                search: 'Cari',
                zeroRecords: 'Tidak ditemukan rekord',
                paginate: {
                    first: '<<',
                    last: '>>',
                    next: '>',
                    previous: '<',
                },
                infoFiltered: "( filter dari _MAX_ jumlah rekord )"
            }
            });

            $('#modal').on('shown.bs.modal', function() {
                var status = $('#modal').attr('tipe');
                if (status === 'edit') {
                    var kategoriId = $(this).attr('kode');

                     //ajax header
                     $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    //cari data detail kamar
                    $.ajax({
                        url: '/admin/cari-detail-kamar',
                        data: { kategoriId },
                        type: 'POST',
                        success: function(res) {
                            res.forEach(data => {
                                arrayDetailKamar.push(data.list_keterangan);
                            });
                            tampilDetailKamar(arrayDetailKamar);
                            bikinHiddenInputDetail(arrayDetailKamar);
                        },
                        error: function(err) {
                            console.log(err);
                        }
                    });
                }
            });

            $('#modal').on('hidden.bs.modal', function () {
                location.reload();
            });
        });
    </script>
@endpush
