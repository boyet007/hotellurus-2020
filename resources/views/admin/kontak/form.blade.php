{!! Form::model($model, [
    'route' => $model->exists ? ['ajax-kategori-kamar.update', $model->id] : 'ajax-kategori-kamar.store',
    'method' => $model->exists ? 'PUT' : 'POST'
]) !!}


<div class="card mb-2">
    <div class="card-header bg-warning">
        <h6 class="font-weight-bold">Input Data Kategori Kamar</h6>
    </div>
    <div class="card-body">
        <div class="col-12">
            <div class="form-row">
                <div class="col-12">
                    <div class="form-group">
                        {!! Form::text('nama', null, ['class' => 'form-control form-control-sm', 'id' => 'nama', 'placeholder' => 'Masukkan Nama Kategori']) !!}
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-6">
                    <div class="form-group">
                        <div class="input-group input-group-sm">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Rp</span>
                            </div>
                            {!! Form::text('harga_permalam', null, ['class' => 'form-control', 'id' => 'harga_permalam', 
                                'placeholder' => 'Harga Permalam']) !!}
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <div class="input-group input-group-sm">
                            {!! Form::number('jumlah_bed', null, ['class' => 'form-control form-control-sm', 'id' => 'jumlah_bed', 'placeholder' => 'Jumlah Bed', 'min' => 0]) !!}    
                            <div class="input-group-append"><span class="input-group-text">bed</span></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-6">
                    <div class="form-group">
                        <div class="input-group input-group-sm">
                            {!! Form::number('jumlah_dewasa', null, ['class' =>'form-control form-control-sm', 'id' => 'jumlah_dewasa', 'placeholder' => 'Jumlah Dewasa', 'min' => 0]) !!}
                            <div class="input-group-append"><span class="input-group-text">orang</span></div>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <div class="input-group input-group-sm">
                            {!! Form::number('jumlah_anak', null, ['class' =>'form-control form-control-sm', 'id' => 'jumlah_anak', 'placeholder' => 'Jumlah Anak', 'min' => 0]) !!}
                            <div class="input-group-append"><span class="input-group-text">anak</span></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-12">
                    <div class="form-group">
                        {!! Form::text('slug', null, ['class' => 'form-control form-control-sm', 'id' => 'slug', 'placeholder' => 'Masukkan slug ']) !!}
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-12">
                    <div class="form-group">
                        {!! Form::textarea('keterangan', null, ['id' => 'keterangan', 'rows' => 3, 'placeholder' => 'Masukkan Keterangan (boleh dikosongkan)', 
                        'class' => 'form-control form-control-sm']) !!}
                    </div>
                </div>
            </div>
            @if($model->exists)
                {!! Form::hidden('hidden_harga_permalam', $model->harga_permalam) !!}
            @endif
        </div>
    </div>
</div>

<div class="card mb-2">
    <div class="card-header bg-info">
        <h6 class="font-weight-bold">Tambah List Keterangan</h6>
    </div>
    <div class="card-body">
        <div class="form-row">
            <div class="col-11">
                {!! Form::text('list_keterangan', null, ['class' => 'form-control form-control-sm', 'id' => 'listKeterangan']) !!}
            </div>
            <div class="col-1">
                <a href="#" id="btnTambahListKeterangan" class="btn btn-sm btn-warning"><i class="fa fa-plus"></i></a>
            </div>

        </div>
    </div>
</div>

<div class="card mb-2">
    <div class="card-header bg-success">
        <h6 class="font-weight-bold">Input List Keterangan (Maximal 3)</h6>
    </div>
    <div class="card-body">
        <table id="tabelListKeterangan" class="table table-hover tabel-detail">
            <thead>
                <tr>
                    <th><small>Keterangan</small></th>
                    <th><small>Aksi</small></th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>


{!! Form::close() !!}
