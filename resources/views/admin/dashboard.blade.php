@extends('layouts.admin')
@section('title', 'Dashboard')

@section('content')
<!-- Small boxes (Stat box) -->
<div class="row">
    <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-info">
            <div class="inner">
                <h3>Rp @formatUang($totalPendapatanTahunIni)</h3>
                <p>Total Pendapatan Tahun Ini</p>
            </div>
            <div class="icon">
                <i class="ion ion-stats-bars"></i>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-gray">
            <div class="inner">
                <h3>{{ $jumlahKamarKosong }}</h3>
                <p>Jumlah Kamar Kosong</p>
            </div>
            <div class="icon">
                <i class="ion ion-home"></i>
            </div>
        </div>
    </div>
    <!-- ./col -->

    <!-- ./col -->
    <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-danger">
            <div class="inner">
                <h3>{{ $jumlahKamarTerisi }}</h3>
                <p>Jumlah Kamar Terisi</p>
            </div>
            <div class="icon">
                <i class="ion ion-home"></i>
            </div>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-warning">
            <div class="inner">
                <h3>{{ $jumlahEmailBelumTerbaca }}</h3>
                <p>Kontak Belum Terbaca</p>
            </div>
            <div class="icon">
                <i class="ion ion-person"></i>
            </div>

        </div>
    </div>
    <!-- ./col -->
</div>
<!-- /.row -->
<!-- Main row -->
<div class="row">
    <!-- Left col -->
    <section class="col-lg-7 connectedSortable ui-sortable">
        <div class="card">
            <div class="card-header">
              <h3 class="card-title">List Kamar Kosong</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="row">
                    @foreach($listKamarKosong as $kamar)
                        <div class="col-lg-3 col-md-4 col-sm-6">
                            <div class="card beranda-list-kamar">
                                <div class="card-header bg-success">
                                    <h6 class="font-weight-bold">{{ $kamar->nomor_kamar }}</h6>
                                </div>
                                <div class="card-body">
                                    <span>{{ $kamar->kategori_kamar->nama }}</span>
                                    <span>Rp @formatUang($kamar->kategori_kamar->harga_permalam)</span>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <!-- /.card-body -->
          </div>
    </section>
    <!-- /.Left col -->
    <!-- right col (We are only adding the ID to make the widgets sortable)-->
    <section class="col-lg-5 connectedSortable ui-sortable">

        <!-- Calendar -->
        <div class="card bg-gradient-primary">
            <div class="card-header border-0 ui-sortable-handle" style="cursor: move;">

                <h3 class="card-title">
                    <i class="far fa-calendar-alt"></i>
                    Kalender
                </h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body pt-0">
                <!--The calendar -->
                <div id="calendar" style="width: 100%">

                </div>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->

        <div class="card">
            <div class="card-header">
              <h3 class="card-title">List Tamu Menginap</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th>NO</th>
                            <th>Nama Lengkap</th>
                            <th>Telpon</th>
                            <th>No Kamar</th>
                            <th>Lama Menginap</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1; ?>
                        @foreach($listUserAktif as $user)
                            <tr>
                                <td>{{ str_pad($no, 2, "0", STR_PAD_LEFT) }}</td>
                                <td>{{ $user->namaLengkap }}</td>
                                <td>{{ $user->telpon }}</td>
                                <td>{{ $user->noKamar }}</td>
                                <td>{{ $user->lamaMenginap }} {{ $user->satuan }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
          </div>
    </section>
    <!-- right col -->
</div>
<!-- /.row (main row) -->
</section>
@stop
