<table class="table table-borderless tabel-master-show">
    <tbody>
        <tr>
            <td>Nama</td>
            <td>:</td>
            <td>{{ $model->nama }}</td>
        </tr>
        <tr>
            <td>Keterangan</td>
            <td>:</td>
            <td>
                @if($model->keterangan === null)
                    -                
                @else 
                    {{ $model->keterangan }}     
                @endif     
            </td>
        </tr>
    </tbody>
</table>
