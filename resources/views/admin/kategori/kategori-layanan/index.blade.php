@extends('layouts.admin')
@section('title', 'Data Kategori Layanan')

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="card border-danger">
            <div class="card-header">
            <h3 class="card-title">List Kategori Layanan</h3>
                <div class="card-tools">
                    <a class="btn btn-sm btn-success btn-tampil-modal" data-toggle="modal" tipe="tambah" 
                        judul="Tambah Satuan Baru"
                    href="{{ route('ajax-kategori-layanan.create') }}">
                        <i class="fa fa-plus"></i></a>
                </div>
            </div>
            <div class="card-body">
                <table id="dataTable" class="table table-sm table-hover tabel-kategori-layanan">
                    <thead>
                        <tr>
                            <th>Nama</th>
                            <th>Keterangan</th>
                            <th width="50">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function() {
            $('#dataTable').DataTable({
            autoWidth: false,
            serverSide: true,
            processing: true,
            responsive: true,
            ajax: "{{ route('table.ajax.kategori.layanan') }}",
            columns: [
                { data: 'nama', name: 'nama' },
                { data: 'keterangan', nama: 'keterangan', render: function(data, type, row) {
                    if (row.keterangan === null) return '-';
                    else return row.keterangan;
                } },
                { data: 'aksi', name: 'aksi' }
            ],
            language: {
                lengthMenu: 'Tampil _MENU_ rekord per halaman',
                info: 'Tampil hal _PAGE_ dari _PAGES_',
                processing: 'Sedang memproses...',
                search: 'Cari',
                zeroRecords: 'Tidak ditemukan rekord',
                paginate: {
                    first: '<<',
                    last: '>>',
                    next: '>',
                    previous: '<',
                },
                infoFiltered: "( filter dari _MAX_ jumlah rekord )"
            }
            })
        })
    </script>
@endpush
