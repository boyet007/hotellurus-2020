{!! Form::model($model, [
    'route' => $model->exists ? ['ajax-kategori-layanan.update', $model->id] : 'ajax-kategori-layanan.store',
    'method' => $model->exists ? 'PUT' : 'POST'
]) !!}


<div class="form-row">
    <div class="col-12">
        <div class="form-group">
            {!! Form::text('nama', null, ['class' => 'form-control form-control-sm', 'id' => 'nama', 'placeholder' => 'Masukkan Nama Kategori']) !!}
        </div>
    </div>
</div>
<div class="form-row">
    <div class="col-12">
        <div class="form-group">
            {!! Form::textarea('keterangan', null, ['id' => 'keterangan', 'rows' => '3', 'placeholder' => 'Masukkan Keterangan (boleh dikosongkan)', 
            'class' => 'form-control form-control-sm']) !!}
        </div>
    </div>
</div>

{!! Form::close() !!}
