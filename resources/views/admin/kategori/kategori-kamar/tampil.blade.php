<table class="table table-borderless tabel-master-show">
    <tbody>
        <tr>
            <td>Nama</td>
            <td>:</td>
            <td>{{ $model->nama }}</td>
        </tr>
        <tr>
            <td>Harga per malam</td>
            <td>:</td>
            <td>Rp @formatUang( $model->harga_permalam)</td>
        </tr>
        <tr>
            <td>Jumlah Dewasa</td>
            <td>:</td>
            <td>{{ $model->jumlah_dewasa }} orang</td>
        </tr>
        <tr>
            <td>Jumlah Anak</td>
            <td>:</td>
            <td>{{ $model->jumlah_anak }} anak</td>
        </tr>
        <tr>
            <td>Jumlah Bed</td>
            <td>:</td>
            <td>{{ $model->jumlah_bed }} bed</td>
        </tr>
        <tr>
            <td>Slug</td>
            <td>:</td>
            <td>{{ $model->slug }}</td>
        </tr>
        <tr>
            <td>Keterangan</td>
            <td>:</td>
            <td>
                @if($model->keterangan === null)
                    -                
                @else 
                    {{ $model->keterangan }}     
                @endif     
            </td>
        </tr>
    </tbody>
</table>
