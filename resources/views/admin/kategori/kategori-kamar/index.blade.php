@extends('layouts.admin')
@section('title', 'Data Kategori Kamar')

@section('content')
<div class="row">
    <div class="col-md-10">
        <div class="card border-danger">
            <div class="card-header">
                <h3 class="card-title">List Kategori Kamar</h3>
                <div class="card-tools">
                    <a class="btn btn-sm btn-success btn-tampil-modal" data-toggle="modal" tipe="tambah" 
                        judul="Tambah Kategori Kamar Baru"
                    href="{{ route('ajax-kategori-kamar.create') }}">
                        <i class="fa fa-plus"></i></a>
                </div>
            </div>
            <div class="card-body">
                <table id="dataTable" class="table table-sm table-hover tabel-kategori-kamar">
                    <thead>
                        <tr>
                            <th width="30">Nama</th>
                            <th>Harga</th>
                            <th>Bed</th>
                            <th>Slug</th>
                            <th>Keterangan</th>
                            <th width="50">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
    <script>
        var arrayDetailKamar = [];
        const MAXIMUM_LIST = 3;

        function tampilDetailKamar(parArrayKamar, nomorRow = 0) {
            parArrayKamar.forEach(detailKamar => {
                var rowBaru = `<tr id="detail-` + nomorRow + `">
                    <td><small>` + detailKamar + `</small></td>
                    <td><button data-id="` + nomorRow + `" class="btn btn-sm btn-danger btn-hapus-detail"><i class="fas fa-trash"></i></button></td>                           
                    </tr>`;
                
                $('#tabelListKeterangan tbody').append(rowBaru);
                nomorRow++;
            });
        }

        function bikinHiddenInputDetail(parArrayDetailKamar, rowNumber = 0) {
            $('input.detail:hidden').remove();

            parArrayDetailKamar.forEach(value => {
                $('<input>').attr({
                    type: 'hidden',
                    name: 'tabelListKeterangan[]' + rowNumber,
                    id: 'tabelListKeterangan[]' + rowNumber,
                    class: 'detail',
                    value: parArrayDetailKamar[rowNumber]
                }).appendTo('form');
                rowNumber++;
            });
        }
        
        $(document).ready(function() {
            $('#dataTable').DataTable({
            autoWidth: false,
            serverSide: true,
            processing: true,
            responsive: true,
            ajax: "{{ route('table.ajax.kategori.kamar') }}",
            columns: [
                { data: 'nama', name: 'nama' },
                { data: 'harga_permalam', name: 'harga_permalam', render: function(data, tipe, row) {
                    return 'Rp ' + formatAngka(data.toString()) + ' /mlm'
                } },
                { data: 'jumlah_bed', nama: 'jumlah_bed' },
                { data: 'slug', nama: 'slug' },
                { data: 'keterangan', nama: 'keterangan', render: function(data, type, row) {
                    if (row.keterangan === null) return '-';
                    else return row.keterangan;
                } },
                { data: 'aksi', name: 'aksi' }
            ],
            language: {
                lengthMenu: 'Tampil _MENU_ rekord per halaman',
                info: 'Tampil hal _PAGE_ dari _PAGES_',
                processing: 'Sedang memproses...',
                search: 'Cari',
                zeroRecords: 'Tidak ditemukan rekord',
                paginate: {
                    first: '<<',
                    last: '>>',
                    next: '>',
                    previous: '<',
                },
                infoFiltered: "( filter dari _MAX_ jumlah rekord )"
            }
            });

            $('body').on('keyup', '#harga_permalam', function(e) {
                e.preventDefault();
                $('#harga_permalam').val(formatAngka($(this).val())); 

                //bikin hidden value
                var integerHarga = accounting.unformat($(this).val()); 
                bikinHiddenInputAngka('hidden_harga_permalam', integerHarga);
            });

            $(document).delegate('#btnTambahListKeterangan', 'click', function(e) {
                e.preventDefault();
                if (arrayDetailKamar.length < MAXIMUM_LIST) {
                    var detailKamar = $('#listKeterangan').val();
                    if (detailKamar.trim() !== '') {
                        arrayDetailKamar.push(detailKamar);

                        //masukkan kedalam tabel
                        $('#tabelListKeterangan tbody').empty();
                        
                        tampilDetailKamar(arrayDetailKamar);
                        bikinHiddenInputDetail(arrayDetailKamar);

                        $('#listKeterangan').val('');
                    }
                }
            });

            $(document).on('click', '.btn-hapus-detail', function(e) {
                e.preventDefault();
                const id = $(this).data('id');
                delete arrayDetailKamar[id];
                arrayDetailKamar = arrayDetailKamar.filter(Boolean);

                //masukkan kedalam tabel
                $('#tabelListKeterangan tbody').empty();
                tampilDetailKamar(arrayDetailKamar);
                bikinHiddenInputDetail(arrayDetailKamar);
            });

            $('#modal').on('shown.bs.modal', function() {
                var status = $('#modal').attr('tipe');
                if (status === 'edit') {
                    var kategoriId = $(this).attr('kode');

                     //ajax header
                     $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    //cari data detail kamar
                    $.ajax({
                        url: '/admin/cari-detail-kamar',
                        data: { kategoriId },
                        type: 'POST',
                        success: function(res) {
                            res.forEach(data => {
                                arrayDetailKamar.push(data.list_keterangan);
                            });
                            tampilDetailKamar(arrayDetailKamar);
                            bikinHiddenInputDetail(arrayDetailKamar);
                        },
                        error: function(err) {  
                            console.log(err);
                        }
                    });
                }
            });

            $('#modal').on('hidden.bs.modal', function () {
                arrayDetailKamar = [];
            });
        });
    </script>
@endpush
