<!DOCTYPE html>
<html>
    <head>
        <title>Cetak Kuitansi</title>
        <style type="text/css">
            * {
                font-family: Verdana, Arial, sans-serif;
                font-size:14px;
                margin:0;
            }

            body {
                padding:0;
                margin:0;
            }

            .main {
                margin-top: 20px;
            }

            .main img {
                width:175px;
                height:60px;
            }

            .main table{
                margin: 5px 0px 5px 40px;
            }

            .main > table > tbody > tr:nth-last-child(){
                border-bottom: 2px solid black;
            }

            .main > table > tbody > tr > td:nth-child(1) {
                width:289px;
            }

            .main .kop-alamat {
                display: block;
                font-size: x-small;
            }            

            .main .kop-judul {
                transform: scale(2, 1.5);
                font-weight: bold;
                margin-bottom: 7px;
                display:block;
            }            

            .main .kop-sub-judul {
                transform: scale(1.2, 0.9);
                font-weight: bold;
                margin-bottom: 7px;
                display:block;
            }

            .garis {
                border-top:1px solid black;
            }           

            .content, .detail {
                padding: 35px 5px 5px 70px;
            } 

            .content > table td {
                font-stretch: 200%;
                font-size: 16px;
                font-style: italic;
            }

            .content > table > tbody > tr > td:nth-child(3) {
                font-weight: bold;
            }

            .detail > table {
                width: 94%;
                border: 1px solid;
                margin: 5px 4px;
            }

            .detail > table > thead > tr > th {
                border-bottom: 1px solid black;
                text-align: left;
            }

            .detail > table > thead > tr > th,
            .detail > table > tbody > tr > td,
            .detail > table > tfoot > tr > td {
                font-size: smaller;
            }

            .detail > table > tbody > tr:last-child > td {
                border-bottom: 1px solid black;
            }

            .detail > table > tfoot > tr > td:last-child {
                font-weight: bold;
            }

            .footer {
                padding-left:600px;
            }

            .footer span {
                display:block;
            }

            .footer span:nth-child(3) {
                margin-top:50px;
                font-weight: bold;
            }


        </style>
    </head>
    <body>
        <div class="main">
            <table>
                <tbody>
                    <tr>
                        <td>
                            <img src="{{ url('images/logo.png') }}"/>
                            <span class="kop-alamat">{{ config('info.ALAMAT_JALAN') }} {{ config('info.ALAMAT_KABUPATEN') }} {{ config('info.ALAMAT_KOTA') }}</span>
                            <span class="kop-alamat">Telp {{ config('info.TELPON') }} HP {{ config('info.HP') }}</span>
                        </td>
                        <td>
                            <span class="kop-judul">BUKTI PEMBAYARAN</span>
                            <span class="kop-sub-judul">NO : {{ $model->notrans }}</span>
                        </td>
                    </tr>
                </tbody>       
            </table> 
        </div>
        <div class="garis"></div>
        <div class="content">
            <table>
                <tbody>
                    <tr>
                        <td>Sudah Terima Dari</td>
                        <td>:</td>
                        <td>{{ $model->user->nama_lengkap }}</td>
                    </tr>
                    <tr>
                        <td>Banyaknya Uang</td>
                        <td>:</td>
                        <td>{{ terbilang($model->dapatkanTotalTransaksi()) }}</td>
                    </tr>
                    <tr>
                        <td>Untuk Pembayaran</td>
                        <td>:</td>
                        @if(trim($model->keterangan) === "")
                            <td>-</td>
                        @else 
                            <td>{{ $model->keterangan }}</td>
                        @endif
                    </tr>
                </tbody>
            </table>
        </div>        
        <div class="detail">
            <h2>Detail Transaksi</h2>
            <table>
                <thead>
                    <th>No</th>
                    <th>Tipe</th>
                    <th>Kategori</th>
                    <th>Nama</th>
                    <th>Harga</th>
                    <th>Satuan</th>
                    <th>Qty</th>
                    <th>Total</th>
                </thead>
                  <?php  
                    $no = 1;
                    $subTotal = 0;
                  ?>
                <tbody>
                    @foreach($model->detail_transaksi as $detail)
                        <?php 
                            $harga = $detail->harga;
                            $qty = $detail->qty;
                            $total = $harga * $qty;
                            $subTotal += $total;
                        ?>
                        <tr>
                            <td>{{ $no }}</td>
                            <td>{{ $detail->tipe }}</td>
                            <td>{{ $detail->kategori }}</td>
                            <td>{{ $detail->nama }}</td>
                            <td>Rp @formatUang($detail->harga)</td>
                            <td>{{ $detail->satuan }}</td>
                            <td>{{ $detail->qty }}</td>
                            <td>Rp @formatUang($total)</td>
                        </tr>
                        <?php $no++; ?>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="5"></td>
                        <td colspan="2">SubTotal</td>
                        <td>Rp @formatUang($subTotal)</td>
                    </tr>
                </tfoot>
            </table>
        </div>
        <div class="footer">
            <span>Tgl {{ formatTanggalStrip($model->tanggal) }}</span>
            <span>Yang Menerima</span>
            <span>{{ $model->dapatkanNamaAdmin() }}</span>
        </div>
    </body>
</html>