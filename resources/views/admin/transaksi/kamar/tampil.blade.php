<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header bg-primary">
                <h6 class="font-weight-bold">Data Transaksi Kamar</h6>
            </div>
            <div class="card-body">
                <table class="table table-borderless tabel-master-show tabel-tampil-kamar">
                    <tbody>
                        <tr>
                            <td>Nomor Transaksi</td>
                            <td>:</td>
                            <td>{{ $model->notrans }}</td>
                        </tr>
                        <tr>
                            <td>Tanggal</td>
                            <td>:</td>
                            <td>{{ formatTanggal($model->tanggal) }}</td>
                        </tr>
                        <tr>
                            <td>Nama Lengkap</td>
                            <td>:</td>
                            <td>{{ $model->user->email }}</td>
                        </tr>
                        <tr>
                            <td>Keterangan</td>
                            <td>:</td>
                            <td>{{ $model->keterangan }}</td>
                        </tr>
                        <tr>
                            <td>Tipe Bayar</td>
                            <td>:</td>
                            <td>{{ $model->tipe_bayar }}</td>
                        </tr>
                        <tr>
                            <td>Aktif</td>
                            <td>:</td>
                            @if(isset($model) && $model->aktif === 1)
                                <td><b>Aktif</b></td>
                            @else 
                                <td>Tidak Aktif</td>
                            @endif
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card">
            <div class="card-header bg-success">
                <h6 class="font-weight-bold">Data User</h6>
            </div>
            <div class="card-body">
                <table class="table table-borderless tabel-master-show tabel-tampil-kamar">
                    <tbody>
                        <tr>
                            <td>Nama Lengkap</td>
                            <td>:</td>
                            <td>{{ $model->user->nama_lengkap }}</td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td>:</td>
                            <td>{{ $model->user->email }}</td>
                        </tr>
                        <tr>
                            <td>Alamat</td>
                            <td>:</td>
                            <td>{{ $model->user->alamat }}</td>
                        </tr>
                        <tr>
                            <td>Kota</td>
                            <td>:</td>
                            <td>{{ $model->user->kota }}</td>
                        </tr>
                        <tr>
                            <td>Kode Pos</td>
                            <td>:</td>
                            <td>{{ $model->user->kodepos }}</td>
                        </tr>
                        <tr>
                            <td>No Telpon</td>
                            <td>:</td>
                            <td>{{ $model->user->no_telpon }}</td>
                        </tr>
                        <tr>
                            <td>Jenkel</td>
                            <td>:</td>
                            @if($model->user->jenkel === 'L')
                                <td>Laki-Laki</td>
                            @elseif($model->user->jenkel === 'P')
                                <td>Perempuan</td>
                            @endif
                        </tr>
                        <tr>
                            <td>Foto</td>
                            <td>:</td>
                            <td><img src="{{ url('/images/persons/' . $model->user->gambar) }}"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-dark">
                <h6 class="font-weight-bold">Detail Transaksi</h6>
            </div>
            <div class="card-body">
                <table class="table table-detail-transaksi">
                    <thead>
                        <th>Tipe</th>
                        <th>Kategori</th>
                        <th>Nama</th>
                        <th>Harga</th>
                        <th>Qty</th>
                        <th>Satuan</th>
                        <th>Total</th>
                    </thead>
                    <tbody>
                        <?php $subTotal = 0; ?>
                        @foreach($model->detail_transaksi as $detail)
                            <tr>
                                <td>{{ $detail->tipe }}</td>
                                <td>{{ $detail->kategori }}</td>
                                <td>{{ $detail->nama }}</td>
                                <td>Rp @formatUang($detail->harga)</td>
                                <td>{{ $detail->qty }}</td>
                                <td>{{ $detail->satuan }}</td>
                                <?php 
                                    $harga = $detail->harga;
                                    $qty = $detail->qty;
                                    $total = $harga * $qty;
                                    $subTotal += $total;
                                ?>
                                <td>Rp @formatUang($total)</td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="5"></td>
                            <td><b>SubTotal</b></td>
                            <td><b>Rp @formatUang($subTotal)</b></td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>



