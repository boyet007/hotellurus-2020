@extends('layouts.admin')
@section('title', 'Transaksi Kamar')

@section('content')
<div class="row">
    <div class="col-md-10">
        <div class="card border-danger">
            <div class="card-header">
                <h3 class="card-title">Transaksi Kamar</h3>
                <div class="card-tools">
                    <a class="btn btn-sm btn-success btn-tampil-modal" data-toggle="modal" tipe="tambah"
                        judul="Tambah Transaksi Baru" ukuran="besar"
                    href="{{ route('ajax-master-transaksi-kamar.create') }}">
                        <i class="fa fa-plus"></i></a>
                </div>
            </div>
            <div class="card-body">
                <table id="dataTable" class="table table-sm table-hover tabel-master-layanan">
                    <thead>
                        <tr>
                            <th>NoTrans</th>
                            <th>Tanggal</th>
                            <th>Nama User</th>
                            <th>Total Bayar</th>
                            <th>Tipe Bayar</th>
                            <th>Admin</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function() {
            let arrDetailTransaksi = [];

            function tampilDetailTransaksi(parArray, rowNumber = 0) {
                $('#tabel-detail-transaksi tbody').empty();
                if (parArray.length > 0) {
                    parArray.forEach(function(value) {
                        var newRow = `<tr>
                            <td><small>` + value.detailTipe + `</small></td>
                            <td><small>` + value.detailKategori + `</small></td>
                            <td><small>` + value.detailNama + `</small></td>
                            <td><small>` + value.detailHarga + `</small></td>
                            <td><small>` + value.detailQty +`</small></td>
                            <td><small>` + value.detailSatuan +`</small></td>

                            <td><button class="btn btn-sm btn-danger btn-hapus-detail"
                                data-tipe="` + value.detailTipe + `"
                                data-row="` + rowNumber + `">
                                </small><span class="fas fa-trash"></span>
                            </td>
                        </tr>`;

                        $('#tabel-detail-transaksi').append(newRow);
                        rowNumber++;
                    });
                }
            }

            function bikinHiddenInput(parArray, rowNumber = 0) {
                $('input.detail:hidden').remove();
                if(parArray.length > 0) {
                    parArray.forEach(function(value) {

                        $('<input>').attr({ type: 'hidden', name: 'tabel-detail-transaksi[]' + rowNumber, class: 'detail',
                            id: 'tabel-detail-transaksi[]' + rowNumber, value: parArray[rowNumber].detailNama })
                            .appendTo('form');

                        $('<input>').attr({ type: 'hidden', name: 'detailTipe[]' + rowNumber, class: 'detail',
                            id: 'detailTipe[]' + rowNumber, value: parArray[rowNumber].detailTipe })
                            .appendTo('form');

                        $('<input>').attr({ type: 'hidden', name: 'detailKategori[]' + rowNumber, class: 'detail',
                            id: 'detailKategori[]' + rowNumber, value: parArray[rowNumber].detailKategori })
                            .appendTo('form');

                        $('<input>').attr({ type: 'hidden', name: 'detailNama[]' + rowNumber, class: 'detail',
                            id: 'detailNama[]' + rowNumber, value: parArray[rowNumber].detailNama })
                            .appendTo('form');

                        $('<input>').attr({ type: 'hidden', name: 'detailQty[]' + rowNumber, class: 'detail',
                            id: 'detailQty[]' + rowNumber, value: parArray[rowNumber].detailQty })
                            .appendTo('form');

                        $('<input>').attr({ type: 'hidden', name: 'detailHarga[]' + rowNumber, class: 'detail',
                            id: 'detailHarga[]' + rowNumber, value: parArray[rowNumber].detailHarga })
                            .appendTo('form');

                        $('<input>').attr({ type: 'hidden', name: 'detailSatuan[]' + rowNumber, class: 'detail',
                            id: 'detailSatuan[]' + rowNumber, value: parArray[rowNumber].detailSatuan })
                            .appendTo('form');

                        rowNumber++;
                    });
                }
            }

            $('#dataTable').DataTable({
                autoWidth: false,
                serverSide: true,
                processing: true,
                responsive: true,
                ajax: "{{ route('table.ajax.transaksi.kamar') }}",
                columns: [
                    { data: 'notrans', name: 'notrans' },
                    { data: 'tanggal', name: 'tanggal', render:function(data, tipe, row) {
                        return moment(data).format('DD-MM-YYYY')
                    }},
                    { data: 'nama_user', name: 'nama_user' },
                    { data: 'total_bayar', name: 'total_bayar', render:function(data, tipe, row) {
                        return 'Rp ' + formatAngka(data.toString());
                    }},
                    { data: 'tipe_bayar', name: 'tipe_bayar' },
                    { data: 'nama_admin', name: 'nama_admin' },
                    { data: 'aksi', name: 'aksi'}
                ],
                language: {
                    lengthMenu: 'Tampil _MENU_ rekord per halaman',
                    info: 'Tampil hal _PAGE_ dari _PAGES_',
                    processing: 'Sedang memproses...',
                    search: 'Cari',
                    zeroRecords: 'Tidak ditemukan rekord',
                    paginate: {
                        first: '<<',
                        last: '>>',
                        next: '>',
                        previous: '<',
                    },
                    infoFiltered: "( filter dari _MAX_ jumlah rekord )"
                }
            })

            $('#modal').on('shown.bs.modal', function(e){
                var status = $('#modal').attr('tipe');
                if (status === 'edit') {
                    var transId = $('\#modal').attr('kode');

                    //cari data user
                    $.ajaxSetup({ headers: { 'X-CSRF-TOKEN' :  $('meta[name=csrf-token]').attr('content') } });
                    $.ajax({
                        url: '/admin/dapatkan-data-transaksi-user',
                        data: { transId },
                        type: 'POST',
                        success: function(res) {
                            $('#namaLengkap').val(res.nama_lengkap);
                            $('#email').val(res.email);
                            $('#alamat').val(res.alamat);
                            $('#kota').val(res.kota);
                            $('#noTelpon').val(res.no_telpon);
                            $('#role').val(res.role);
                        },
                        error: function(e) {
                            console.log(e);
                        }
                    });

                    //cari data detail
                    $.ajax({
                        url: '/admin/dapatkan-data-transaksi-detail',
                        data: { transId },
                        type: 'POST',
                        success: function(res) {
                            for(i=0; i<res.length; i++) {
                                const detailTipe = res[i].tipe;
                                const detailKategori = res[i].kategori;
                                const detailNama = res[i].nama;
                                const detailHarga = res[i].harga;
                                const detailQty = res[i].qty;
                                const detailSatuan = res[i].satuan;

                                const detailTransaksi = { detailTipe, detailKategori, detailNama,
                                    detailHarga, detailQty, detailSatuan };

                                arrDetailTransaksi.push(detailTransaksi);
                            }

                            tampilDetailTransaksi(arrDetailTransaksi);
                            bikinHiddenInput(arrDetailTransaksi);
                        },
                        error: function(e) {
                            debugger;
                            console.log(e);
                        }
                    });
                }
            });

            $(document).delegate('#user_id', 'change', function(e) {
                e.preventDefault();
                const userId = $(this).find('option:selected').val();
                if(userId) {
                    $.ajaxSetup({ headers: { 'X-CSRF-TOKEN' :  $('meta[name=csrf-token]').attr('content') } });
                    $.ajax({
                        url: '/admin/dapatkan-data-user',
                        data: { userId },
                        type: 'POST',
                        success: function(res) {
                            $('#namaLengkap').val(res.nama_lengkap);
                            $('#email').val(res.email);
                            $('#alamat').val(res.alamat);
                            $('#kota').val(res.kota);
                            $('#noTelpon').val(res.no_telpon);
                            $('#role').val(res.role);
                        },
                        error: function(err) {
                            console.log(err);
                        }
                    });
                } else {
                    $('#namaLengkap').val('');
                    $('#email').val('');
                    $('#alamat').val('');
                    $('#kota').val('');
                    $('#noTelpon').val('');
                    $('#role').val('');
                }
            });

            $(document).delegate('#detailTipe', 'change', function(e) {
                e.preventDefault();
                const tipe = $(this).val();
                const isAktif = $('#aktif').is(':checked');
                
                if(tipe !== '' && tipe === 'kamar') {
                    $('#detailNama').empty();
                    $.ajaxSetup({ headers: { 'X-CSRF-TOKEN' :  $('meta[name=csrf-token]').attr('content') } });
                    $.ajax({
                        url: '/admin/dapatkan-data-tipe-transaksi',
                        data: { tipe, isAktif },
                        type: 'POST',
                        success: function(res) {
                            var option = '';
                            var data = Object.entries(res).map(([key, value]) => ({key,value}));
                            for (var i=0; i<data.length; i++){
                                option += '<option value="'+ data[i].key + '">' + data[i].value + '</option>';
                            }
                            $('#detailNama').append(option);
                            $("#detailNama").prepend("<option value='' selected=''>== Kamar ==</option>");
                        },
                        error: function(err) {
                            console.log(err);
                        }
                    });
                } else if(tipe !== '' && tipe === 'layanan') {
                    $('#detailNama').empty();
                    $.ajaxSetup({ headers: { 'X-CSRF-TOKEN' :  $('meta[name=csrf-token]').attr('content') } });
                    $.ajax({
                        url: '/admin/dapatkan-data-tipe-transaksi',
                        data: { tipe },
                        type: 'POST',
                        success: function(res) {
                            var option = '';
                            var data = Object.entries(res).map(([key, value]) => ({key,value}));
                            for (var i=0; i<data.length; i++){
                                option += '<option value="'+ data[i].key + '">' + data[i].value + '</option>';
                            }
                            $('#detailNama').append(option);
                            $("#detailNama").prepend("<option value='' selected=''>== Layanan ==</option>");
                        },
                        error: function(err) {
                            console.log(err);
                        }
                    });
                }
            });

            $(document).delegate('#detailNama', 'change', function() {
                const detailTipe = $('#detailTipe').val();
                if (detailTipe !== '' && detailTipe === 'kamar') {
                    //hapus input hidden detailKodeTransaksi
                    $("input#detailKategori:hidden").remove();

                    const kamarId = $('#detailNama').val();
                    //cari data kamar
                    $.ajaxSetup({ headers: { 'X-CSRF-TOKEN' :  $('meta[name=csrf-token]').attr('content') } });
                    $.ajax({
                        url: '/admin/dapatkan-data-kamar',
                        data: { kamarId },
                        type: 'POST',
                        success: function(res) {
                            const id = res.id;
                            const nama = res.nomor_kamar;
                            const harga = res.kategori_kamar.harga_permalam;
                            const qty = 1;
                            const satuan = res.kategori_kamar.satuan.kode;
                            $('#detailHarga').val(harga);
                            $('#detailQty').val(qty);
                            $('#detailSatuan').val(satuan);
                            $('#detailTotal').val(harga * qty);
                            $('<input>').attr({ type: 'hidden', id: 'detailKategori', value: res.kategori_kamar.nama }).appendTo('form');
                        },
                        error: function(err) {
                            console.log(err);
                        }
                    });
                } else if (detailTipe !== '' && detailTipe === 'layanan') {
                     //hapus input hidden detailKodeTransaksi
                     $("input#detailKategori:hidden").remove();

                    const layananId = $('#detailNama').val();
                    //cari data kamar
                    $.ajaxSetup({ headers: { 'X-CSRF-TOKEN' :  $('meta[name=csrf-token]').attr('content') } });
                    $.ajax({
                        url: '/admin/dapatkan-data-layanan',
                        data: { layananId },
                        type: 'POST',
                        success: function(res) {
                            const id = res.id;
                            const nama = res.nama;
                            const harga = res.harga;
                            const qty = 1;
                            const satuan = res.satuan.kode;
                            $('#detailHarga').val(harga);
                            $('#detailQty').val(qty);
                            $('#detailSatuan').val(satuan);
                            $('#detailTotal').val(harga * qty);
                            $('<input>').attr({ type: 'hidden', id: 'detailKategori', value: res.kategori_layanan.nama }).appendTo('form');
                        },
                        error: function(err) {
                            console.log(err);
                        }
                    });
                }
            });

            $(document).delegate('#detailQty', 'change', function() {
                const detailQty = $(this).val();
                const detailHarga = $('#detailHarga').val();
                $('#detailTotal').val(detailHarga * detailQty);
            });

            $(document).delegate('#detailBtnTambah', 'click', function(e) {
                e.preventDefault();
                const form = $('#modal-body form');
                const detailTipe = $('#detailTipe').val();
                const detailKategori = $('#detailKategori').val();
                const detailNama = $('#detailNama option:selected').text();
                const detailHarga = $('#detailHarga').val();
                const detailQty = $('#detailQty').val();
                const detailSatuan = $('#detailSatuan').val();

                form.find('.form-text').remove();
                form.find('.form-group').removeClass('has-error');

                //validasi detail
                $.ajaxSetup({ headers: { 'X-CSRF-TOKEN' :  $('meta[name=csrf-token]').attr('content') } });

                $.ajax({
                    url: '/admin/validasi-detail-transaksi',
                    data: { detailTipe, detailKategori, detailNama, detailHarga,
                            detailQty, detailSatuan },
                    type: 'POST',
                    success:function(){
                        const detailTransaksi = { detailTipe, detailKategori, detailNama,
                            detailHarga, detailQty, detailSatuan };

                        arrDetailTransaksi.push(detailTransaksi);

                        //tampilkan detail ke table
                        tampilDetailTransaksi(arrDetailTransaksi);

                        //masukkan ke dalam hidden input
                        bikinHiddenInput(arrDetailTransaksi);

                        //kosongkan detail form
                        $('#detailTipeTransaksi').val('');
                        $('#detailNama').val('');
                        $('#detailHarga').val('');
                        $('#detailQty').val('');
                        $('#detailTotal').val('');
                    },
                    error: function(err){
                        console.log(err);
                        var res = err.responseJSON;
                        if ($.isEmptyObject(res) === false) {
                            $.each(res.errors, function(key, value) {
                            $('#' + key)
                                .closest('.form-group')
                                .addClass('has-error')
                                .append('<span class="form-text text-danger"><strong>' + value +  '</strong></span>')
                            })
                        }
                    }
                });
            });

            $(document).on('click', '.btn-hapus-detail', function(e) {
                e.preventDefault();
                const row = $(this).data('row');
                arrDetailTransaksi.splice(row, 1);
                tampilDetailTransaksi(arrDetailTransaksi);
                bikinHiddenInput(arrDetailTransaksi);
            });

            $('#modal').on('hidden.bs.modal', function() {
                arrDetailTransaksi = [];
            });
        });
    </script>
@endpush
