{!! Form::model($model, [
    'route' => $model->exists ? ['ajax-master-transaksi-kamar.update', $model->id] : 'ajax-master-transaksi-kamar.store',
    'method' => $model->exists ? 'PUT' : 'POST',
    'novalidate' => 'novalidate',
    'files' => 'true'
]) !!}

{!! Form::hidden('notrans', $noTrans) !!}
{!! Form::hidden('admin_id', Auth::user()->id ) !!}

@if(isset($model) && $model->aktif === 1)
    {!! Form::hidden('detail_transaksi_lama', $model->detail_transaksi) !!}
@endif



<div class="card mb-2">
    <div class="card-header bg-dark">
        <h6 class="font-weight-bold">Input Transaksi Kamar</h6>
    </div>
    <div class="card-body">
        <div class="form-row">
            <div class="col-lg-6">
                <div class="form-group">
                    {!! Form::text('notrans', $noTrans, ['id' => 'notrans', 'disabled' => 'disabled',
                        'class' => 'form-control form-control-sm']) !!}
                </div>
                <div class="form-group">
                   {!! Form::date('tanggal', null, ['id' => 'tanggal', 'class' => 'form-control form-control-sm'])  !!}
                </div>
                <div class="form-group">
                   {!! Form::select('user_id', $user, null, ['id' => 'user_id', 'class' => 'form-control form-control-sm'])  !!}
                </div>
                <div class="form-group">
                    {!! Form::textarea('keterangan', null, ['id' => 'keterangan', 'class' => 'form-control form-control-sm',
                        'rows' => '3', 'placeholder' => 'Masukkan Keterangan']) !!}
                </div>
                <div class="form-group">
                   {!!  Form::select('tipe_bayar', $tipeBayar, null, ['id' => 'tipe_bayar','class' => 'form-control form-control-sm'])  !!}
                </div>
                <div class="form-group">
                    @if(isset($model) && $model->aktif === 1)
                        {!! Form::checkbox('aktif', null, true, ['id' => 'aktif']) !!}
                    @else 
                        {!! Form::checkbox('aktif', null, false, ['id' => 'aktif']) !!}
                    @endif
                    {!! Form::label('label_aktif', 'aktif', ['class' => 'form-check-label']) !!}
                </div>
            </div>
            <div class="col-lg-6">
                <div id="data_user" class="card">
                    <div class="card-header bg-dark">
                        <h6 class="font-weight-bold">Data User</h6>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-5">Nama Lengkap</div>
                            <div class="col-7">
                                {!! Form::text('nama_lengkap', null, ['id' => 'namaLengkap', 'class' => 'form-control form-control-sm',
                                    'disabled' => 'disabled']) !!}</div>
                        </div>
                        <div class="row">
                            <div class="col-5">Email</div>
                            <div class="col-7">{!! Form::email('email', null, ['id' => 'email', 'class' => 'form-control form-control-sm',
                                'disabled' => 'disabled']) !!}</div>
                        </div>
                        <div class="row">
                            <div class="col-5">Alamat</div>
                            <div class="col-7">{!! Form::textarea('alamat', null, ['id' => 'alamat', 'class' => 'form-control form-control-sm',
                                'disabled' => 'disabled', 'rows' => 3]) !!}</div>
                        </div>
                        <div class="row">
                            <div class="col-5">Kota</div>
                            <div class="col-7">{!! Form::text('kota', null, ['id' => 'kota', 'class' => 'form-control form-control-sm', 'disabled'=> 'disabled']) !!}</div>
                        </div>
                        <div class="row">
                            <div class="col-5">No Telpon</div>
                            <div class="col-7">{!! Form::text('no_telpon', null, ['id' => 'noTelpon', 'class' => 'form-control formn-control-sm', 'disabled' => 'disablled']) !!}</div>
                        </div>
                        <div class="row">
                            <div class="col-5">role</div>
                            <div class="col-7">{!! Form::text('role', null, ['id' => 'role', 'class' => 'form-control form-control-sm', 'disabled' => 'disabled']) !!}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header bg-dark">
                        <h6 class="font-weight-bold">Tambah Detail Transaksi</h6>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-2 form-group">
                                {!! Form::select('tipe', $tipeTransaksi, null, ['id' => 'detailTipe', 'class' => 'form-control form-control-sm']) !!}
                            </div>
                            <div class="col-3 form-group">
                                {!! Form::select('nama', [],  null, ['id' => 'detailNama', 'class' => 'form-control form-control-sm']) !!}
                            </div>
                            <div class="col-2 form-group">
                                {!! Form::number('harga', null, ['id' => 'detailHarga', 'min' => '0', 'class' => 'form-control form-control-sm', 'placeholder' => 'harga']) !!}
                            </div>
                            <div class="col-1 form-group">
                                {!! Form::number('qty', null, ['id' => 'detailQty', 'min' => '1', 'class' => 'form-control form-control-sm', 'placeholder' => 'qty']) !!}
                            </div>
                            <div class="col-1 form-group">
                                {!! Form::text('satuan', null, ['id' => 'detailSatuan', 'disabled' => 'disabled', 'class' => 'form-control form-control-sm', 'placeholder' => 'Satuan']) !!}
                            </div>
                            <div class="col-2 form-group">
                                {!! Form::text('total', null, ['id' => 'detailTotal', 'class' => 'form-control form-control-sm', 'placeholder' => 'Total',
                                    'disabled' => 'disabled']) !!}
                            </div>
                            <div class="col-1">
                                <a href="#" id="detailBtnTambah" class="btn btn-sm btn-success" ><span class="fa fa-plus"></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header bg-dark">
                        <h6 class="font-weight-bold">
                            Detail Transaksi
                        </h6>
                    </div>
                    <div class="card-body">
                        <table class="table table-detail-transaksi" id="tabel-detail-transaksi">
                            <thead>
                                <th><small>TIPE</small></th>
                                <th><small>KATEGORI</small></th>
                                <th><small>NAMA</small></th>
                                <th><small>HARGA</small></th>
                                <th><small>QTY</small></th>
                                <th><small>SAT</small></th>
                                <th><small>AKSI</small></th>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
