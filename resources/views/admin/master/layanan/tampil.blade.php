<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-primary">
                <h6 class="font-weight-bold">Data Layanan</h6>
            </div>
            <div class="card-body">
                <table class="table table-borderless tabel-master-show tabel-tampil-kamar">
                    <tbody>
                        <tr>
                            <td>Nama</td>
                            <td>:</td>
                            <td>{{ $model->nama }}</td>
                        </tr>
                        <tr>
                            <td>Kategori Layanan</td>
                            <td>:</td>
                            <td>{{ $model->kategori_layanan->nama }}</td>
                        </tr>
                        <tr>
                            <td>Harga</td>
                            <td>:</td>
                            <td>Rp @formatUang( $model->harga )</td>
                        </tr>
                        <tr>
                            <td>Satuan</td>
                            <td>:</td>
                            <td>{{ $model->satuan->nama }}</td>
                        </tr>
                        <tr>
                            <td>Keterangan</td>
                            <td>:</td>
                            <td>
                                @if($model->keterangan === null)
                                    -                
                                @else 
                                    {{ $model->keterangan }}     
                                @endif     
                            </td>
                        </tr>
                        <tr>
                            <td>Gambar</td>
                            <td>:</td>
                            <td><img src="{{ url('/images/layanan/' . $model->gambar) }}"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>



