{!! Form::model($model, [
    'route' => $model->exists ? ['ajax-master-layanan.update', $model->id] : 'ajax-master-layanan.store',
    'method' => $model->exists ? 'PUT' : 'POST',
    'novalidate' => 'novalidate',
    'files' => 'true'
]) !!}

<div class="card mb-2">
    <div class="card-header bg-dark">
        <h6 class="font-weight-bold">Input Data Layanan</h6>
    </div>
    <div class="card-body">
        <div class="form-row">
            <div class="col-md-12">
                <div class="form-group">
                    {!! Form::select('kategori_layanan_id', $listKategoriLayanan, null, ['id' => 'kategori_layanan_id',
                        'class' => 'form-control form-control-sm']) !!}
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-3">
                            {!! Form::text('kode', null, ['class' => 'form-control form-control-sm',
                            'id' => 'kode', 'placeholder' => 'Masukkan Kode Makanan / Layanan']) !!}
                        </div>
                        <div class="col-9">
                            {!! Form::text('nama', null, ['class' => 'form-control form-control-sm',
                            'id' => 'nama', 'placeholder' => 'Masukkan Nama Makanan / Layanan']) !!}
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-6">
                            {!! Form::text('harga', null, ['class' => 'form-control form-control-sm',
                                'id' => 'harga', 'placeholder' => 'Masukkan Harga']) !!}
                        </div>
                        <div class="col-6">
                            {!! Form::select('satuan_id', $listSatuan, null, ['id' => 'satuan_id',
                                'class' => 'form-control form-control-sm']) !!}
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::textarea('keterangan', null, ['id' => 'keterangan', 'placeholder' => 'Masukkan Keterangan (boleh dikosongkan)',
                    'class' => 'form-control form-control-sm', 'rows' => 3]) !!}
                </div>
                <div class="form-group">
                    {!! Form::file('file', ['id' => 'file', 'class' => 'form-control form-control-sm']) !!}
                </div>
                @if($model->exists)
                    {!! Form::hidden('nama_file_lama', $model->gambar) !!}
                @endif
                <div class="card">
                    <div class="card-header bg-dark">
                    <h6 class="font-weight-bold">Gambar</h6>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">
                                <img id="data_gambar" src="{{ url('/images/layanan/unknown.jpg') }}" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
