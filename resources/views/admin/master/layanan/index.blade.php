@extends('layouts.admin')
@section('title', 'Data Master Layanan')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card border-danger">
            <div class="card-header">
                <h3 class="card-title">List Layanan</h3>
                <div class="card-tools">
                    <a class="btn btn-sm btn-success btn-tampil-modal" data-toggle="modal" tipe="tambah"
                        judul="Tambah Layanan Baru" ukuran="besar"
                    href="{{ route('ajax-master-layanan.create') }}">
                        <i class="fa fa-plus"></i></a>
                </div>
            </div>
            <div class="card-body">
                <table id="dataTable" class="table table-sm table-hover tabel-master-layanan">
                    <thead>
                        <tr>
                            <th>Gambar</th>
                            <th>Kategori</th>
                            <th>Kode</th>
                            <th>Nama</th>
                            <th>Harga</th>
                            <th>Satuan</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function() {
            $('#dataTable').DataTable({
                autoWidth: false,
                serverSide: true,
                processing: true,
                responsive: true,
                ajax: "{{ route('table.ajax.master.layanan') }}",
                columns: [
                    { data: 'gambar', nama: 'gambar', render:function(data, tipe, row) {
                        return "<img src='/images/layanan/" + data + "'>";
                    }, orderable: false },
                    { data: 'kategori_layanan', name: 'kategori_layanan' },
                    { data: 'kode', name: 'kode' },
                    { data: 'nama', name: 'nama' },
                    { data: 'harga', name: 'harga', render: function(data, tipe, row) {
                        return 'Rp ' + formatAngka(data.toString());
                    } },
                    { data: 'satuan', name: 'satuan' },
                    { data: 'aksi', name: 'aksi' }
                ],
                language: {
                    lengthMenu: 'Tampil _MENU_ rekord per halaman',
                    info: 'Tampil hal _PAGE_ dari _PAGES_',
                    processing: 'Sedang memproses...',
                    search: 'Cari',
                    zeroRecords: 'Tidak ditemukan rekord',
                    paginate: {
                        first: '<<',
                        last: '>>',
                        next: '>',
                        previous: '<',
                    },
                    infoFiltered: "( filter dari _MAX_ jumlah rekord )"
                }
            })

            $('body').on('change', '#kategori_kamar_id', function(e) {
                e.preventDefault();
                if ($(this).val() !== '') {
                    var kategoriId = $(this).val();
                    tampilInfoKategoriKamar(kategoriId);
                }
            });

            $('body').on('change', '#file', function(E) {
                bacaUrl(this);
            });

            $('#modal').on('shown.bs.modal', function(e){
                var status = $('#modal').attr('tipe');
                if (status === 'edit') {
                    var layananId = $('#modal').attr('kode');
                    var keterangan;

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    //cari data layanan
                    $.ajax({
                        url: '/admin/dapatkan-data-layanan',
                        data: { layananId },
                        type: 'POST',
                        success: function(res) {
                            var layanan = res;
                            gambar = layanan.gambar;
                            $('#data_gambar').attr('src', '/images/layanan/' + gambar);
                        },
                        error: function(e) {
                            console.log(e);
                        }
                    });
                }
            });
        });

        $('body').on('keyup', '#harga', function(e) {
            e.preventDefault();
            $('#harga').val(formatAngka($(this).val()));

            //bikin hidden value
            debugger;
            var integerHarga = accounting.unformat($(this).val());
            bikinHiddenInputAngka('hidden_harga', integerHarga);
        });


    </script>
@endpush
