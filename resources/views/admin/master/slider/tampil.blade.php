<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-primary">
                <h6 class="font-weight-bold">Data Slider</h6>
            </div>
            <div class="card-body">
                <table class="table table-borderless tabel-master-show tabel-tampil-kamar">
                    <tbody>
                        <tr>
                            <td>Tag</td>
                            <td>:</td>
                            <td>{{ $model->tag }}</td>
                        </tr>
                        <tr>
                            <td>Judul</td>
                            <td>:</td>
                            <td>{{ $model->judul }}</td>
                        </tr>
                        <tr>
                            <td>Link</td>
                            <td>:</td>
                            <td>{{ $model->link }}</td>
                        </tr>
                        <tr>
                            <td>Gambar</td>
                            <td>:</td>
                            <td><img src="{{ url('/images/sliders/' . $model->gambar) }}"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>



