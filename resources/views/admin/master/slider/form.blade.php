{!! Form::model($model, [
    'route' => $model->exists ? ['ajax-master-slider.update', $model->id] : 'ajax-master-slider.store',
    'method' => $model->exists ? 'PUT' : 'POST'
]) !!}

<div class="card mb-2">
    <div class="card-header bg-dark">
        <h6 class="font-weight-bold">Input Data Slider</h6>
    </div>
    <div class="card-body">
        <div class="form-row">
            <div class="col-12">
                <div class="form-group">
                    {!! Form::text('tag', null, ['class' => 'form-control form-control-sm', 'id' => 'tag', 
                        'placeholder' => 'Masukkan Nama Tag']) !!}
                </div>
                <div class="form-group">
                    {!! Form::text('judul', null, ['class' => 'form-control form-control-sm', 
                        'id' => 'judul', 'placeholder' => 'Masukkan Judul']) !!}
                </div>
                <div class="form-group">
                    {!! Form::text('link', null, ['class' => 'form-control form-control-sm', 'id' => 'link', 
                       'placeholder' => 'Masukkan Link']) !!}
                </div>
                <div class="form-group">
                    {!! Form::file('file', ['id' => 'file', 'class' => 'form-control form-control-sm']) !!}
                </div>
                @if($model->exists)
                    {!! Form::hidden('nama_file_lama', $model->gambar) !!}
                @endif
            </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-header bg-dark">
        <h6 class="font-weight-bold">Gambar</h6>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-12">
                <img id="data_gambar" src="{{ url('/images/sliders/unknown.jpg') }}" />
            </div>
        </div>
    </div>
</div>




{!! Form::close() !!}
