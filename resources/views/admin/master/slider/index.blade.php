@extends('layouts.admin')
@section('title', 'Data Slider')

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="card border-danger">
            <div class="card-header">
                <h3 class="card-title">List Gambar Slider</h3>
                <div class="card-tools">
                    <a class="btn btn-sm btn-success btn-tampil-modal" data-toggle="modal" tipe="tambah" 
                        judul="Tambah Gambar Slider Baru"
                        href="{{ route('ajax-master-slider.create') }}">
                        <i class="fa fa-plus"></i></a>
                </div>
            </div>
            <div class="card-body">
                <table id="dataTable" class="table table-sm table-hover tabel-master-slider">
                    <thead>
                        <tr>
                            <th>Gambar</th>
                            <th>Judul</th>
                            <th>Tag</th>
                            <th>Link</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
$(document).ready(function() {
    $('#dataTable').DataTable({
        autoWidth: false,
        serverSide: true,
        processing: true,
        responsive: true,
        ajax: "{{ route('table.ajax.master.slider') }}",
        columns: [
            { data: 'gambar', nama: 'gambar', render:function(data, tipe, row) {
                return "<img src='/images/sliders/" + data + "'>";
            }}, 
            { data: 'judul', name: 'judul' },
            { data: 'tag', name: 'tag' },
            { data: 'link', name: 'link', render:function(data, tipe, row) {
                return "<a onclick='window.location.href='" + window.location.host + "/" + data + "' target='_blank'>"  + data + "</a>";
            } },
            { data: 'aksi', name: 'aksi' }
        ],
        language: {
            lengthMenu: 'Tampil _MENU_ rekord per halaman',
            info: 'Tampil hal _PAGE_ dari _PAGES_',
            processing: 'Sedang memproses...',
            search: 'Cari',
            zeroRecords: 'Tidak ditemukan rekord',
            paginate: {
                first: '<<',
                last: '>>',
                next: '>',
                previous: '<',
            },
            infoFiltered: "( filter dari _MAX_ jumlah rekord )"
        }
    })

    $('body').on('change', '#file', function(E) {
        bacaUrl(this);
    });    

    $('#modal').on('shown.bs.modal', function(e){
        var status = $('#modal').attr('tipe');
        if (status === 'edit') {
            var sliderId = $('#modal').attr('kode');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            //cari data slider
            $.ajax({
                url: '/admin/dapatkan-data-slider',
                data: { sliderId },
                type: 'POST',
                success: function(res) {
                    var slider = res;
                    gambar = slider.gambar;
                    $('#data_gambar').attr('src', '/images/sliders/' + gambar);
                },
                error: function(e) {
                    console.log(e);
                }
            });
        }               
    });
})
</script>
@endpush
