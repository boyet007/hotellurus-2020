@extends('layouts.admin')
@section('title', 'Data Satuan')

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="card border-danger">
            <div class="card-header">
                <h3 class="card-title">List Satuan</h3>
                <div class="card-tools">
                    <a class="btn btn-sm btn-success btn-tampil-modal" data-toggle="modal" tipe="tambah" judul="Tambah Satuan Baru"
                    href="{{ route('ajax-master-satuan.create') }}">
                        <i class="fa fa-plus"></i></a>
                </div>
            </div>
            <div class="card-body">
                <table id="dataTable" class="table table-sm table-hover">
                    <thead>
                        <tr>
                            <th width="30">Kode</th>
                            <th>Nama</th>
                            <th width="50">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
$(document).ready(function() {
    $('#dataTable').DataTable({
    autoWidth: false,
    serverSide: true,
    processing: true,
    responsive: true,
    ajax: "{{ route('table.ajax.satuan') }}",
    columns: [
        { data: 'kode', name: 'kode' },
        { data: 'nama', name: 'nama' },
        { data: 'aksi', name: 'aksi' }
    ],
    language: {
        lengthMenu: 'Tampil _MENU_ rekord per halaman',
        info: 'Tampil hal _PAGE_ dari _PAGES_',
        processing: 'Sedang memproses...',
        search: 'Cari',
        zeroRecords: 'Tidak ditemukan rekord',
        paginate: {
            first: '<<',
            last: '>>',
            next: '>',
            previous: '<',
        },
        infoFiltered: "( filter dari _MAX_ jumlah rekord )"
    }
    })
})
</script>
@endpush
