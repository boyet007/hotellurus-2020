{!! Form::model($model, [
    'route' => $model->exists ? ['ajax-master-satuan.update', $model->id] : 'ajax-master-satuan.store',
    'method' => $model->exists ? 'PUT' : 'POST'
]) !!}


<div class="form-row">
    <div class="col-6">
        <div class="form-group">
            @if($model->exists)
                {!! Form::hidden('kode', $model->kode); !!}
                {!! Form::text('kode', null, ['disabled' => 'disabled', 'class' => 'form-control form-control-sm', 'id' => 'kode', 'placeholder' => 'Masukkan kode Satuan']) !!}
            @else
                {!! Form::text('kode', null, ['class' => 'form-control form-control-sm', 'id' => 'kode', 'placeholder' => 'Masukkan kode Satuan']) !!}
            @endif
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            {!! Form::text('nama', null, ['class' => 'form-control form-control-sm', 'id' => 'nama', 'placeholder' => 'Masukkan Nama Kota']) !!}
        </div>
    </div>
</div>

{!! Form::close() !!}
