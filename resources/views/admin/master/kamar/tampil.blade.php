<div class="row">
    <div class="col-md-6 col-sm-12">
        <div class="card">
            <div class="card-header bg-primary">
                <h6 class="font-weight-bold">Data Transaksi</h6>
            </div>
            <div class="card-body">
                <table class="table table-borderless tabel-master-show tabel-tampil-kamar">
                    <tbody>
                        <tr>
                            <td>Nomor Kamar</td>
                            <td>:</td>
                            <td>{{ $model->nomor_kamar }}</td>
                        </tr>
                        <tr>
                            <td>Kategori Kamar</td>
                            <td>:</td>
                            <td>{{ $model->kategori_kamar->nama }}</td>
                        </tr>
                        <tr>
                            <td>Lantai</td>
                            <td>:</td>
                            <td>{{ $model->lantai }}</td>
                        </tr>
                        <tr>
                            <td>Status</td>
                            <td>:</td>
                            <td>{{ $model->status }}</td>
                        </tr>
                        <tr>
                            <td>Keterangan</td>
                            <td>:</td>
                            <td>
                                @if($model->keterangan === null)
                                    -
                                @else
                                    {{ $model->keterangan }}
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>Gambar</td>
                            <td>:</td>
                            <td><img src="{{ url('/images/kamar/' . $model->gambar) }}"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-12">
        <div class="card">
            <div class="card-header bg-success">
                <h6 class="font-weight-bold">Data Kategori Kamar</h6>
            </div>
            <div class="card-body">
                <table class="table table-borderless tabel-master-show">
                    <tr>
                        <td>Nama</td>
                        <td>:</td>
                        <td>{{ $model->kategori_kamar->nama }}</td>
                    </tr>
                    <tr>
                        <td>Harga</td>
                        <td>:</td>
                        <td>Rp @formatUang($model->kategori_kamar->harga_permalam)</td>
                    </tr>
                    <tr>
                        <td>Jumlah Dewasa</td>
                        <td>:</td>
                        <td>{{ $model->kategori_kamar->jumlah_dewasa }} orang</td>
                    </tr>
                    <tr>
                        <td>Jumlah Anak</td>
                        <td>:</td>
                        <td>{{ $model->kategori_kamar->jumlah_anak }} anak</td>
                    </tr>
                    <tr>
                        <td>Jumlah Bed</td>
                        <td>:</td>
                        <td>{{ $model->kategori_kamar->jumlah_bed }} bed</td>
                    </tr>
                    <tr>
                        <td>Keterangan</td>
                        <td>:</td>
                        <td>
                            @if($model->kategori_kamar->keterangan === null)
                                -
                            @else
                                {{ $model->kategori_kamar->keterangan }}</td>
                            @endif
                        </tr>
                </table>
            </div>
        </div>
    </div>
</div>



