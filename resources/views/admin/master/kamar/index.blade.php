@extends('layouts.admin')
@section('title', 'Data Master Kamar')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card border-danger">
            <div class="card-header">
                <h3 class="card-title">List Kamar</h3>
                <div class="card-tools">
                    <a class="btn btn-sm btn-success btn-tampil-modal" data-toggle="modal" tipe="tambah" 
                        judul="Tambah Kamar Baru" ukuran="besar"
                    href="{{ route('ajax-master-kamar.create') }}">
                        <i class="fa fa-plus"></i></a>
                </div>
            </div>
            <div class="card-body">
                <table id="dataTable" class="table table-sm table-hover tabel-master-kamar">
                    <thead>
                        <tr>
                            <th>Gambar</th>
                            <th>Nomor</th>
                            <th>Kategori</th>
                            <th>Harga</th>
                            <th>Lantai</th>
                            <th>Status</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
    <script>
        function tampilInfoKategoriKamar(kategoriId) {
            $.ajaxSetup({
                headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                url: '/admin/dapatkan-info-kategori-kamar',
                data: { kategoriId },
                async: false,
                type: 'POST',
                success: function(res) {
                    $('input[name=data_nama]').val(res.nama);
                    $('input[name=data_harga_permalam]').val(accounting.formatMoney(res.harga_permalam, 'Rp ', 2, ',', '.'));
                    $('input[name=data_jumlah_dewasa]').val(res.jumlah_dewasa + ' orang');
                    $('input[name=data_jumlah_anak]').val(res.jumlah_anak + ' anak');
                    $('input[name=data_bed]').val(res.jumlah_bed + ' bed');
                    $('input[name=data_keterangan]').val(res.keterangan);
                },
                error: function(err) {
                    console.log(err);
                }
            });
        }

        var model = {!! json_encode($test) !!};
        $(document).ready(function() {
            
            $('#dataTable').DataTable({
                autoWidth: false,
                serverSide: true,
                processing: true,
                responsive: true,
                ajax: "{{ route('table.ajax.master.kamar') }}",
                columns: [
                    { data: 'gambar', nama: 'gambar', render:function(data, tipe, row) {
                        return "<img src='/images/kamar/" + data + "'>";
                    }, 
                    orderable: false },
                    { data: 'nomor_kamar', name: 'nomor_kamar' },
                    { data: 'kategori_kamar', name: 'kategori_kamar' },
                    { data: 'harga', name: 'harga', render: function(data, tipe, row) {
                        return 'Rp ' + formatAngka(data.toString()) + ' /mlm';
                    } },
                    { data: 'lantai', nama: 'lantai' },
                    { data: 'status', nama: 'status' },
                    { data: 'aksi', name: 'aksi' }
                ],
                language: {
                    lengthMenu: 'Tampil _MENU_ rekord per halaman',
                    info: 'Tampil hal _PAGE_ dari _PAGES_',
                    processing: 'Sedang memproses...',
                    search: 'Cari',
                    zeroRecords: 'Tidak ditemukan rekord',
                    paginate: {
                        first: '<<',
                        last: '>>',
                        next: '>',
                        previous: '<',
                    },
                    infoFiltered: "( filter dari _MAX_ jumlah rekord )"
                }
            })

            $('body').on('change', '#kategori_kamar_id', function(e) {
                e.preventDefault();
                if ($(this).val() !== '') {
                    var kategoriId = $(this).val();
                    tampilInfoKategoriKamar(kategoriId);
                }
            });

            $('body').on('change', '#file', function(E) {
                bacaUrl(this);
            });

            $('#modal').on('shown.bs.modal', function(e){
                var status = $('#modal').attr('tipe');
                if (status === 'edit') {
                    var kamarId = $('#modal').attr('kode');
                    var kategoriKamarId;
                    var nomorKamar;
                    var lantai;
                    var keterangan;
                    var status;
                    var gambar;
                    
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    //cari data kamar
                    $.ajax({
                        url: '/admin/dapatkan-data-kamar',
                        data: { kamarId },
                        type: 'POST',
                        success: function(res) {
                            var kamar = res;
                            kamarId = kamar.id;
                            kategoriKamarId = kamar.kategori_kamar_id;
                            nomorKamar = kamar.nomor_kamar;
                            lantai = kamar.lantai;
                            keterangan = kamar.keterangan;
                            status = kamar.status;
                            gambar = kamar.gambar;
                            $('#data_gambar').attr('src', '/images/kamar/' + gambar);
                            tampilInfoKategoriKamar(kategoriKamarId);
                        },
                        error: function(e) {
                            console.log(e);
                        }
                    });
<<<<<<< HEAD
                }               
=======
                }
            });

            $('body').on('change', '#file', function(E) {
                bacaUrl(this);
            });

            $('#modal').on('shown.bs.modal', function(e){
                console.log(windowvar.model);
                var status = $('#modal').attr('tipe');
                if(status === 'edit') {
                    debugger;
                    var kamarId = $('#modal').attr('kode');
                    var kategoriKamar;
                    var nomorKamar;
                    var lantai;
                    var keterangan;
                    var status;
                    var gambar;

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    //cari data kategori kamar
                    $.ajax({ 
                        url: '/dapatkan-info-kategori-kamar',
                        data: { },
                        async: false,
                        type: "POST",
                        success: function(data){
                            var produk = data;
                            id = produk.id;
                            satuanId = produk.satuan_id;
                            merekId = produk.merek_id;
                            supplierId = produk.supplier_id;
                            harga = produk.harga;
                            harga_beli = produk.harga_beli;
                            gambar = produk.gambar;

                            if(produk.diskon === null) {
                                diskon = 0;
                            } else {
                                diskon = produk.diskon;
                            }

                            $.ajax({ 
                                url: '/cari-nama-satuan',
                                data: { satuanId },
                                type: "POST",
                                success: function(res){
                                    $('#data_satuan_nama').val(res);
                                },
                                error: function(error) {
                                    console.log(error);
                                }
                            });
                            
                            $.ajax({ 
                                url: '/hitung-harga-diskon',
                                data: { harga, diskon },
                                type: "POST",
                                success: function(res){
                                    var hargaDiskon = res;
                                    $('#data_harga_diskon').val(accounting.formatMoney(hargaDiskon, 'Rp ', 2, ',', '.'));  
                                },
                                error: function(error) {
                                    console.log(error);
                                }
                            });

                            $.ajax({ 
                                url: "/cari-merek-logo",
                                data: { merekId },
                                type: "POST",
                                success: function(data){
                                    var gambar = data;
                                    $('#data_gambar_merek').attr('src', 'web/img/' + gambar);
                                },
                                error: function(error) {
                                    console.log(error);
                                }
                            });

                            $.ajax({ 
                                url: "/cari-data-supplier",
                                data: { supplierId },
                                type: "POST",
                                success: function(data){
                                    $('#data_supplier_alamat').val(data.alamat);
                                    $('#data_supplier_kota').val(data.kota.nama);
                                    $('#data_supplier_pic').val(data.pic);
                                    $('#data_supplier_telpon').val(data.no_telpon);
                                },
                                error: function(error) {
                                    console.log(error);
                                }
                            });

                            $.ajax({ 
                                url: "/cari-gambar-produk",
                                data: { id },
                                type: "POST",
                                success: function(data){
                                    var gambar = data;
                                    $('#data_gambar').attr('src', 'web/img/produk/' + gambar);
                                },
                                error: function(error) {
                                    console.log(error);
                                }
                            });

                            //bikin hidden input untuk harga dan harga beli
                            // bikinHiddenInput('hidden_harga', harga);
                            // bikinHiddenInput('hidden_harga_beli', harga_beli);
                            
                        },
                        error: function(error) {
                            console.log(error);
                        }
                    }); 
                }
>>>>>>> master
            });
        });
    </script>
@endpush
