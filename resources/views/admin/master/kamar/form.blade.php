{!! Form::model($model, [
    'route' => $model->exists ? ['ajax-master-kamar.update', $model->id] : 'ajax-master-kamar.store',
    'method' => $model->exists ? 'PUT' : 'POST',
    'novalidate' => 'novalidate',
    'files' => 'true'
]) !!}

<div class="card mb-2">
    <div class="card-header bg-dark">
        <h6 class="font-weight-bold">Input Data Kamar</h6>
    </div>
    <div class="card-body">
        <div class="form-row">
            <div class="col-md-12 col-lg-6">
                <div class="form-group">
                    {!! Form::select('kategori_kamar_id', $listKategoriKamar, null, ['id' => 'kategori_kamar_id',
                        'class' => 'form-control form-control-sm']) !!}
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-6">
                            {!! Form::number('nomor_kamar', null, ['class' => 'form-control form-control-sm',
                            'id' => 'nomor_kamar', 'min' => 0, 'max' => '999',
                            'placeholder' => 'Masukkan Nomor Kamar']) !!}
                        </div>
                        <div class="col-6">
                            {!! Form::number('lantai', null, ['class' => 'form-control form-control-sm',
                            'id' => 'lantai', 'min' => 0, 'max' => '99',
                            'placeholder' => 'Masukkan Lantai Kamar']) !!}
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::textarea('keterangan', null, ['id' => 'keterangan', 'placeholder' => 'Masukkan Keterangan (boleh dikosongkan)',
                    'class' => 'form-control form-control-sm', 'rows' => 3]) !!}
                </div>
                <div class="form-group">

                    {!! Form::select('status', $listStatus, null, ['id' => 'status', 'class' => 'form-control form-control-sm']) !!}
                </div>
                <div class="form-group">
                    {!! Form::file('file', ['id' => 'file', 'class' => 'form-control form-control-sm']) !!}
                </div>
                @if($model->exists)
                    {{ Form::hidden('nama_file_lama', $model->gambar) }}
                @endif
                <div class="card">
                    <div class="card-header bg-dark">
                    <h6 class="font-weight-bold">Gambar Kamar</h6>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">
                                <img id="data_gambar" src="{{ url('/images/kamar/room-unknown.jpg') }}" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-lg-6">
                <div class="card">
                    <div class="card-header bg-dark">
                        <h6 class="font-weight-bold">Data Kategori Kamar</h6>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-4"><label class="label col-form-label-sm">Nama</label></div>
                            <div class="col-8">{!! Form::text('data_nama', null, ['class' => 'form-control form-control-sm',
                                'disabled' => 'disabled']) !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-4"><label class="label col-form-label-sm">Harga / malam</label></div>
                            <div class="col-8">{!! Form::text('data_harga_permalam', null, ['class' => 'form-control form-control-sm',
                                'disabled' => 'disabled']) !!}</div>
                        </div>
                        <div class="row">
                            <div class="col-4"><label class="label col-form-label-sm">Jumlah dewasa</label></div>
                            <div class="col-8">{!! Form::text('data_jumlah_dewasa', null, ['class' => 'form-control form-control-sm',
                                'disabled' => 'disabled']) !!}</div>
                        </div>
                        <div class="row">
                            <div class="col-4"><label class="label col-form-label-sm">Jumlah anak</label></div>
                            <div class="col-8">{!! Form::text('data_jumlah_anak', null, ['class' => 'form-control form-control-sm',
                                'disabled' => 'disabled']) !!}</div>
                        </div>
                        <div class="row">
                            <div class="col-4"><label class="label col-form-label-sm">Jumlah bed</label></div>
                            <div class="col-8">{!! Form::text('data_bed', null, ['class' => 'form-control form-control-sm',
                                'disabled' => 'disabled']) !!}</div>
                        </div>
                        <div class="row">
                            <div class="col-4"><label class="label col-form-label-sm">Keterangan</label></div>
                            <div class="col-8">{!! Form::textarea('data_keterangan', null, ['class' => 'form-control form-control-sm',
                                'disabled' => 'disabled', 'rows' => 3]) !!}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
