@extends('layouts.admin')
@section('title', 'Data Master User')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card border-danger">
            <div class="card-header">
                <h3 class="card-title">List User</h3>
                <div class="card-tools">
                    <a class="btn btn-sm btn-success btn-tampil-modal" data-toggle="modal" tipe="tambah"
                        judul="Tambah User Baru" ukuran="besar"
                    href="{{ route('ajax-master-user.create') }}">
                        <i class="fa fa-plus"></i></a>
                </div>
            </div>
            <div class="card-body">
                <table id="dataTable" class="table table-sm table-hover tabel-master-user">
                    <thead>
                        <tr>
                            <th>Photo</th>
                            <th>Nama</th>
                            <th>Kota</th>
                            <th>No Telpon</th>
                            <th>Role</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function() {
            $('#dataTable').DataTable({
                autoWidth: false,
                serverSide: true,
                processing: true,
                responsive: true,
                ajax: "{{ route('table.ajax.master.user') }}",
                columns: [
                    { data: 'gambar', nama: 'gambar', render:function(data, tipe, row) {
                        return "<img src='/images/persons/" + data + "'>";
                    },
                    orderable: false },
                    { data: 'nama_lengkap', name: 'nama_lengkap' },
                    { data: 'kota', name: 'kota' },
                    { data: 'no_telpon', name: 'no_telphon'},
                    { data: 'role', name: 'role' },
                    { data: 'aksi', name: 'aksi' }
                ],
                language: {
                    lengthMenu: 'Tampil _MENU_ rekord per halaman',
                    info: 'Tampil hal _PAGE_ dari _PAGES_',
                    processing: 'Sedang memproses...',
                    search: 'Cari',
                    zeroRecords: 'Tidak ditemukan rekord',
                    paginate: {
                        first: '<<',
                        last: '>>',
                        next: '>',
                        previous: '<',
                    },
                    infoFiltered: "( filter dari _MAX_ jumlah rekord )"
                }
            })

            $('body').on('change', '#kategori_kamar_id', function(e) {
                e.preventDefault();
                if ($(this).val() !== '') {
                    var kategoriId = $(this).val();
                    tampilInfoKategoriKamar(kategoriId);
                }
            });

            $('body').on('change', '#file', function(E) {
                bacaUrl(this);
            });

            $('#modal').on('shown.bs.modal', function(e){
                var status = $('#modal').attr('tipe');
                if (status === 'edit') {
                    var userId = $('#modal').attr('kode');

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $.ajax({
                        url: '/admin/dapatkan-data-user',
                        data: { userId },
                        type: 'POST',
                        success: function(res) {
                            var user = res;
                            gambar = user.gambar;
                            $('#data_gambar').attr('src', '/images/persons/' + gambar);
                        },
                        error: function(e) {
                            console.log(e);
                        }
                    });
                }
            });
        });

        $('body').on('keyup', '#harga', function(e) {
            e.preventDefault();
            $('#harga').val(formatAngka($(this).val()));

            //bikin hidden value
            var integerHarga = accounting.unformat($(this).val());
            bikinHiddenInput('hidden_harga', integerHarga);
        });


    </script>
@endpush
