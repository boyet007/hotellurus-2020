<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-primary">
                <h6 class="font-weight-bold">Data User</h6>
            </div>
            <div class="card-body">
                <table class="table table-borderless tabel-master-show tabel-tampil-kamar">
                    <tbody>
                        <tr>
                            <td>Nama Lengkap</td>
                            <td>:</td>
                            <td>{{ $model->nama_lengkap }}</td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td>:</td>
                            <td>{{ $model->email }}</td>
                        </tr>
                        <tr>
                            <td>Alamat</td>
                            <td>:</td>
                            <td>{{ $model->alamat }}</td>
                        </tr>
                        <tr>
                            <td>Kota</td>
                            <td>:</td>
                            <td>{{ $model->kota }}</td>
                        </tr>
                        <tr>
                            <td>No Telpon</td>
                            <td>:</td>
                            <td>{{ $model->no_telpon }}</td>
                        </tr>
                        <tr>
                            <td>Photo</td>
                            <td>:</td>
                            <td><img src="{{ url('/images/persons/' . $model->gambar) }}"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>



