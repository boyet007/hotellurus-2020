{!! Form::model($model, [
    'route' => $model->exists ? ['ajax-master-user.update', $model->id] : 'ajax-master-user.store',
    'method' => $model->exists ? 'PUT' : 'POST',
    'novalidate' => 'novalidate',
    'files' => 'true'
]) !!}

<div class="card mb-2">
    <div class="card-header bg-dark">
        <h6 class="font-weight-bold">Input Data User</h6>
    </div>
    <div class="card-body">
        <div class="form-row">
            <div class="col-md-12">
                <div class="form-group">
                    {!! Form::text('nama_lengkap', null, ['class' => 'form-control form-control-sm',
                    'id' => 'nama_lengkap', 'placeholder' => 'Masukkan Nama Lengkap']) !!}
                </div>
                <div class="form-group">
                    {!! Form::Email('email', null, ['class' => 'form-control form-control-sm',
                    'id' => 'email', 'placeholder' => 'Masukkan Email']) !!}
                </div>
                @if(!$model->exists)
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                {!! Form::password('password', ['id' => 'password', 'class' => 'form-control form-control-sm',
                                    'placeholder' => 'Masukkan Password']) !!}
                            </div>
                            <div class="col-md-6">
                                {!! Form::password('password_confirmation', ['class' => 'form-control form-control-sm',
                                    'placeholder' => 'Konfirmasi Password']) !!}
                            </div>
                        </div>
                    </div>
                @endif
                <div class="form-group">
                    {!! Form::textarea('alamat', null, ['id' => 'alamat', 'placeholder' => 'Masukkan Alamat',
                    'class' => 'form-control form-control-sm', 'rows' => 3]) !!}
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            {!! Form::select('kota', $kota, null, ['id' => 'kota', 'class' => 'form-control form-control-sm']) !!}
                        </div>
                        <div class="col-md-6">
                            {!! Form::text('kodepos', null, ['id' => 'kodepos', 'class' => 'form-control form-control-sm',
                                'placeholder' => 'Masukkan Kode Pos']) !!}
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            {!! Form::select('jenkel', $jenkel, null, ['id' => 'jenkel', 'class' => 'form-control form-control-sm']) !!}
                        </div>
                        <div class="col-md-6">
                            {!! Form::text('no_telpon', null, ['id' => 'no_telpon', 'class' => 'form-control form-control-sm',
                            'placeholder'  => 'No Telpon']) !!}
                        </div>
                    </div>
               </div>
                <div class="form-group">
                    {!! Form::file('file', ['id' => 'file', 'class' => 'form-control form-control-sm']) !!}
                </div>
                @if($model->exists)
                    {!! Form::hidden('nama_file_lama', $model->gambar) !!}
                @endif
                <div class="card">
                    <div class="card-header bg-dark">
                    <h6 class="font-weight-bold">Gambar</h6>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">
                                <img id="data_gambar" src="{{ url('/images/persons/unknown.jpg') }}" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
