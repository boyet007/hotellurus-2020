<!DOCTYPE HTML>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>{{ config('info.NAMA_HOTEL') }} - {{ config('info.TAG_HOTEL') }}</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="author" content="" />

	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content="" />
	<meta property="og:image" content="" />
	<meta property="og:url" content="" />
	<meta property="og:site_name" content="" />
	<meta property="og:description" content="" />
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700" rel="stylesheet">
	<link
		href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:ital,wght@0,200;0,300;0,400;0,700;0,900;1,200;1,300;1,400&display=swap"
		rel="stylesheet">

	<!-- Animate.css -->
	<link rel="stylesheet" href="{{ url('web/css/animate.css') }}">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="{{ url('web/css/icomoon.css') }}">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="{{ url('web/css/bootstrap.css') }}">

	<!-- Magnific Popup -->
	<link rel="stylesheet" href="{{ url('web/css/magnific-popup.css') }}">

	<!-- Flexslider  -->
	<link rel="stylesheet" href="{{ url('web/css/flexslider.css') }}">

	<!-- Owl Carousel -->
	<link rel="stylesheet" href="{{ url('web/css/owl.carousel.min.css') }}">
	<link rel="stylesheet" href="{{ url('web/css/owl.theme.default.min.css') }}">

	<!-- Date Picker -->
	<link rel="stylesheet" href="{{ url('web/css/bootstrap-datepicker.css') }}">
	<!-- Flaticons  -->
	<link rel="stylesheet" href="{{ url('web/fonts/flaticon/font/flaticon.css') }}">

	<!-- Theme style  -->
	<link rel="stylesheet" href="{{ url('web/css/style.css') }}">

	<!-- Theme style  -->
	<link rel="stylesheet" href="{{ url('web/css/custom.css') }}">

	<!-- Modernizr JS -->
	<script src="{{ url('web/js/modernizr-2.6.2.min.js') }}"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="{{ url('web/js/respond.min.js') }}"></script>
	<![endif]-->

</head>

<body>
	<div class="colorlib-loader"></div>
	<div id="page">
		<nav class="colorlib-nav" role="navigation">
			<div class="top">
				<div class="container">
					<div class="row">
						<div class="col-xs-4">
							<p class="site">{{ config('info.EMAIL') }}</p>
						</div>
						<div class="col-xs-8 text-right">
							<p class="num">Telpon: {{ config('info.TELPON') }}</p>
						</div>
					</div>
				</div>
			</div>
			<div class="top-menu">
				<div class="container">
					<div class="row">
						<div class="col-xs-2">
							<div id="colorlib-logo"><a href="{{ url('/') }}"><img
										src="{{ url('/images/' . config('info.LOGO')) }}" /></a></div>
						</div>
						<div class="col-xs-10 text-right menu-1">
							<ul>
								<li {{ Request::path() === '/' ? 'class=active' : null }}><a
										href="{{ url('/') }}">Beranda</a></li>
								<li class="has-dropdown">
									<a href="{{ url('/kamar/all') }}">Kamar</a>
									<ul class="dropdown">
										@foreach($kategoriKamar as $kategori)
											<li><a href="{{ route('kamar', $kategori->slug) }}">{{ $kategori->nama }}</a></li>
										@endforeach
									</ul>
								</li>
								<li {{ Request::path() === 'amenities' ? 'class=active' : null }}><a
										href="{{ url('/amenities') }}">Amenities</a></li>
								<li {{ Request::path() === 'tentang-kami' ? 'class=active' : null }}><a
										href="{{ url('/tentang-kami') }}">Tentang Kami</a></li>
								<li {{ Request::path() === 'kontak-kami' ? 'class=active' : null }}><a
										href="{{ url('/kontak-kami') }}">Kontak</a></li>
								@if(Auth::check())
								<li class="has-dropdown">
									<a href="{{ url('/') }}">{{ Auth::user()->email }}</a>
									<ul class="dropdown">
										@if(Auth::user()->role === 'admin')
										<li><a href="{{ url('/admin/dashboard') }}">Panel Admin</a></li>
										@endif
										<li><a href="{{ url('/logout') }}">Logout</a></li>
									</ul>
								</li>
								@else
								<li {{ Request::path() === 'login' ? 'class=active' : null }}><a
										href="{{ url('/login') }}">Login</a></li>
								@endif
							</ul>
						</div>
					</div>
				</div>
			</div>
		</nav>

		@yield('content')

		<footer id="colorlib-footer" role="contentinfo">
			<div class="container">
				<div class="row row-pb-md">
					<div class="col-md-4 colorlib-widget">
						<h4>{{ config('info.NAMA_HOTEL') }}</h4>
						<p style="text-align:justify">{{ config('info.DESKRIPSI') }}</p>
					</div>
					<div class="col-md-4">

					</div>

					<div class="col-md-4 col-md-push-1">
						<h4>Kontak</h4>
						<ul class="colorlib-footer-links">
							<li>{{ config('info.ALAMAT_JALAN') }}, <br> {{ config('info.ALAMAT_KABUPATEN') }}
								{{ config('info.ALAMAT_KOTA') }}, {{ config('info.ALAMAT_PROVINSI') }}
								{{ config('info.ALAMAT_KODEPOS') }}</li>
							<li><a href="tel://{{ config('info.TELPON_BASIC') }}">{{ config('info.TELPON') }}</a></li>
							<li><a href="mailto:{{ config('info.EMAIL') }}">{{ config('info.EMAIL') }}</a></li>
							<li><a href="//{{ config('info.WEBSITE') }}">{{ config('info.WEBSITE') }}</a></li>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 text-center">
						<p>
							<small class="block">
								<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
								Copyright &copy;<script>
									document.write(new Date().getFullYear());
								</script> All rights reserved | This template is made with <i class="icon-heart3"
									aria-hidden="true"></i> by <a href="https://colorlib.com"
									target="_blank">Colorlib</a>
								<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></small>
						</p>
					</div>
				</div>
			</div>
		</footer>
	</div>

	<div class="gototop js-top">
		<a href="#" class="js-gotop"><i class="icon-arrow-up2"></i></a>
	</div>

	<!-- jQuery -->
	<script src="{{ url('web/js/jquery.min.js') }}"></script>
	<!-- jQuery Easing -->
	<script src="{{ url('web/js/jquery.easing.1.3.js') }}"></script>
	<!-- Bootstrap -->
	<script src="{{ url('web/js/bootstrap.min.js') }}"></script>
	<!-- Waypoints -->
	<script src="{{ url('web/js/jquery.waypoints.min.js') }}"></script>
	<!-- Flexslider -->
	<script src="{{ url('web/js/jquery.flexslider-min.js') }}"></script>
	<!-- Owl carousel -->
	<script src="{{ url('web/js/owl.carousel.min.js') }}"></script>
	<!-- Magnific Popup -->
	<script src="{{ url('web/js/jquery.magnific-popup.min.js') }}"></script>
	<script src="{{ url('web/js/magnific-popup-options.js') }}"></script>
    <!-- Moment js -->
    <script src="{{ url('admin/plugins/moment/moment.min.js') }}"></script>
    <!-- Date Picker -->
	<script src="{{ url('web/js/bootstrap-datepicker.js') }}"></script>
	<!-- Main -->
    <script src="{{ url('web/js/main.js') }}"></script>
    <!-- Custom -->
    <script src="{{ url('web/js/custom.js') }}"></script>
</body>

</html>
