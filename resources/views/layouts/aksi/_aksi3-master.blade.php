<a href="{{ $url_show }}" class="btn-tampil-modal" tipe="lihat" judul="Detail: {{ $model->nama }}">
    <i class="fas fa-eye text-primary"></i></a>

<a href="{{ $url_edit }}" class="btn-tampil-modal" tipe="edit" 
    data-kode="{{ $model->id }}" judul="Edit: {{ $model->nama }}">
    <i class="fas fa-pencil-alt text-warning"></i></a>

<a href="{{ $url_destroy }}" judul='{{ $model->nama }}'
    class="btn-hapus"><i class="fas fa-trash text-danger"></i></a>
