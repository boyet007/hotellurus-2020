@extends('layouts.web')

@section('content')
    @if (session()->has('sukses'))
        <div class="row">
            <div class="col-md-offset-3 col-md-6">
                <div class="alert alert-success text-center">
                    {{ session('sukses') }}
                </div>
            </div>
        </div>
    @elseif (session()->has('error'))
    <div class="row">
        <div class="col-md-offset-3 col-md-6">
            <div class="alert alert-danger text-center">
                {{ session('error') }}
            </div>
        </div>
    </div>
    @endif
<aside id="colorlib-hero">
    <div class="flexslider">
        <ul class="slides">
            @foreach($listSlider as $slider)
                <li style="background-image: url(/images/sliders/{{ $slider->gambar }}">
                    <div class="overlay"></div>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6 col-sm-12 col-md-offset-3 slider-text">
                                <div class="slider-text-inner text-center">
                                    <h2>{{ $slider->tag }}</h2>
                                    <h1>{{ $slider->judul }}</h1>
                                    <p><a href="{{ url($slider->link) }}" class="btn btn-primary btn-learn">Lebih Lanjut</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            @endforeach
        </ul>
    </div>
</aside>
<div id="colorlib-services">
    <div class="container">
        <div class="row">
            <div class="col-md-3 animate-box text-center">
                <div class="services">
                    <span class="icon">
                        <i class="{{ config('situs.BERANDA_FRONT_ICON_1') }}"></i>
                    </span>
                    <h3>{{ config('situs.BERANDA_FRONT_TITLE_1') }}</h3>
                    <p>{{ config('situs.BERANDA_FRONT_PARAGRAPH_1') }}</p>
                </div>
            </div>
            <div class="col-md-3 animate-box text-center">
                <div class="services">
                    <span class="icon">
                        <i class="{{ config('situs.BERANDA_FRONT_ICON_3') }}"></i>
                    </span>
                    <h3>{{ config('situs.BERANDA_FRONT_TITLE_3') }}</h3>
                    <p>{{ config('situs.BERANDA_FRONT_PARAGRAPH_3') }}</p>
                </div>
            </div>
            <div class="col-md-3 animate-box text-center">
                <div class="services">
                    <span class="icon">
                        <i class="{{ config('situs.BERANDA_FRONT_ICON_4') }}"></i>
                    </span>
                    <h3>{{ config('situs.BERANDA_FRONT_TITLE_4') }}</h3>
                    <p>{{ config('situs.BERANDA_FRONT_PARAGRAPH_4') }}</p>
                </div>
            </div>


        </div>
    </div>
</div>

<div id="colorlib-rooms" class="colorlib-light-grey">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center colorlib-heading animate-box">
                <span><i class="icon-star-full"></i><i class="icon-star-full"></i><i class="icon-star-full"></i><i
                        class="icon-star-full"></i><i class="icon-star-full"></i></span>
                <h2>{{ config('situs.BERANDA_ROOM_TITLE') }}</h2>
                <p>{{ config('situs.BERANDA_ROOM_PARAGRAPH') }}</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 animate-box">
                <div class="owl-carousel owl-carousel2">

                    @foreach($kategoriKamar as $kategori)
                        <?php

                            if ($kategori->kamar->count() > 0) {
                                $gambar = Arr::random($kategori->kamar->toArray())['gambar'];
                            }

                            $listKeterangan = $kategori->listKamarKeterangan;
                        ?>

                        @if($kategori->kamar->count() > 0)
                            <div class="item">
                                <a href="{{ url('/images/kamar/' . $gambar) }}" class="room image-popup-link"
                                    style="background-image: url(/images/kamar/{{ $gambar }});"></a>
                                <div class="desc text-center">
                                    <h3><a href="rooms-suites.html">{{ $kategori->nama }}</a></h3>
                                    <p class="price">
                                        <span class="currency">Rp</span>
                                        <span class="price-room">@formatUang($kategori->harga_permalam)</span>
                                        <span class="per">/ per malam</span>
                                    </p>
                                    <ul>
                                        @foreach($listKeterangan as $keterangan)
                                            <li><i class="icon-check"></i> {{ $keterangan->list_keterangan }}</li>
                                        @endforeach
                                    </ul>
                                    <p><a href="{{ route('kamar',$kategori->slug) }}" class="btn btn-primary btn-lihat">Lihat Kamar</a></p>
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
            <div class="col-md-12 text-center animate-box">
                <a href="#">Lihat Semua Kamar <i class="icon-arrow-right3"></i></a>
            </div>
        </div>
    </div>
</div>


<div id="colorlib-dining-bar">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center colorlib-heading animate-box">
                <span><i class="icon-star-full"></i><i class="icon-star-full"></i><i class="icon-star-full"></i><i
                        class="icon-star-full"></i><i class="icon-star-full"></i></span>
                <h2>{{ config('situs.BERANDA_DINING_TITLE') }}</h2>
                <p>{{ config('situs.BERANDA_DINING_PARAGRAPH') }}</p>
            </div>
        </div>
        <div class="row">
            <div class="diningbar-flex">
                <div class="half animate-box">
                    <ul class="nav nav-tabs text-center" role="tablist">
                        <li role="presentation" class="active"><a href="#mains" aria-controls="mains" role="tab"
                                data-toggle="tab">{{ $menu1->nama }}</a></li>
                        <li role="presentation"><a href="#desserts" aria-controls="desserts" role="tab"
                                data-toggle="tab">{{ $menu2->nama }}</a></li>
                    </ul>

                    <?php
                        $listMenu1 = $menu1->layanan;
                        $listMenu2 = $menu2->layanan;
                    ?>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="mains">
                            <div class="row">
                                <div class="col-md-12">
                                    <ul class="menu-dish">
                                        @foreach($listMenu1 as $list)
                                            <li>
                                                <figure class="image"><img src="{{ url('/images/layanan/' . $list->gambar) }}"
                                                        alt="{{ $list->nama }}"></figure>
                                                <div class="text">
                                                    <span class="price">Rp @formatUang($list->harga)</span>
                                                    <h3>{{ $list->nama }}</h3>
                                                </div>
                                            </li>
                                       @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div role="tabpanel" class="tab-pane" id="desserts">
                            <div class="row">
                                <div class="col-md-12">
                                    <ul class="menu-dish">
                                        @foreach($listMenu2 as $list)
                                            <li>
                                                <figure class="image"><img src="{{ url('/images/layanan/' . $list->gambar) }}"
                                                        alt="{{ $list->nama }}"></figure>
                                                <div class="text">
                                                    <span class="price">Rp @formatUang($list->harga)</span>
                                                    <h3>{{ $list->nama }}</h3>
                                                </div>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </div>
                </div><!-- end half -->
                <?php
                    $gambar = Arr::random($listMenu1->toArray())['gambar'];
                ?>
                <div class="half diningbar-img" style="background-image: url(/images/{{ config('situs.BERANDA_DINING_IMAGE_BACKGROUND') }});"></div>
                <!-- end half -->
            </div>
        </div>
    </div>
</div>

@stop
