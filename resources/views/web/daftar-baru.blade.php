@extends('layouts.web')

@section('content')
<aside id="colorlib-hero">
    <div class="flexslider">
        <ul class="slides">
            <li style="background-image: url(/images/sliders/{{ config('situs.DAFTAR_BARU_TOP_IMAGE') }}); width: 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2;"
                class="flex-active-slide">
                <div class="overlay"></div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6 col-sm-12 col-md-offset-3 slider-text animated fadeInUp">
                            <div class="slider-text-inner slider-text-inner2 text-center">
                                <h2>{{ config('situs.DAFTAR_TAG') }}</h2>
                                <h1>{{ config('situs.DAFTAR_TITLE') }}</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
        <ol class="flex-control-nav flex-control-paging"></ol>
        <ul class="flex-direction-nav">
            <li class="flex-nav-prev"><a class="flex-prev flex-disabled" href="#" tabindex="-1">Previous</a></li>
            <li class="flex-nav-next"><a class="flex-next flex-disabled" href="#" tabindex="-1">Next</a></li>
        </ul>
    </div>
</aside>
<div class="colorlib-contact">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 animate-box" style="margin-top:10px">
                @if(session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
                @endif
                <h3>{{ config('situs.DAFTAR_SUB_TITLE') }}</h3>
                {!! Form::open(['route' => 'post.daftar.baru', 'class' => 'form-registering']) !!}
                {!! Form::text('nama_lengkap', '', ['id' => 'nama_lengkap', 'class' => 'form-control', 'placeholder' =>
                'Nama Lengkap', 'autofocus' => 'autofocus']) !!}
                @if($errors->has('nama_lengkap'))
                <span class="help-block">{{ $errors->first('nama_lengkap') }}</span>
                @endif
                {!! Form::text('email', '', ['id' => 'email', 'class' => 'form-control', 'placeholder' => 'Email']) !!}
                @if($errors->has('email'))
                <span class="help-block">{{ $errors->first('email') }}</span>
                @endif
                <div class="row">
                    <div class="col-md-6">
                        {!! Form::password('password', ['id' => 'password', 'class' => 'form-control', 'placeholder' =>
                        'Password']) !!}
                        @if($errors->has('password'))
                        <span class="help-block">{{ $errors->first('password') }}</span>
                        @endif
                    </div>
                    <div class="col-md-6">
                        {!! Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' =>
                        'Konfirmasi Password']) !!}
                        @if($errors->has('password_confirmation'))
                        <span class="help-block">{{ $errors->first('password_confirmation') }}</span>
                        @endif
                    </div>
                </div>
                {!! Form::textarea('alamat', '', ['id' => 'alamat', 'class' => 'form-control', 'placeholder' =>
                'Alamat']) !!}
                @if($errors->has('alamat'))
                <span class="help-block">{{ $errors->first('alamat') }}</span>
                @endif
                <div class="row">
                    <div class="col-sm-6">
                        {!! Form::select('kota', $kota, null, ['id' => 'kota', 'class' => 'form-control']) !!}
                        @if($errors->has('kota'))
                        <span class="help-block">{{ $errors->first('kota') }}</span>
                        @endif
                    </div>
                    <div class="col-sm-6">
                        {!! Form::text('kodepos', null, ['id' => 'kodepos', 'placeholder' => 'Kode Pos', 'class' =>
                        'form-control']) !!}
                        @if($errors->has('kodepos'))
                        <span class="help-block">{{ $errors->first('kodepos') }}</span>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        {!! Form::select('jenkel', $jenkel, null, ['id' => 'jenkel', 'class' => 'form-control']) !!}
                        @if($errors->has('jenkel'))
                        <span class="help-block">{{ $errors->first('jenkel') }}</span>
                        @endif
                    </div>
                    <div class="col-sm-6">
                        {!! Form::text('no_telpon', null, ['id' => 'no_telpon', 'class' => 'form-control', 'placeholder'
                        => 'No Telpon']) !!}
                        @if($errors->has('no_telpon'))
                        <span class="help-block">{{ $errors->first('no_telpon') }}</span>
                        @endif
                    </div>
                </div>

                {!! Form::submit('Daftar', ['class' => 'btn btn-login']) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>


@stop
