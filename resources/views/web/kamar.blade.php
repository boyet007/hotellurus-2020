@extends('layouts.web')

@section('content')

<aside id="colorlib-hero">
    <div class="flexslider">
        <ul class="slides">
            <li style="background-image: url(/images/sliders/{{ config('situs.KAMAR_BACKGROUND') }}); width: 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2;"
                class="flex-active-slide">
                <div class="overlay"></div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6 col-sm-12 col-md-offset-3 slider-text animated fadeInUp">
                            <div class="slider-text-inner slider-text-inner2 text-center">
                                <h2>{{ config('situs.KAMAR_TAG') }}</h2>
                                <h1>{{ config('situs.KAMAR_TITLE') }}</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
        <ol class="flex-control-nav flex-control-paging"></ol>
        <ul class="flex-direction-nav">
            <li class="flex-nav-prev"><a class="flex-prev flex-disabled" href="#" tabindex="-1">Previous</a></li>
            <li class="flex-nav-next"><a class="flex-next flex-disabled" href="#" tabindex="-1">Next</a></li>
        </ul>
    </div>
</aside>

<div id="colorlib-rooms" class="colorlib-light-grey">
    <div class="container">
        <div class="row">
            @foreach($kamar as $k)
                <div class="col-md-4 room-wrap animate-box fadeInUp animated-fast">
                    <a href="/images/kamar/{{ $k->gambar }}" class="room image-popup-link"
                        style="background-image: url(/images/kamar/{{ $k->gambar }});"></a>
                    <div class="desc text-center">
                        <h4>Nomor : {{ $k->nomor_kamar }}</h4>
                        <h3>{{ $k->kategori_kamar['nama'] }}</h3>
                        <p class="price">
                            <span class="currency">Rp</span>
                            <span class="price-room">@formatUang($k->kategori_kamar['harga_permalam'])</span>
                            <span class="per">/ per malam</span>
                        </p>
                        <ul>
                            <?php
                                $keterangan = $k->kategori_kamar['listKamarKeterangan'];
                            ?>

                            @if(is_array($keterangan) || is_object($keterangan))
                                @foreach($keterangan as $ket)

                                    <li><i class="icon-check"></i> {{ $ket->list_keterangan }}</li>
                                @endforeach
                            @endif
                        </ul>
                        <p><a href="{{ route('detail.kamar', $k->id) }}" class="btn btn-primary">Cek Detail</a></p>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
@stop
