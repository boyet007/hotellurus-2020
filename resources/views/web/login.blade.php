@extends('layouts.web')

@section('content')
<aside id="colorlib-hero">
    <div class="flexslider">
        <ul class="slides">
            <li style="background-image: url(images/sliders/{{ config('situs.KAMAR_BACKGROUND') }}); width: 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2;"
                class="flex-active-slide">
                <div class="overlay"></div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6 col-sm-12 col-md-offset-3 slider-text animated fadeInUp">
                            <div class="slider-text-inner slider-text-inner2 text-center">
                                <h2>{{ config('situs.LOGIN_TAG') }}</h2>
                                <h1>{{ config('situs.LOGIN_TITLE') }}</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
        <ol class="flex-control-nav flex-control-paging"></ol>
        <ul class="flex-direction-nav">
            <li class="flex-nav-prev"><a class="flex-prev flex-disabled" href="#" tabindex="-1">Previous</a></li>
            <li class="flex-nav-next"><a class="flex-next flex-disabled" href="#" tabindex="-1">Next</a></li>
        </ul>
    </div>
</aside>
<div class="login">
    {!! Form::open(['route' => 'post.login','class' => 'form-signin']) !!}
    @if(session('error')) 
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
    @endif
        <h2 class="text-login text-center form-signin-heading">{{ config('situs.LOGIN_SUB_TITLE') }}</h2>
        {!! Form::text('email', '', ['id' => 'email', 'class' => 'form-control', 'placeholder' => 'Email', 'autofocus' => 'autofocus']) !!}
        @if($errors->has('email'))
            <span class="help-block">{{ $errors->first('email') }}</span>
        @endif
        {!! Form::password('password', ['id' => 'password', 'class' => 'form-control', 'placeholder' => 'Password']) !!}
        @if($errors->has('password'))
            <span class="help-block">{{ $errors->first('password') }}</span>
        @endif
        {!! Form::submit('Login', ['class' => 'btn btn-login btn-block']) !!}
        <div class="text-right">
            <a href="{{ url('/daftar-baru') }}" class="text-info">Daftar Baru</a>
        </div>
    {!! Form::close() !!}
</div>

@stop