@extends('layouts.web')

@section('content')
<aside id="colorlib-hero">
    <div class="flexslider">
        <ul class="slides">
            <li style="background-image: url(/images/sliders/{{ config('situs.DETAIL_KAMAR_TOP_IMAGE') }}); width: 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2;"
                class="flex-active-slide">
                <div class="overlay"></div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6 col-sm-12 col-md-offset-3 slider-text animated fadeInUp">
                            <div class="slider-text-inner slider-text-inner2 text-center">
                                <h2>{{ config('situs.DETAIL_KAMAR_TAG') }}</h2>
                                <h1>{{ config('situs.DETAIL_KAMAR_TOP_IMAGE') }}</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
        <ol class="flex-control-nav flex-control-paging"></ol>
        <ul class="flex-direction-nav">
            <li class="flex-nav-prev"><a class="flex-prev flex-disabled" href="#" tabindex="-1">Previous</a></li>
            <li class="flex-nav-next"><a class="flex-next flex-disabled" href="#" tabindex="-1">Next</a></li>
        </ul>
    </div>
</aside>
<div id="colorlib-contact">
    <div class="container detail-kamar">
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-primary panel-gambar">
                    <div class="panel-heading">
                        Gambar Kamar
                    </div>
                    <div class="panel-body">
                        <img class="gambar-kamar" src="{{ url('/images/kamar/' . $kamar->gambar) }}" alt="">
                    </div>
                </div>

            </div>
            <div class="col-md-6">
                <div class="panel panel-primary panel-data">
                    <div class="panel-heading">
                        Data Kamar
                    </div>

                    <div class="panel-body">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td>Nomor</td>
                                    <td>:</td>
                                    <td>{{ $kamar->nomor_kamar }}</td>
                                </tr>
                                <tr>
                                    <td>Kategori Kamar</td>
                                    <td>:</td>
                                    <td>{{ $kamar->kategori_kamar->nama }}</td>
                                </tr>
                                <tr>
                                    <td>Harga permalam</td>
                                    <td>:</td>
                                    <td>Rp @formatUang($kamar->kategori_kamar->harga_permalam)</td>
                                </tr>
                                <tr>
                                    <td>Lantai</td>
                                    <td>:</td>
                                    <td>{{ $kamar->lantai }}</td>
                                </tr>
                                <tr>
                                    <td>Status</td>
                                    <td>:</td>
                                    <td>{{ $kamar->status }}</td>
                                </tr>
                                <tr>
                                    <td>Jumlah Bed</td>
                                    <td>:</td>
                                    <td>{{ $kamar->kategori_kamar->jumlah_bed }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-offset-3 col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        Keterangan
                    </div>
                    <div class="panel-body">
                        <ul>
                            @foreach($listKeterangan as $ket)
                                <li>{{ $ket->list_keterangan }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection
