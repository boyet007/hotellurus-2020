@extends('layouts.web')

@section('content')
<aside id="colorlib-hero">
    <div class="flexslider">
        <ul class="slides">
            <li style="background-image: url(/images/sliders/{{ config('situs.KONTAK_KAMI_IMAGE_BACKGROUND') }}); width: 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2;"
                class="flex-active-slide">
                <div class="overlay"></div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6 col-sm-12 col-md-offset-3 slider-text animated fadeInUp">
                            <div class="slider-text-inner slider-text-inner2 text-center">
                                <h2>{{ config('situs.KONTAK_KAMI_TAG') }}</h2>
                                <h1>{{ config('situs.KONTAK_KAMI_TITLE') }}</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
        <ol class="flex-control-nav flex-control-paging"></ol>
        <ul class="flex-direction-nav">
            <li class="flex-nav-prev"><a class="flex-prev flex-disabled" href="#" tabindex="-1">Previous</a></li>
            <li class="flex-nav-next"><a class="flex-next flex-disabled" href="#" tabindex="-1">Next</a></li>
        </ul>
    </div>
</aside>
<div id="colorlib-contact">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 animate-box">
                <h3>Informasi Kontak</h3>
                <div class="row contact-info-wrap">
                    <div class="col-md-3">
                        <p><span><i class="icon-location-2"></i></span> {{ config('info.ALAMAT_JALAN') }}, <br>
                            {{ config('info.ALAMAT_KABUPATEN') }}, {{ config('info.ALAMAT_KOTA') }}
                            {{ config('info.ALAMAT_KODEPOS') }}</p>
                    </div>
                    <div class="col-md-3">
                        <p><span><i class="icon-phone3"></i></span> <a
                                href="tel://{{ config('info.TELPON_BASIC') }}">{{ config('info.TELPON') }}</a></p>
                    </div>
                    <div class="col-md-3">
                        <p><span><i class="icon-paperplane"></i></span> <a
                                href="mailto:{{ config('info.EMAIL') }}">{{ config('info.EMAIL') }}</a></p>
                    </div>
                    <div class="col-md-3">
                        <p><span><i class="icon-globe"></i></span> <a
                                href="//{{ config('info.WEBSITE') }}">{{ config('info.WEBSITE') }}</a></p>
                    </div>
                </div>
            </div>
            <div class="col-md-10 col-md-offset-1 animate-box">
                @if(session('sukses'))
                <div class="alert alert-success">
                    {{ session('sukses') }}
                </div>
                @endif
                <h3>{{ config('situs.KONTAK_KAMI_TITLE') }}</h3>
                {!! Form::open(['route' => 'post.kontak.kami']) !!}
                <div class="row">
                    <div class="col-md-6">
                        {!! Form::text('nama_lengkap', '', ['id' => 'nama_lengkap', 'class' => 'form-control',
                        'placeholder' => 'Masukkan nama lengkap anda']) !!}
                        @if($errors->has('nama_lengkap'))
                        <span class="help-block">{{ $errors->first('nama_lengkap') }}</span>
                        @endif
                    </div>
                    <div class="col-md-6">
                        {!! Form::text('no_telpon', '', ['id' => 'no_telpon', 'class' => 'form-control', 'placeholder'
                        => 'Masukkan telpon anda']) !!}
                        @if($errors->has('no_telpon'))
                        <span class="help-block">{{ $errors->first('no_telpon') }}</span>
                        @endif
                    </div>
                </div>
                {{ Form::text('email', '', ['id' => 'email', 'class' => 'form-control', 'placeholder' => 'Masukkan email anda']) }}
                @if($errors->has('email'))
                <span class="help-block">{{ $errors->first('email') }}</span>
                @endif
                {!! Form::text('subyek', '', ['id' => 'subyek', 'class' => 'form-control', 'placeholder' => 'Masukkan subjek yang ingin disampaikan']) !!}
                @if($errors->has('subyek'))
                <span class="help-block">{{ $errors->first('subyek') }}</span>
                @endif
                {!! Form::textarea('pesan', '', ['id' => 'pesan', 'class' => 'form-control', 'placeholder' => 'Masukkan pesan anda', 'cols' => '30', 'rows' => '10']) !!}
                @if($errors->has('pesan'))
                <span class="help-block">{{ $errors->first('pesan') }}</span>
                @endif
                {!! Form::submit('Kirim Pesan', ['class' => 'btn btn-login']) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
<div id="map" class="colorlib-map"></div>
@stop