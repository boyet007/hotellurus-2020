@extends('layouts.web')

@section('content')
<aside id="colorlib-hero">
    <div class="flexslider">
        <ul class="slides">
            <li style="background-image: url(/images/sliders/{{ config('situs.TENTANG_KAMI_TOP_IMAGE') }}); width: 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2;"
                class="flex-active-slide">
                <div class="overlay"></div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6 col-sm-12 col-md-offset-3 slider-text animated fadeInUp">
                            <div class="slider-text-inner slider-text-inner2 text-center">
                                <h2>{{ config('situs.TENTANG_KAMI_TAG') }}</h2>
                                <h1>{{ config('situs.TENTANG_KAMI_TITLE') }}</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
        <ol class="flex-control-nav flex-control-paging"></ol>
        <ul class="flex-direction-nav">
            <li class="flex-nav-prev"><a class="flex-prev flex-disabled" href="#" tabindex="-1">Previous</a></li>
            <li class="flex-nav-next"><a class="flex-next flex-disabled" href="#" tabindex="-1">Next</a></li>
        </ul>
    </div>
</aside>
<div id="colorlib-about">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="about animate-box fadeInUp animated-fast">
                    <h2>{{ config('situs.TENTANG_KAMI_CONTENT_TITLE') }}</h2>
                    <p style="text-align: justify">{{ config('situs.TENTANG_KAMI_CONTENT_PARAGRAPH') }}</p>
                </div>
            </div>
            <div class="col-md-6">
                <img class="img-responsive" src="{{ url('images/sliders/' . config('situs.TENTANG_KAMI_CONTENT_IMAGE')) }}"
                    alt="Free HTML5 Bootstrap Template by colorlib.com">
            </div>
        </div>
    </div>
</div>
@stop