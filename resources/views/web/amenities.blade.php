@extends('layouts.web')

@section('content')
<aside id="colorlib-hero">
    <div class="flexslider">
        <ul class="slides">
            <li style="background-image: url(/images/sliders/{{ config('situs.AMENITIES_BACKGROUND') }}); width: 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2;"
                class="flex-active-slide">
                <div class="overlay"></div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6 col-sm-12 col-md-offset-3 slider-text animated fadeInUp">
                            <div class="slider-text-inner slider-text-inner2 text-center">
                                <h2>{{ config('situs.AMENITIES_TAG') }}</h2>
                                <h1>{{ config('situs.AMENITIES_TITLE') }}</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
        <ol class="flex-control-nav flex-control-paging"></ol>
        <ul class="flex-direction-nav">
            <li class="flex-nav-prev"><a class="flex-prev flex-disabled" href="#" tabindex="-1">Previous</a></li>
            <li class="flex-nav-next"><a class="flex-next flex-disabled" href="#" tabindex="-1">Next</a></li>
        </ul>
    </div>
</aside>
<div id="colorlib-amenities">
    <div class="container">
        <div class="row">
            <div class="amenities-flex">
                <div class="amenities-img animate-box fadeInUp animated-fast" style="background-image: url(/images/amenities/{{ config('situs.AMENITIES_1_GAMBAR') }});"></div>
                <div class="desc animate-box fadeInUp animated-fast">
                    <h2><a href="#">{{ config('situs.AMENITIES_1_TITLE') }}</a></h2>
                    <p class="price">
                        <span class="free">
                            @if(config('situs.AMENITIES_1_HARGA') !== 0)
                                <p class="price">
                                    <span class="currency">Rp</span>
                                    <span class="price-room">@formatUang(config('situs.AMENITIES_1_HARGA'))</span>
                                    <span class="per">/ {{ config('situ.AMENITIES_1_SATUAN') }}</span>
                                </p>
                            @else 
                                <p class="price">
                                    <span class="free">Gratis</span>
                                </p>
                            @endif
                        </span>
                    </p>
                    <p>{{ config('situs.AMENITIES_1_KETERANGAN') }}</p>
                </div>
                <div class="desc animate-box fadeInUp animated-fast">
                    <h2><a href="#">{{ config('situs.AMENITIES_2_TITLE') }}</a></h2>
                    <p class="price">
                        <span class="free">
                            @if(config('situs.AMENITIES_1_HARGA') !== 0)
                                <p class="price">
                                    <span class="currency">Rp</span>
                                    <span class="price-room">@formatUang(config('situs.AMENITIES_2_HARGA'))</span>
                                    <span class="per">/ {{ config('situ.AMENITIES_2_SATUAN') }}</span>
                                </p>
                            @else 
                                <p class="price">
                                    <span class="free">Gratis</span>
                                </p>
                            @endif
                        </span>
                    </p>
                    <p>{{ config('situs.AMENITIES_2_KETERANGAN') }}</p>
                </div>
                <div class="amenities-img animate-box fadeInUp animated-fast" style="background-image: url(/images/amenities/{{ config('situs.AMENITIES_2_GAMBAR') }});"></div>
                <div class="amenities-img animate-box fadeInUp animated-fast" style="background-image: url(/images/amenities/{{ config('situs.AMENITIES_3_GAMBAR') }});"></div>
                <div class="desc animate-box fadeInUp animated-fast">
                    <h2><a href="#">{{ config('situs.AMENITIES_3_TITLE') }}</a></h2>
                    <p class="price">
                        <span class="free">
                            @if(config('situs.AMENITIES_3_HARGA') !== 0)
                                <p class="price">
                                    <span class="currency">Rp</span>
                                    <span class="price-room">@formatUang(config('situs.AMENITIES_3_HARGA'))</span>
                                    <span class="per">/ {{ config('situ.AMENITIES_3_SATUAN') }}</span>
                                </p>
                            @else 
                                <p class="price">
                                    <span class="free">Gratis</span>
                                </p>
                            @endif
                        </span>
                    </p>
                    <p>{{ config('situs.AMENITIES_3_KETERANGAN') }}</p>
                </div>
                <div class="desc animate-box fadeInUp animated-fast">
                    <h2><a href="#">{{ config('situs.AMENITIES_4_TITLE') }}</a></h2>
                    <p class="price">
                        <span class="free">
                            @if(config('situs.AMENITIES_4_HARGA') !== 0)
                                <p class="price">
                                    <span class="currency">Rp</span>
                                    <span class="price-room">@formatUang(config('situs.AMENITIES_4_HARGA'))</span>
                                    <span class="per">/ {{ config('situ.AMENITIES_4_SATUAN') }}</span>
                                </p>
                            @else 
                                <p class="price">
                                    <span class="free">Gratis</span>
                                </p>
                            @endif
                        </span>
                    </p>
                    <p>{{ config('situs.AMENITIES_4_KETERANGAN') }}</p>
                </div>
                <div class="amenities-img animate-box fadeInUp animated-fast" style="background-image: url(/images/amenities/{{ config('situs.AMENITIES_4_GAMBAR') }});"></div>
        </div>
    </div>
</div>

@stop