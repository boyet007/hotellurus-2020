(function() {

    var db = {

        loadData: function(filter) {
            return $.grep(this.clients, function(client) {
                return (!filter.Name || client.Name.indexOf(filter.Name) > -1)
                    && (filter.Age === undefined || client.Age === filter.Age)
                    && (!filter.Address || client.Address.indexOf(filter.Address) > -1)
                    && (!filter.Country || client.Country === filter.Country)
                    && (filter.Married === undefined || client.Married === filter.Married);
            });
        },

        insertItem: function(insertingClient) {
            this.clients.push(insertingClient);
        },

        updateItem: function(updatingClient) { },

        deleteItem: function(deletingClient) {
            var clientIndex = $.inArray(deletingClient, this.clients);
            this.clients.splice(clientIndex, 1);
        }

    };

    window.db = db;

    db.kota = [
        { Nama: "", Id: 0 },
        { Nama: "Jakarta", Id: 1 },
        { Nama: "Bekasi", Id: 2 },
        { Nama: "Tanggerang", Id: 3 },
        { Nama: "Depok", Id: 4 },
        { Nama: "Makasar", Id: 5 },
        { Nama: "Ujung Pandang", Id: 6 },
        { Nama: "Banjarmasin", Id: 7 }
    ];

    db.jenkel = [
        { Nama: "", Kode: ""},
        { Nama: "Laki-Laki", Kode: "L" },
        { Nama: "Perempuan", Kode: "P" },
    ];

    db.clients = [
        {
            "Nama": "Bpk Sulaiman",
            "Alamat": "Jl Haji Makmur No. 6",
            "Kota": 6,
            "No_Telpon": "021 928 9112",
            "Jenkel": "L"
        },
        {
            "Nama": "Ibu Wina",
            "Alamat": "Jl Haji Makmur No. 7",
            "Kota": 1,
            "No_Telpon": "021 928 9113",
            "Jenkel": "P"
        },
        {
            "Nama": "Bpk Budi",
            "Alamat": "Jl Haji Makmur No. 8",
            "Kota": 5,
            "No_Telpon": "021 928 9114",
            "Jenkel": "L"
        },
        {
            "Nama": "Bpk Madura",
            "Alamat": "Jl Haji Makmur No. 12",
            "Kota": 2,
            "No_Telpon": "021 928 9115",
            "Jenkel": "L"
        },
        {
            "Nama": "Ibu Widuri",
            "Alamat": "Jl Haji Makmur No. 14",
            "Kota": 3,
            "No_Telpon": "021 928 9116",
            "Jenkel": "P"
        },
    ];

    db.users = [
        {
            "ID": "x",
            "Account": "A758A693-0302-03D1-AE53-EEFE22855556",
            "Name": "Carson Kelley",
            "RegisterDate": "2002-04-20T22:55:52-07:00"
        },
      
    
      
     ];

}());